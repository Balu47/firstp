package sonawane2.balu.com.firstp;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by shine on 12-Feb-18.
 */

public class SuperLogin extends AppCompatActivity {

    private RequestQueue requestQueue;
    private Button admin, teacher, login, student, back,CandD;
    private EditText name, pwd, editte;
    private boolean isAdmin;
    private SharedPreferences preferences, Showpref;
    SharedPreferences.Editor editor, seditor;
    private ImageView bilogo, smalllogo;
    private  LinearLayout logui;
    private ProgressDialog progressDialog;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_SECURE,WindowManager.LayoutParams.FLAG_SECURE);

        setContentView(R.layout.superlogin);

        name = findViewById(R.id.editText5);
        pwd = findViewById(R.id.editText6);
        admin = findViewById(R.id.button6);
        teacher = findViewById(R.id.button7);
        login = findViewById(R.id.button4);
        student = findViewById(R.id.button10);
        bilogo = findViewById(R.id.imageView8);
        smalllogo = findViewById(R.id.imageView7);
        back = findViewById(R.id.button11);
        CandD = findViewById(R.id.button24);
        editte = findViewById(R.id.editteacher);
        progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(false);
        progressDialog.setMessage("Login....");


        logui = (LinearLayout) findViewById(R.id.linearLayout5);

        requestQueue = Volley.newRequestQueue(this);

        preferences = getSharedPreferences("com.example.TOKEN", Context.MODE_PRIVATE);
        editor = preferences.edit();

        Showpref = this.getSharedPreferences("com.example.SHOW_SCREEN",this.MODE_PRIVATE);
        seditor = Showpref.edit();

        boolean show = Showpref.getBoolean("show",true);

//        Toast.makeText(getApplicationContext(),"!Show"+!show,Toast.LENGTH_LONG).show();
        if (!show){

            String where = Showpref.getString("goto", "");

            switch (where){

                case  "admin":
                    Intent admin = new Intent(SuperLogin.this, fileuploads.class);
                    admin.putExtra("admin", true);

                    startActivity(admin);
                    finish();
                    break;

                case  "teacher":
                    Intent teacher = new Intent(SuperLogin.this, fileuploads.class);

                    teacher.putExtra("admin", false);
                    startActivity(teacher);
                    finish();
                    break;

                case "student":

                    Intent student = new Intent(SuperLogin.this, MainPage.class);

                    startActivity(student);
                    finish();
                    break;

                default:

                    Toast.makeText(getApplicationContext(),"Error",Toast.LENGTH_LONG).show();


            }
        }

        CandD.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(SuperLogin.this, MapView.class);
                intent.putExtra("load",true);
                startActivity(intent);
            }
        });

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ReChangeV();
            }
        });

        student.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(SuperLogin.this, sonawane2.balu.com.firstp.login.class);
                startActivity(intent);
                finish();
            }
        });

        admin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                editte.setVisibility(View.GONE);
                name.setVisibility(View.VISIBLE);
               ChangeV();
               isAdmin = true;
            }
        });

        teacher.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ChangeV();
                name.setVisibility(View.GONE);
                editte.setVisibility(View.VISIBLE);
//                Intent intent = new Intent(SuperLogin.this, fileuploads.class);
//                intent.putExtra("admin", false);
//                startActivity(intent);
                isAdmin = false;
            }
        });

        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String email = editte.getText().toString();
                String na = name.getText().toString().trim();
                String pass = pwd.getText().toString().trim();

                if (isAdmin){
                    AdminLog(na, pass);

                }else{
                    TeacherLog(email, pass);
                }
            }
        });
    }

    private void AdminLog(final String name, final String pass) {

        progressDialog.show();
//        String url = "https://new-exapp.herokuapp.com/auth/admin";

//        String url = "http://new-exapp.openode.io/auth/admin";

        HashMap<String, String> para = new HashMap<>();
        para.put("Aname",name);
        para.put("Apass",pass);

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, URLS.Admin,new JSONObject(para), new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {

                progressDialog.dismiss();
                try {
                    boolean success = response.getBoolean("success");
                    String token = response.getString("token");

                    if (success){
                        editor.putString("TOKEN",token);
                        editor.commit();
                        seditor.putString("goto", "admin");
                        seditor.putBoolean("show", false);
                        seditor.commit();
                        Toast.makeText(getApplicationContext(),"Login Success ",Toast.LENGTH_LONG).show();

                        Intent intent = new Intent(SuperLogin.this, fileuploads.class);
                        intent.putExtra("admin", true);
                        startActivity(intent);
                        finish();
                    }else {
                        Toast.makeText(getApplicationContext(),"Success "+success,Toast.LENGTH_LONG).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
//                    Toast.makeText(getApplicationContext(),"Success "+e,Toast.LENGTH_LONG).show();
                }

//                if (response.equals("success")){
//                    Toast.makeText(getApplicationContext(),"Success",Toast.LENGTH_LONG).show();
//                    Intent intent = new Intent(SuperLogin.this, fileuploads.class);
//                    startActivity(intent);
//                }else {
//                    Toast.makeText(getApplicationContext(),""+response,Toast.LENGTH_LONG).show();
//                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.dismiss();
                Toast.makeText(getApplicationContext(),"Error "+error,Toast.LENGTH_LONG).show();
            }
        });
//        {
//            @Override
//            public String getBodyContentType() {
//                return "application/x-www-form-urlencoded; charset=UTF-8";
//            }
//
//            @Override
//            protected Map<String, String> getParams() throws AuthFailureError {
//                Map<String, String> param = new HashMap<>();
//                param.put("Aname",name);
//                param.put("Apass",pass);
//
//                return param;
//            }
//        };


        requestQueue.add(request);
    }

    private void TeacherLog(final String name, final String pass) {

        progressDialog.show();
//        String url = "https://new-exapp.herokuapp.com/auth/teacherlog";

        StringRequest request = new StringRequest(Request.Method.POST, URLS.Teacher, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                progressDialog.dismiss();
              if (response.equals("success")){
                  seditor.putBoolean("show", false);
                  seditor.putString("goto", "teacher");
                  seditor.commit();
                  Toast.makeText(getApplicationContext(),"Success",Toast.LENGTH_LONG).show();
                  Intent intent = new Intent(SuperLogin.this, fileuploads.class);
                  intent.putExtra("admin", false);
                  startActivity(intent);
                  finish();
              }else {
                  Toast.makeText(getApplicationContext(),""+response,Toast.LENGTH_LONG).show();
              }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                progressDialog.dismiss();
//                Toast.makeText(getApplicationContext(),""+error,Toast.LENGTH_LONG).show();

            }
        }){
            @Override
            public String getBodyContentType() {
                return "application/x-www-form-urlencoded; charset=UTF-8";
            }

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> param = new HashMap<>();
                param.put("tname",name);
                param.put("pass",pass);

                return param;
            }
        };

        requestQueue.add(request);
    }


    private void ChangeV() {
        bilogo.setVisibility(View.GONE);
        CandD.setVisibility(View.GONE);
        student.setVisibility(View.GONE);
        back.setVisibility(View.VISIBLE);
        smalllogo.setVisibility(View.VISIBLE);
        logui.setVisibility(View.VISIBLE);
        admin.setVisibility(View.GONE);
        teacher.setVisibility(View.GONE);
        login.setVisibility(View.VISIBLE);
    }

    private void ReChangeV(){
        bilogo.setVisibility(View.VISIBLE);
        CandD.setVisibility(View.VISIBLE);
        student.setVisibility(View.VISIBLE);
        back.setVisibility(View.GONE);
        smalllogo.setVisibility(View.GONE);
        logui.setVisibility(View.GONE);
        admin.setVisibility(View.VISIBLE);
        teacher.setVisibility(View.VISIBLE);
        login.setVisibility(View.GONE);
    }
}
