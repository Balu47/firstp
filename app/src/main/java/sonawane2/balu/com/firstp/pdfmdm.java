package sonawane2.balu.com.firstp;

/**
 * Created by shine on 03-Feb-18.
 */

public class pdfmdm {
    String name, pdfurl, sub, sem, uploader, course, branch, year, pdfname, id;

    public pdfmdm(String name, String pdfurl, String sub, String sem, String uploader, String course, String branch, String year, String pdfname, String id) {
        this.name = name;
        this.pdfurl = pdfurl;
        this.sub = sub;
        this.sem = sem;
        this.uploader = uploader;
        this.course = course;
        this.branch = branch;
        this.year = year;
        this.pdfname = pdfname;
        this.id = id;
    }

    public String getPdfname() {
        return pdfname;
    }

    public void setPdfname(String pdfname) {
        this.pdfname = pdfname;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCourse() {
        return course;
    }

    public void setCourse(String course) {
        this.course = course;
    }

    public String getBranch() {
        return branch;
    }

    public void setBranch(String branch) {
        this.branch = branch;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPdfurl() {
        return pdfurl;
    }

    public void setPdfurl(String pdfurl) {
        this.pdfurl = pdfurl;
    }

    public String getSub() {
        return sub;
    }

    public void setSub(String sub) {
        this.sub = sub;
    }

    public String getSem() {
        return sem;
    }

    public void setSem(String sem) {
        this.sem = sem;
    }

    public String getUploader() {
        return uploader;
    }

    public void setUploader(String uploader) {
        this.uploader = uploader;
    }
}
