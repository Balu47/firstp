package sonawane2.balu.com.firstp.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

import sonawane2.balu.com.firstp.R;
import sonawane2.balu.com.firstp.StudentPOJO;

/**
 * Created by shine on 24-Feb-18.
 */

public class Student_Adapter extends RecyclerView.Adapter<Student_Adapter.ViewHolder> {

    ArrayList<StudentPOJO> arrayList = new ArrayList<>();
    Context context;
    private Student_onclick oncli ;

    public Student_Adapter(ArrayList<StudentPOJO> arrayList, Context context, Student_onclick oncli) {
        this.arrayList = arrayList;
        this.context = context;
        this.oncli = oncli;
    }

    @Override
    public Student_Adapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.tlist,parent,false);
        return new Student_Adapter.ViewHolder(view);    }

    @Override
    public void onBindViewHolder(Student_Adapter.ViewHolder holder,final int position) {
        holder.sname.setText("Name : "+arrayList.get(position).getSname());
        holder.syear.setText("Year : "+arrayList.get(position).getYear());

        holder.sname.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (oncli != null) {
                    oncli.Smyonclick(position,"view");
                }
            }
        });
//        holder.name.setOnLongClickListener(new View.OnLongClickListener() {
//            @Override
//            public boolean onLongClick(View view) {
//                if (oncli != null) {
//                    oncli.myonclick(position,"view");
//                }                return true;
//            }
//        });
    }

    @Override
    public int getItemCount() {
        return arrayList.isEmpty() ? 0 : arrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        TextView sname, syear;
        public ViewHolder(View itemView) {
            super(itemView);
            sname = itemView.findViewById(R.id.tname);
            syear = itemView.findViewById(R.id.tsub);
        }
    }

    public interface Student_onclick{
        public void Smyonclick(int positions,String action);
    }
}
