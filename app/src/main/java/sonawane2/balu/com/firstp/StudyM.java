package sonawane2.balu.com.firstp;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by shine on 13-Feb-18.
 */

public class StudyM extends AppCompatActivity {

    private TextView curyear, name, no, cr, br;
    private SharedPreferences preferences,  logoutpref, log;
    private String SEM1, SEM2;
    private ListView listView;
    private String[] semli;
    private ArrayList<String> arrayList;
    private int semester = 0;
    private  String course,branch;
    private Button logout, detail;
    private DrawerLayout drawerLayout;
    private ActionBarDrawerToggle drawerToggle;
    private SharedPreferences.Editor editor, seditor;
    private ProgressDialog progressDialog;
    private RequestQueue requestQueue;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_SECURE,WindowManager.LayoutParams.FLAG_SECURE);

        setContentView(R.layout.studymaterial);

        Toolbar toolbar = (Toolbar)findViewById(R.id.mtToolbar);
        setSupportActionBar(toolbar);
        ActionBar ab = getSupportActionBar();
        ab.setDisplayShowHomeEnabled(true);
        ab.setTitle("Mayur Engineering Academy");

        requestQueue = Volley.newRequestQueue(this);
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Loading First Time....");

        drawerLayout = findViewById(R.id.sdrawer);

        drawerToggle = new ActionBarDrawerToggle(this,drawerLayout,toolbar,R.string.open,R.string.close);
        drawerLayout.addDrawerListener(drawerToggle);

        drawerToggle.syncState();

        name = findViewById(R.id.textView11);
        no = findViewById(R.id.textView12);
        cr = findViewById(R.id.cr);
        br = findViewById(R.id.br);
        listView = findViewById(R.id.semlist);
        logout = findViewById(R.id.detail);
        detail = findViewById(R.id.button25);

        preferences = this.getSharedPreferences("com.example.USER_INFO", Context.MODE_PRIVATE);
        logoutpref = this.getSharedPreferences("com.example.SHOW_SCREEN",this.MODE_PRIVATE);
        log = this.getSharedPreferences("com.example.SHOW",this.MODE_PRIVATE);
        String cuy = preferences.getString("curyear","");
        String uname = preferences.getString("username","");
        String uno = preferences.getString("userno","");
         course = preferences.getString("course","");
         branch = preferences.getString("branch", "");
         semester = Integer.parseInt(preferences.getString("sem","0"));

     editor = log.edit();
     seditor = preferences.edit();


     CheckLog();
//        LoadInfo();
        detail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });

        logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });

    }

    private void CheckLog() {

        if (log.getBoolean("login",false) || name.getText().toString().isEmpty()){

          LoadInfo();

        }else{

            name.setText("Name : "+preferences.getString("username",""));
            no.setText("No.: "+preferences.getString("userno",""));
            cr.setText("Course: "+course);
            br.setText("Branch: "+branch);

            init();
        }

    }

    private void LoadInfo() {

        String url = URLS.FirstR+"auth/getSinfo";
        progressDialog.show();
        HashMap<String, String> para = new HashMap<>();
        para.put("sid",log.getString("UID",""));

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, url, new JSONObject(para), new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {

                progressDialog.dismiss();
                try {
                    if (response.getBoolean("success")){

                    JSONObject  object = response.getJSONObject("result");

                        seditor.putString("username",object.getString("name"));
                        seditor.putString("userno", object.getString("no"));
                        seditor.putString("curyear", object.getString("year"));
                        seditor.putString("course", object.getString("course"));
                        seditor.putString("branch", object.getString("branch"));
                        seditor.putString("sem",object.getString("sem"));
                         editor.putBoolean("login", false);
                         editor.commit();
                        seditor.commit();





                        CheckLog();

                    }else{

                    }
//                    Toast.makeText(getApplicationContext(),"Error ",Toast.LENGTH_LONG).show();

                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(getApplicationContext()," Exeption ",Toast.LENGTH_LONG).show();

                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
Toast.makeText(getApplicationContext(),"Error ",Toast.LENGTH_LONG).show();
                progressDialog.dismiss();
            }
        });

        requestQueue.add(request);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.student_menu,menu);

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()){
            case R.id.detail:

                Intent intent = new Intent(StudyM.this, MapView.class);
                startActivity(intent);
                return true;

            case R.id.logout:

                Logout();
                return true;

            case R.id.update:


                Intent intent1 = new Intent(this, signup.class);
                intent1.putExtra("update", true);
                startActivity(intent1);
                finish();
                return  true;
            case R.drawable.ic_menu_black_24dp:

                  return true;
            default:

                return super.onOptionsItemSelected(item);

        }



    }

    private void Logout(){

        SharedPreferences.Editor editor , eeditor;

        editor = logoutpref.edit();
        eeditor = log.edit();

        eeditor.putBoolean("show", false);
        eeditor.commit();

        editor.putBoolean("show", true);
        editor.commit();

        Intent intents = new Intent(StudyM.this, SuperLogin.class);

        startActivity(intents);
        finish();
    }

    private void init() {

        semli = new String[]{
                "Semester - 1",
                "Semester - 2",
                "Semester - 3",
                "Semester - 4",
                "Semester - 5",
                "Semester - 6",
                "Semester - 7",
                "Semester - 8",
                "Semester - 9",
                "Semester - 10"
        };

        arrayList = new ArrayList<>();
        for (int i = 0; i < semester; i++){
            arrayList.add(semli[i]);
        }

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(StudyM.this, R.layout.simple_list2, R.id.textsl, arrayList);

        listView.setAdapter(adapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

                String sem ="";

                switch (i){
                    case 0:
                        sem = "sem1";
                    break;

                    case 1:
                        sem = "sem2";
                        break;

                    case 2:
                        sem = "sem3";
                        break;

                    case 3:
                        sem = "sem4";
                        break;

                    case 4:
                        sem = "sem5";
                        break;

                    case 5:
                        sem = "sem6";
                        break;

                    case 6:
                        sem = "sem7";
                        break;

                    case 7:
                        sem = "sem8";
                        break;

                    case 8:
                        sem = "sem9";
                        break;

                    case 9:
                        sem = "sem10";
                        break;

                   default:

                }
                String seme = (String) listView.getItemAtPosition(i);
//                Toast.makeText(getApplicationContext(),sem,Toast.LENGTH_LONG).show();

                Intent intent = new Intent(StudyM.this, SubjectList.class);
                intent.putExtra("sem",sem);
                intent.putExtra("course", course);
                intent.putExtra("branch", branch);
                intent.putExtra("admin", false);
                startActivity(intent);
            }
        });
    }
}
