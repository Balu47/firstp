package sonawane2.balu.com.firstp.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import sonawane2.balu.com.firstp.R;
import sonawane2.balu.com.firstp.VidPlayer;
import sonawane2.balu.com.firstp.pixabay;

/**
 * Created by shine on 14-Jan-18.
 */

public class Videoli extends RecyclerView.Adapter<Videoli.ViewHolder> {
    Context context;
    ArrayList<pixabay> pixabays = new ArrayList<>();

    public Videoli(Context context, ArrayList<pixabay> pixabays){
        this.context = context;
        this.pixabays = pixabays;
    }

    @Override
    public Videoli.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_view,parent,false);

        return new Videoli.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(Videoli.ViewHolder holder, int position) {
        pixabay pos = pixabays.get(position);
        final String imuri = pos.getImagev();

        holder.tag.setText(pos.getTags());
        holder.like.setText("Likes: " + pos.getLikes());
        holder.views.setText("Views: " + pos.getView());

        Picasso.with(context).load(imuri).placeholder(R.drawable.default_artwork).fit().into(holder.imageView);

        holder.imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context,VidPlayer.class);
                intent.putExtra("uri",imuri);
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return pixabays == null ? 0 : pixabays.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        TextView views, like, tag;
        ImageView imageView;

        public ViewHolder(View v){
            super(v);
            views =v.findViewById(R.id.view);
            like = v.findViewById(R.id.like);
            tag = v.findViewById(R.id.tag);
            imageView = v.findViewById(R.id.imageView);
        }
    }
}
