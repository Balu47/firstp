package sonawane2.balu.com.firstp.Singleton;

import android.content.Context;
import android.net.Uri;
import android.view.SurfaceView;

import com.google.android.exoplayer2.DefaultLoadControl;
import com.google.android.exoplayer2.DefaultRenderersFactory;
import com.google.android.exoplayer2.ExoPlayerFactory;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.source.ExtractorMediaSource;
import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.trackselection.AdaptiveTrackSelection;
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector;
import com.google.android.exoplayer2.trackselection.TrackSelection;
import com.google.android.exoplayer2.ui.SimpleExoPlayerView;
import com.google.android.exoplayer2.upstream.DataSource;
import com.google.android.exoplayer2.upstream.DefaultBandwidthMeter;
import com.google.android.exoplayer2.upstream.DefaultHttpDataSourceFactory;
import com.google.android.exoplayer2.upstream.HttpDataSource;
import com.google.android.exoplayer2.upstream.cache.Cache;
import com.google.android.exoplayer2.upstream.cache.CacheDataSourceFactory;
import com.google.android.exoplayer2.upstream.cache.LeastRecentlyUsedCacheEvictor;
import com.google.android.exoplayer2.upstream.cache.SimpleCache;
import com.google.android.exoplayer2.util.Util;

/**
 * Created by shine on 17-Jan-18.
 */

public class SExoplayer {

    private static final DefaultBandwidthMeter BANDWIDTH_METER  = new DefaultBandwidthMeter();

    public static SExoplayer instance;

    public static SExoplayer getInstance(){
        if ( instance == null){
            instance = new SExoplayer();
        }
        return instance;
    }

    private SimpleExoPlayer player;
    private Uri uri;
    private boolean isPlaing;

    private SExoplayer(){}

    public void prepareExoplayer(Context context, Uri uri, SimpleExoPlayerView playerView){
        if (context != null && uri != null && playerView != null){
            if (!uri.equals(uri) && player == null ){

                TrackSelection.Factory adaptiveTrackSelectionFactory =
                        new AdaptiveTrackSelection.Factory(BANDWIDTH_METER);

                player = ExoPlayerFactory.newSimpleInstance(
                        new DefaultRenderersFactory(context),
                        new DefaultTrackSelector(),
                        new DefaultLoadControl());

                String ua = Util.getUserAgent(context,"Firstp");

                Cache cache = new SimpleCache(context.getCacheDir(),new LeastRecentlyUsedCacheEvictor(10 * 1024 * 1024));
                HttpDataSource.Factory httpDataSource = new DefaultHttpDataSourceFactory(ua);

                DataSource.Factory cacheDataSource = new CacheDataSourceFactory(cache,httpDataSource);
                MediaSource mediaSource = new ExtractorMediaSource.Factory(cacheDataSource).createMediaSource(uri);

                player.prepare(mediaSource,true,false);
            }

            player.clearVideoSurface();
            player.setVideoSurfaceView( (SurfaceView)playerView.getVideoSurfaceView() );
            player.seekTo(player.getCurrentPosition() + 1);
            playerView.setPlayer(player);

        }
    }

    public void releaseVideo(){
        if (player != null){
            player.release();
        }
        player = null;
    }

    public void goToBackground(){
        if (player != null){
            isPlaing = player.getPlayWhenReady();
            player.setPlayWhenReady(false);
        }
    }

    public void goToForground(){
        if (player != null){
            player.setPlayWhenReady(isPlaing);
        }
    }


}
