package sonawane2.balu.com.firstp;

/**
 * Created by shine on 24-Feb-18.
 */

public class StudentPOJO {

    private String sname, no, course, branch, year,cid ;

    public StudentPOJO(String name, String no, String course, String branch, String year, String cid) {
        this.sname = name;
        this.no = no;
        this.course = course;
        this.branch = branch;
        this.year = year;
        this.cid = cid;
    }

    public String getCid() {
        return cid;
    }

    public void setCid(String cid) {
        this.cid = cid;
    }

    public String getSname() {
        return sname;
    }

    public void setSname(String sname) {
        this.sname = sname;
    }

    public String getNo() {
        return no;
    }

    public void setNo(String no) {
        this.no = no;
    }

    public String getCourse() {
        return course;
    }

    public void setCourse(String course) {
        this.course = course;
    }

    public String getBranch() {
        return branch;
    }

    public void setBranch(String branch) {
        this.branch = branch;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }
}
