package sonawane2.balu.com.firstp;

import android.Manifest;
import android.app.Dialog;
import android.app.NotificationManager;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.NavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.RequestQueue;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.nbsp.materialfilepicker.MaterialFilePicker;
import com.nbsp.materialfilepicker.ui.FilePickerActivity;


import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import java.util.regex.Pattern;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

/**
 * Created by shine on 10-Feb-18.
 */

public class fileuploads extends AppCompatActivity {

    private Dialog dialog, Filedata, SemChoose, dialogaddcb;
    private boolean admin;
    private NotificationManager notificationManager;
    private NotificationCompat.Builder builder;
    private int ID = 1;
    private RequestQueue requestQueue;
//    private static final String UPLOAD_URL = "https://1f1e5b10.ngrok.io/upload";
    private boolean permissiongrant;
    private Uri filepath;
    private String path;
    private static final int STORAGE_PER = 21;
    private static final int PDF_CHOOSER = 20;
    private Button choose, teacher, liteacher, liofpdf, addbranch, addcourse, liofcourse, liofbranch, liofstudent, logout, deletpdf, addabout, address, addsub, addcha, listofsub, listofcha, deatails;
    private EditText namea;
    private ProgressDialog progressDialog;
    private SharedPreferences preferences, logoutpref;
    private String CURRENT_SAME;
    private CBPOJO cbpojo;
    private BaseAdapter adapter;
    private String Acourse = "";
    private ArrayList<CBPOJO> cbpojos;
    private String Abranch = "";
    private String Ayear = "";
    private String token;
    private static final String UPLOAD_URL = URLS.FirstR + "upload";
    private Common_Dialog common_dialog;
    private DrawerLayout drawerLayout;
    private ActionBarDrawerToggle drawerToggle;
    private ListView listView;
    private TextView noStudents, noTeacher, noBranch, noSubjects, noPdfds;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fileupload);

        choose = findViewById(R.id.choose);
        teacher = findViewById(R.id.button5);
        liteacher = findViewById(R.id.button12);
        liofpdf = findViewById(R.id.liofpdf);
        addbranch = findViewById(R.id.adbranch);
        addcourse = findViewById(R.id.addcourse);
        liofcourse = findViewById(R.id.liofcourse);
        liofbranch = findViewById(R.id.liofbranch);
        liofstudent = findViewById(R.id.liofstudent);
        logout = findViewById(R.id.logout);
        deletpdf = findViewById(R.id.deletepdf);
        addabout = findViewById(R.id.about);
        address = findViewById(R.id.address);
        addsub = findViewById(R.id.addsub);
        addcha = findViewById(R.id.addcha);
        listofsub = findViewById(R.id.liofsubject);
        listofcha = findViewById(R.id.liofchapter);
        noStudents = findViewById(R.id.textView29);
        noTeacher = findViewById(R.id.textView30);
        noSubjects = findViewById(R.id.textView32);
        noPdfds = findViewById(R.id.textView34);
        deatails = findViewById(R.id.details);




        drawerLayout = findViewById(R.id.drawerlayout);
        Toolbar toolbar = (Toolbar)findViewById(R.id.masterToolbar);

        setSupportActionBar(toolbar);
        ActionBar ab = getSupportActionBar();
        ab.setDisplayShowHomeEnabled(true);
        ab.setTitle("Mayur Engineering Academy");

        drawerToggle = new ActionBarDrawerToggle(this,drawerLayout,toolbar,R.string.open,R.string.close);
        drawerLayout.addDrawerListener(drawerToggle);

        drawerToggle.syncState();

        LoadDashBoard();

        listView = findViewById(R.id.dlist);

        String[] somearr = new String[]{
                " ADD TEACHER",
                " ADD COURSE ",
                " ADD BRANCH ",
                " LIST OF TEACHER",
                " LIST OF STUDENTS",
                " LIST OF PDF",
                " LIST OF BRANCH",
                " DELETE PDF",
                " LOGOUT",
        };
        ArrayAdapter dlistadapter = new ArrayAdapter<String>(fileuploads.this,R.layout.simple_list3, R.id.textsl3, somearr);
        listView.setAdapter(dlistadapter);
        preferences = this.getSharedPreferences("com.example.TOKEN",Context.MODE_PRIVATE);
        logoutpref = this.getSharedPreferences("com.example.SHOW_SCREEN",this.MODE_PRIVATE);
        dialog = new Dialog(this, R.style.Theme_AppCompat_DayNight_Dialog_Alert);
        Filedata = new Dialog(this, R.style.Theme_AppCompat_DayNight_Dialog_Alert);
        SemChoose = new Dialog(this,R.style.Theme_AppCompat_DayNight_Dialog_Alert);
        dialogaddcb = new Dialog(this,R.style.Theme_AppCompat_DayNight_Dialog_Alert);
        token  = preferences.getString("TOKEN","");
      cbpojos = new ArrayList<>();

       common_dialog = new Common_Dialog();
       common_dialog.ClearThings();;

        Bundle bundle = getIntent().getExtras();
        if (bundle != null){
            admin = bundle.getBoolean("admin");
            if (!admin){
                liteacher.setVisibility(View.GONE);
                teacher.setVisibility(View.GONE);
                liofstudent.setVisibility(View.GONE);
                liofbranch.setVisibility(View.GONE);
                liofcourse.setVisibility(View.GONE);
                addbranch.setVisibility(View.GONE);
                addcourse.setVisibility(View.GONE);
                address.setVisibility(View.GONE);
                addabout.setVisibility(View.GONE);
            }
        }

        requestQueue = Volley.newRequestQueue(this);
        progressDialog = new ProgressDialog(this);
        progressDialog.setTitle("Please Wait...");
        progressDialog.setCancelable(false);
       if(!permissiongrant){
           requestStorage();
       }
       init();

       BackGround();

     deatails.setOnClickListener(new View.OnClickListener() {
         @Override
         public void onClick(View view) {
             Intent intent = new Intent(fileuploads.this, MapView.class);
             intent.putExtra("load", true);
             startActivity(intent);
         }
     });

       noStudents.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View view) {

               progressDialog.show();
               StringRequest request = new StringRequest(com.android.volley.Request.Method.GET, URLS.FirstR + "scount", new com.android.volley.Response.Listener<String>() {
                   @Override
                   public void onResponse(String response) {

                       progressDialog.dismiss();
                       if (response.equals("error")){

                       }else{
                           noStudents.setText("Total No. of Students : "+response);
                       }

                   }
               }, new com.android.volley.Response.ErrorListener() {
                   @Override
                   public void onErrorResponse(VolleyError error) {

                       progressDialog.dismiss();
                   }
               });
               requestQueue.add(request);
           }


       });

       noTeacher.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View view) {

               progressDialog.show();
               StringRequest request = new StringRequest(com.android.volley.Request.Method.GET, URLS.FirstR + "tcount", new com.android.volley.Response.Listener<String>() {
                   @Override
                   public void onResponse(String response) {
                       progressDialog.dismiss();
                       if (response.equals("error")){

                       }else{
                           noTeacher.setText("Total No. of Teacher : "+response);
                       }

                   }
               }, new com.android.volley.Response.ErrorListener() {
                   @Override
                   public void onErrorResponse(VolleyError error) {

                       progressDialog.dismiss();
                   }
               });

               requestQueue.add(request);
           }

       });

       noSubjects.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View view) {
progressDialog.show();
               StringRequest request = new StringRequest(com.android.volley.Request.Method.GET, URLS.FirstR + "subcount", new com.android.volley.Response.Listener<String>() {
                   @Override
                   public void onResponse(String response) {
progressDialog.dismiss();
                       if (response.equals("error")){

                       }else{
                           noSubjects.setText("Total No. of Subjects : "+response);
                       }

                   }
               }, new com.android.volley.Response.ErrorListener() {
                   @Override
                   public void onErrorResponse(VolleyError error) {
progressDialog.dismiss();
                   }
               });

               requestQueue.add(request);
           }
       });

       noPdfds.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View view) {
progressDialog.show();
               StringRequest request = new StringRequest(com.android.volley.Request.Method.GET, URLS.FirstR + "pdfcount", new com.android.volley.Response.Listener<String>() {
                   @Override
                   public void onResponse(String response) {
progressDialog.dismiss();
                       if (response.equals("error")){

                       }else{
                           noPdfds.setText("Total No. of Pdf : "+response);
                       }

                   }
               }, new com.android.volley.Response.ErrorListener() {
                   @Override
                   public void onErrorResponse(VolleyError error) {
progressDialog.dismiss();
                   }
               });
               requestQueue.add(request);
           }
       });

       listofsub.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View view) {

               Intent intent = new Intent(fileuploads.this, Common_Act.class);
               intent.putExtra("where","subject");
               startActivity(intent);
           }
       });

       listofcha.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View view) {

               Intent intent = new Intent(fileuploads.this, Common_Act.class);
               intent.putExtra("where","chapter");
               startActivity(intent);
           }
       });

       addsub.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View view) {

               ShowPOP(true);
           }
       });

       addcha.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View view) {

               ShowPOP(false);
           }
       });

       address.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View view) {
               ShowAbouDialog("address");
           }
       });

       addabout.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View view) {
               ShowAbouDialog("about");
           }
       });
       logout.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View view) {
               SharedPreferences.Editor editor = logoutpref.edit();
               editor.putBoolean("show", true);
               editor.commit();
               Intent intent = new Intent(fileuploads.this, SuperLogin.class);

               startActivity(intent);
               finish();
           }
       });

       deletpdf.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View view) {
               SharedPreferences.Editor editor = preferences.edit();
               editor.putBoolean("delete", true);
               editor.commit();
               Intent intent = new Intent(fileuploads.this, Common_Act.class);
               intent.putExtra("where","deletepdf");
               startActivity(intent);           }
       });

       liofstudent.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View view) {
             Intent intent = new Intent(fileuploads.this, Common_Act.class);
             intent.putExtra("where","student");
             startActivity(intent);
           }
       });

       liofcourse.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View view) {
               commomlist("course");

           }
       });

       liofbranch.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View view) {
               commomlist("branch");
           }
       });

       addcourse.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View view) {
               AddCourse();
           }
       });

       addbranch.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View view) {
               AddBranch();
           }
       });
       liofpdf.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View view) {
//               Intent intent = new Intent(fileuploads.this, PdfList.class);
//               startActivity(intent);
               SharedPreferences.Editor editor = preferences.edit();
               editor.putBoolean("delete", false);
               editor.commit();
//               commomlist("pdf");
               Intent intent = new Intent(fileuploads.this, Common_Act.class);
               intent.putExtra("where","pdf");
               startActivity(intent);
           }
       });

       liteacher.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View view) {
//             commomlist("teacher");
               Intent intent = new Intent(fileuploads.this, Common_Act.class);
               intent.putExtra("where","teacher");
               startActivity(intent);
           }
       });

       teacher.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View view) {
               showPopup();
           }
       });
    }

    private void BackGround() {
        Handler handler = new Handler();

        handler.post(new Runnable() {
            @Override
            public void run() {
                StringRequest request = new StringRequest(com.android.volley.Request.Method.GET, URLS.FirstR + "scount", new com.android.volley.Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        progressDialog.dismiss();
                        if (response.equals("error")){

                        }else{
                            noStudents.setText("Total No. of Students : "+response);
                        }

                    }
                }, new com.android.volley.Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                });
                requestQueue.add(request);
            }

        });

        handler.post(new Runnable() {
            @Override
            public void run() {
                StringRequest request = new StringRequest(com.android.volley.Request.Method.GET, URLS.FirstR + "tcount", new com.android.volley.Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        progressDialog.dismiss();
                        if (response.equals("error")){

                        }else{
                            noTeacher.setText("Total No. of Teacher : "+response);
                        }

                    }
                }, new com.android.volley.Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                });
                requestQueue.add(request);
            }

        });

        handler.post(new Runnable() {
            @Override
            public void run() {
                StringRequest request = new StringRequest(com.android.volley.Request.Method.GET, URLS.FirstR + "subcount", new com.android.volley.Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        progressDialog.dismiss();
                        if (response.equals("error")){

                        }else{
                            noSubjects.setText("Total No. of Subjects : "+response);
                        }

                    }
                }, new com.android.volley.Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                });
                requestQueue.add(request);
            }

        });

        handler.post(new Runnable() {
            @Override
            public void run() {
                StringRequest request = new StringRequest(com.android.volley.Request.Method.GET, URLS.FirstR + "pdfcount", new com.android.volley.Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        progressDialog.dismiss();
                        if (response.equals("error")){

                        }else{
                            noPdfds.setText("Total No. of Pdf : "+response);
                        }

                    }
                }, new com.android.volley.Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                });
                requestQueue.add(request);
            }

        });


    }

    private void LoadDashBoard() {


    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()){

            case R.drawable.ic_menu_black_24dp:

                return true;
            default:

                return super.onOptionsItemSelected(item);

        }



    }

    private void ShowPOP(final boolean isSub) {
      final   Button co, br, year, sem, sub, submit, cancel;
      final   EditText sname, cname;
        dialog.setContentView(R.layout.add_sub);
        co = dialog.findViewById(R.id.button20c);
        br = dialog.findViewById(R.id.button20b);
        year = dialog.findViewById(R.id.button20y);
        sem = dialog.findViewById(R.id.button20sem);
        sub = dialog.findViewById(R.id.button20s);
        sname = dialog.findViewById(R.id.editTextSub);
        cname = dialog.findViewById(R.id.editTextcha);
        submit = dialog.findViewById(R.id.button17);
        cancel = dialog.findViewById(R.id.button17c);

        if (isSub){
            cname.setVisibility(View.GONE);
            sub.setVisibility(View.GONE);
        }else{
            sname.setVisibility(View.GONE);

        }

        dialog.show();
        co.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                common_dialog.ChooseCourse(dialogaddcb,fileuploads.this, requestQueue,co);
            }
        });

        br.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                common_dialog.ChooseBranch(dialogaddcb,fileuploads.this, requestQueue,br);

            }
        });

        year.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                common_dialog.LoadYear(dialogaddcb,fileuploads.this, requestQueue,year);

            }
        });

        sub.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                common_dialog.ChooseSubject(dialogaddcb,fileuploads.this, requestQueue,sub);

            }
        });

        sem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                ChosePopup(sem);
            }
        });

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (isSub){
                    if (sname.getText().toString().isEmpty() || CURRENT_SAME.equals("") || common_dialog.UNIQUEC.isEmpty() || common_dialog.UNIQUEB.isEmpty() || common_dialog.UNIQUEY.isEmpty()){

                        Toast.makeText(getApplicationContext(),"Please Choose All Fields", Toast.LENGTH_LONG).show();
                    }else{
                        AddSubject(sname.getText().toString());
                        dialog.dismiss();
                    }

                }else{
                    if (cname.getText().toString().isEmpty() || common_dialog.UNIQUEID.isEmpty()){

                        Toast.makeText(getApplicationContext(),"Please Choose All Fields", Toast.LENGTH_LONG).show();

                    }else{

                        AddCha(cname.getText().toString());
                        dialog.dismiss();
                    }
                }
            }
        });

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                dialog.dismiss();
                common_dialog.ClearThings();
            }
        });

        dialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialogInterface) {
                common_dialog.ClearThings();
            }
        });

    }

    private void AddSubject(final String sub) {


        StringRequest request = new StringRequest(com.android.volley.Request.Method.POST, URLS.AddSub, new com.android.volley.Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                progressDialog.dismiss();
                common_dialog.ClearThings();

                Toast.makeText(getApplicationContext(),"Subject Added",Toast.LENGTH_LONG).show();
            }
        }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.dismiss();
                common_dialog.ClearThings();
                Toast.makeText(getApplicationContext(),"error "+error,Toast.LENGTH_LONG).show();

            }
        }){
            @Override
            public String getBodyContentType() {
                return "application/x-www-form-urlencoded; charset=UTF-8";
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> header = new HashMap<>();
                header.put("token",preferences.getString("TOKEN",""));
                return header;
            }

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> param = new HashMap<>();
                param.put("sname",sub);
                param.put("course", common_dialog.UNIQUEC);
                param.put("branch", common_dialog.UNIQUEB);
                param.put("year", common_dialog.UNIQUEY);
                param.put("sem", CURRENT_SAME);

                return param;
            }
        };
        requestQueue.add(request);    }

    private void AddCha(final String cha){

        StringRequest request = new StringRequest(com.android.volley.Request.Method.POST, URLS.AddChap, new com.android.volley.Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                progressDialog.dismiss();
                common_dialog.ClearThings();

                Toast.makeText(getApplicationContext(),response,Toast.LENGTH_LONG).show();
            }
        }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.dismiss();
                common_dialog.ClearThings();
                Toast.makeText(getApplicationContext(),"error "+error,Toast.LENGTH_LONG).show();

            }
        }){
            @Override
            public String getBodyContentType() {
                return "application/x-www-form-urlencoded; charset=UTF-8";
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> header = new HashMap<>();
                header.put("token",preferences.getString("TOKEN",""));
                return header;
            }

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> param = new HashMap<>();
                param.put("sid",common_dialog.UNIQUEID);
                param.put("chapter", cha);


                return param;
            }
        };
        requestQueue.add(request);
    }

    private void ShowAbouDialog(final String where) {

        final EditText line1, line2, line3, pin, mob, land, title, desc;
        Button sub, cancel;
        dialog.setContentView(R.layout.address);

        line1 = dialog.findViewById(R.id.lin1);
        line2 = dialog.findViewById(R.id.lin2);
        line3 = dialog.findViewById(R.id.lin3);
        pin = dialog.findViewById(R.id.lin4);
        mob = dialog.findViewById(R.id.lin5);
        land = dialog.findViewById(R.id.lin6);
        title = dialog.findViewById(R.id.lin7);
        desc = dialog.findViewById(R.id.lin8);

        if (where == "about"){

            line1.setVisibility(View.GONE);
            line2.setVisibility(View.GONE);
            line3.setVisibility(View.GONE);
            pin.setVisibility(View.GONE);
            mob.setVisibility(View.GONE);
            land.setVisibility(View.GONE);

        }else{

            title.setVisibility(View.GONE);
            desc.setVisibility(View.GONE);

        }

        cancel = dialog.findViewById(R.id.button18);
        sub = dialog.findViewById(R.id.button19);

        sub.setText("submit");
        cancel.setText("cancel");
        dialog.show();

        sub.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                StringRequest request = new StringRequest(com.android.volley.Request.Method.POST, URLS.About, new com.android.volley.Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        dialog.dismiss();
                        Toast.makeText(getApplicationContext(),response,Toast.LENGTH_LONG).show();

                    }
                }, new com.android.volley.Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                        dialog.dismiss();
                        Toast.makeText(getApplicationContext(),"Error"+error,Toast.LENGTH_LONG).show();
                    }
                }){
                    @Override
                    protected Map<String, String> getParams() throws AuthFailureError {
                        Map<String, String> param = new HashMap<>();
                        param.put("line1",line1.getText().toString());
                        param.put("line2", line2.getText().toString());
                        param.put("line3",line3.getText().toString());
                        param.put("landline", land.getText().toString());
                        param.put("mo",mob.getText().toString());
                        param.put("pincode", pin.getText().toString());
                        param.put("about",desc.getText().toString());
                        param.put("title", title.getText().toString());
                        param.put("cid","address");
                        param.put("where", where);

                        return param;
                    }

                };
                requestQueue.add(request);
            }
        });

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
    }

    private void commomlist(String where) {
        Intent intent = new Intent(fileuploads.this,CourseList.class);
        intent.putExtra("where", where);
        startActivity(intent);
    }

    private void AddCourse(){
        final EditText name, nofsem, toyear,name2;

        dialogaddcb.setContentView(R.layout.adcoursebranch);

        name = dialogaddcb.findViewById(R.id.editText7);
        nofsem = dialogaddcb.findViewById(R.id.editText8);
        toyear = dialogaddcb.findViewById(R.id.editText9);
        name2 = dialogaddcb.findViewById(R.id.editText77);
        Button sub = dialogaddcb.findViewById(R.id.button17);
        Button button = dialogaddcb.findViewById(R.id.button20);
        Button cancel = dialogaddcb.findViewById(R.id.button35);

        name2.setVisibility(View.GONE);
        button.setVisibility(View.GONE);

        dialogaddcb.show();

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                dialogaddcb.dismiss();
            }
        });

        sub.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String nam = name.getText().toString();
                String nofseme = nofsem.getText().toString();
                String toyears = toyear.getText().toString();

               if (!nam.isEmpty() && !nam.equals("") && !nofseme.isEmpty() && !toyears.isEmpty()){

//                   Toast.makeText(getApplicationContext(),"sem "+nofseme+" year "+toyears,Toast.LENGTH_LONG).show();

                   dialogaddcb.dismiss();
                   AddCB( nam ,nofseme, toyears);
               }else{
                   Toast.makeText(getApplicationContext(),"Please Enter Name!",Toast.LENGTH_LONG).show();

               }
            }
        });
    }

    private void AddCB(final String nam, final String sem, final String year) {
     Toast.makeText(getApplicationContext(),token,Toast.LENGTH_LONG).show();
        progressDialog.show();
        StringRequest requestbrach = new StringRequest(com.android.volley.Request.Method.POST, URLS.AddCourse, new com.android.volley.Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                progressDialog.dismiss();
                if (response.equals("success")){
                    Toast.makeText(getApplicationContext(),"Added Successfully",Toast.LENGTH_LONG).show();

                }else{
                    Toast.makeText(getApplicationContext(),response,Toast.LENGTH_LONG).show();
                }

            }
        }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.dismiss();
                Toast.makeText(getApplicationContext(),"Error "+error,Toast.LENGTH_LONG).show();

            }
        }){
            @Override
            public String getBodyContentType() {
                return "application/x-www-form-urlencoded";
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> header = new HashMap<>();
                header.put("token",preferences.getString("TOKEN",""));
                return header;
            }

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> param = new HashMap<>();
                param.put("cbname",nam);
                param.put("nofsem",sem);
                param.put("toyear",year);



                return param;
            }
        };

        requestQueue.add(requestbrach);

    }

    private void AddBranch() {
        final EditText name, nofsem, toyear, name2;

        dialogaddcb.setContentView(R.layout.adcoursebranch);

        name2 = dialogaddcb.findViewById(R.id.editText7);
        nofsem = dialogaddcb.findViewById(R.id.editText8);
        toyear = dialogaddcb.findViewById(R.id.editText9);
        name = dialogaddcb.findViewById(R.id.editText77);
        Button sub = dialogaddcb.findViewById(R.id.button17);
        final Button button = dialogaddcb.findViewById(R.id.button20);
        Button cancel = dialogaddcb.findViewById(R.id.button35);

        name2.setVisibility(View.GONE);
        nofsem.setVisibility(View.GONE);
        toyear.setVisibility(View.GONE);

        dialogaddcb.show();

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                dialogaddcb.dismiss();
            }
        });

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                 ChooseCourse(button);
                 common_dialog.ChooseCourse(dialog,fileuploads.this, requestQueue, button);
            }
        });

        sub.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String brname = name.getText().toString();

                if (!common_dialog.UNIQUEC.isEmpty() && !brname.isEmpty()){

                    AddBR(brname);

                    dialogaddcb.dismiss();
                }else{
                    Toast.makeText(getApplicationContext(),"Please Fill All The Fields", Toast.LENGTH_LONG).show();
                }


            }
        });
    }

    private void AddBR(final String name) {
        progressDialog.show();

//        String url = "http://139.59.26.186/auth/branches";

        StringRequest request = new StringRequest(com.android.volley.Request.Method.POST, URLS.AddBranch, new com.android.volley.Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                progressDialog.dismiss();
                common_dialog.ClearThings();

                Toast.makeText(getApplicationContext(),response,Toast.LENGTH_LONG).show();
            }
        }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.dismiss();
                common_dialog.ClearThings();
                Toast.makeText(getApplicationContext(),"error "+error,Toast.LENGTH_LONG).show();

            }
        }){
            @Override
            public String getBodyContentType() {
                return "application/x-www-form-urlencoded; charset=UTF-8";
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> header = new HashMap<>();
                header.put("token",preferences.getString("TOKEN",""));
                return header;
            }

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> param = new HashMap<>();
                param.put("cbname",name);
                param.put("course", common_dialog.UNIQUEC);

                return param;
            }
        };
        requestQueue.add(request);
    }

    private void ChooseCourse(final Button button) {


//        disable = true;
//        String url = "http://139.59.26.186/course";

                final ListView listView;

        final    Dialog   cdialog = new Dialog(this,R.style.Theme_AppCompat_DayNight_Dialog_Alert);

        cdialog.setContentView(R.layout.courses);

        listView = (ListView) cdialog.findViewById(R.id.clist);
        final ProgressBar Bar = cdialog.findViewById(R.id.progressBar10);

        cdialog.show();

        JsonObjectRequest request = new JsonObjectRequest(com.android.volley.Request.Method.GET, URLS.Course, null, new com.android.volley.Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {

                Bar.setVisibility(View.INVISIBLE);

                try {
                    boolean success = response.getBoolean("success");

                    if (success){

                        JSONArray array = response.getJSONArray("Name");
                        for (int i = 0; i < array.length(); i++){
                            JSONObject object = array.getJSONObject(i);

                            String courses = object.getString("name");

                            cbpojo = new CBPOJO(courses);
                          cbpojos.add(cbpojo);

                            adapter = new CorseAdapter(cbpojos, fileuploads.this);


                            listView.setAdapter(adapter);

//                            progressBar.setVisibility(View.GONE);

                        }
                    }

                } catch (JSONException e) {
                    Bar.setVisibility(View.INVISIBLE);
                    e.printStackTrace();
                }

            }
        }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
//                disable = false;
//                progressBar.setVisibility(View.GONE);
                Bar.setVisibility(View.INVISIBLE);

                Toast.makeText(getApplicationContext(),"Volley "+error,Toast.LENGTH_LONG).show();

            }
        });

        requestQueue.add(request);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

//                disable = false;
                cbpojo = (CBPOJO) listView.getItemAtPosition(i);
                String course = cbpojo.getCbpojo();
                Toast.makeText(getApplicationContext(),course,Toast.LENGTH_LONG).show();
                button.setText(course);
                Acourse = course;
                cbpojos.clear();
                cdialog.dismiss();
            }
        });

        cdialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialogInterface) {
//                disable = false;
                cbpojos.clear();
            }
        });
    }

    private void showPopup() {

        final EditText tname, tsub, tpass, tmo, temail;
        final Button cancel, submit, course, branch, sub, mail, year;
        dialog.setContentView(R.layout.addteacher);

        tname = dialog.findViewById(R.id.tname);
//        tsub = dialog.findViewById(R.id.tsub);
        tmo = dialog.findViewById(R.id.editText9);
        temail = dialog.findViewById(R.id.editText10);
        tpass = dialog.findViewById(R.id.tpass);


        course = dialog.findViewById(R.id.button22);
        branch = dialog.findViewById(R.id.button23);
        sub = dialog.findViewById(R.id.button23sub);
        mail = dialog.findViewById(R.id.button23em);
        year = dialog.findViewById(R.id.button23year);
        cancel = dialog.findViewById(R.id.cancel);
        submit = dialog.findViewById(R.id.submit);

        dialog.show();

        year.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (common_dialog.UNIQUEC.isEmpty()){
                    Toast.makeText(getApplicationContext(), "Please Choose Course",Toast.LENGTH_LONG).show();

                }else{
                    common_dialog.LoadYear(dialogaddcb, fileuploads.this, requestQueue, year);
                }
            }
        });

        sub.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!common_dialog.UNIQUEB.isEmpty()){
                    common_dialog.ChooseSubject(dialogaddcb, fileuploads.this,requestQueue, sub);
                }else{
                    Toast.makeText(getApplicationContext(), "Please Choose All Fields",Toast.LENGTH_LONG).show();

                }
            }
        });

        mail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (temail.getText().equals("")|| tpass.getText().equals("")){
                    Toast.makeText(getApplicationContext(),"Please Enter Email and Password",Toast.LENGTH_LONG).show();


                }else{
                    sendMail(temail.getText().toString(), tpass.getText().toString());

                }

            }
        });

        course.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                ChooseCourse(course);
                common_dialog.ChooseCourse(dialogaddcb,fileuploads.this,requestQueue,course );

            }
        });

        branch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (common_dialog.UNIQUEC.equals("")){
                    Toast.makeText(getApplicationContext(), "Please Choose Course",Toast.LENGTH_LONG).show();
                }else{
//                    ChooseBranch(branch);
                    common_dialog.ChooseBranch(dialogaddcb,fileuploads.this,requestQueue, branch);
                }
            }
        });

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String name, sub, pass, no,email;
                name = tname.getText().toString().trim();
                pass = tpass.getText().toString().trim();
                no = tmo.getText().toString().trim();
                email = temail.getText().toString().trim();
          if (name.isEmpty() || common_dialog.UNIQUES.isEmpty() || pass.isEmpty() || common_dialog.UNIQUEC.equals("") || common_dialog.UNIQUEB.equals("") || common_dialog.UNIQUEY.equals("")){
              Toast.makeText(getApplicationContext(), "Please Fill All The Fields",Toast.LENGTH_LONG).show();

          }else {
              addTeacher(name, pass, common_dialog.UNIQUES , no, email, common_dialog.UNIQUEC, common_dialog.UNIQUEB);

              dialog.dismiss();
          }

            }
        });

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                common_dialog.ClearThings();
                dialog.dismiss();
            }
        });

    }

    private void sendMail(final String email, final String pwd) {

        progressDialog.show();
        StringRequest request = new StringRequest(com.android.volley.Request.Method.POST, URLS.FirstR+"unique/semail", new com.android.volley.Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
progressDialog.dismiss();

                if (response.equals("success")){

                    Toast.makeText(getApplicationContext(),"Email Send Succefully",Toast.LENGTH_LONG).show();

                }else{

                    Toast.makeText(getApplicationContext(),response,Toast.LENGTH_LONG).show();

                }


            }
        }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
progressDialog.dismiss();
                Toast.makeText(getApplicationContext(),"Error "+error,Toast.LENGTH_LONG).show();

            }
        }){
            @Override
            public String getBodyContentType() {
                return "application/x-www-form-urlencoded; charset=UTF-8";
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> header = new HashMap<>();
                header.put("token",token);
                return header;
            }

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> param = new HashMap<>();
                param.put("email",email);
                param.put("pwd", pwd);


                return param;
            }
        };
        requestQueue.add(request);

    }

    private void clearData(){
        Acourse = "";
        Abranch = "";
   }
    private void addTeacher(final String name, final String pass, final String sub, final String no, final String email, final String course, final String branch) {

//        String url = "https://d9060b79.ngrok.io/teacher";
//        String url = "http://6ba92598.ngrok.io/auth/addteacher";
        final String token = preferences.getString("TOKEN","");
        progressDialog.show();

        StringRequest request = new StringRequest(com.android.volley.Request.Method.POST, URLS.AddTeacher, new com.android.volley.Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                common_dialog.ClearThings();
                Toast.makeText(getApplicationContext(),"Added Successfully.",Toast.LENGTH_LONG).show();
                progressDialog.dismiss();
            }
        }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                common_dialog.ClearThings();
                Toast.makeText(getApplicationContext(),""+error,Toast.LENGTH_LONG).show();
                progressDialog.dismiss();
            }
        }){

            @Override
            public String getBodyContentType() {
                return "application/x-www-form-urlencoded; charset=UTF-8";
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> header = new HashMap<>();
                header.put("token",token);
                return header;
            }

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> param = new HashMap<>();
                param.put("tname",name);
                param.put("pass",pass);
                param.put("sub",sub);
                param.put("no",no);
                param.put("email",email);
                param.put("course",course);
                param.put("branch",branch);
                param.put("year", common_dialog.UNIQUEY);

                return param;
            }
        };
        requestQueue.add(request);
    }

    private void init() {

        choose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (permissiongrant){
                    filechooser();
                }else{
                    requestStorage();
                }

            }
        });


    }
    public static byte[] convertFileToByteArray(File f)
    {
        byte[] byteArray = null;
        try
        {
            InputStream inputStream = new FileInputStream(f);
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            byte[] b = new byte[1024*8];
            int bytesRead =0;

            while ((bytesRead = inputStream.read(b)) != -1)
            {
                bos.write(b, 0, bytesRead);
            }

            byteArray = bos.toByteArray();
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
        return byteArray;
    }
    private void uploadFiles() {
        final String path = FilePath.getPath(this, filepath);
        final File file = new File(path);

        progressDialog.show();
        VolleyMultipartRequest request = new VolleyMultipartRequest(com.android.volley.Request.Method.POST, UPLOAD_URL,
                new com.android.volley.Response.Listener<NetworkResponse>() {
                    @Override
                    public void onResponse(NetworkResponse response) {
                        Toast.makeText(getApplicationContext(),"res "+response,Toast.LENGTH_LONG).show();
                      progressDialog.dismiss();
                    }
                }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getApplicationContext(),"error "+error,Toast.LENGTH_LONG).show();
              progressDialog.dismiss();
            }
        }){
            @Override
            protected Map<String, DataPart> getByteData() throws AuthFailureError {
                Map<String, DataPart> params = new HashMap<>();
                params.put("userFile", new DataPart(path, convertFileToByteArray(file),"application/pdf"));
                return params;
            }
        };
     request.setRetryPolicy(new DefaultRetryPolicy(15000,
             DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
     requestQueue.add(request);
    }


    private void uploadFile(String name, String sub, String uname) {
        final Handler handler = new Handler();
        progressDialog.show();

//        String path = FilePath.getPath(this, filepath);
        builder.setProgress(0,0,true);
        notificationManager.notify(ID,builder.build());
       final File mfile = new File(path);
       if (!mfile.exists()){
           Toast.makeText(getApplicationContext(),"File not exists", Toast.LENGTH_LONG).show();
       }
       Log.e("path"," "+path);

        try{
            final MediaType mediaType = MediaType.parse("application/pdf");
            RequestBody requestBody = new MultipartBody.Builder()
                    .setType(MultipartBody.FORM)
                    .addFormDataPart("pdname", name)
                    .addFormDataPart("sub", common_dialog.UNIQUES)
                    .addFormDataPart("chapter", common_dialog.UNIQUECHA)
                    .addFormDataPart("sem",CURRENT_SAME)
                    .addFormDataPart("uploader",uname)
                    .addFormDataPart("course",common_dialog.UNIQUEC)
                    .addFormDataPart("branch",common_dialog.UNIQUEB)
                    .addFormDataPart("year",common_dialog.UNIQUEY)
                    .addFormDataPart("subid", common_dialog.UNIQUEID)
                    .addFormDataPart("userFile", path, RequestBody.create(mediaType, convertFileToByteArray(mfile))).build();

            Log.e("Data", "pdfname "+name+" sub "+sub+" uploader "+uname);
//            Toast.makeText(getApplicationContext(),"pdfname "+name+" sub "+sub+" uploader "+uname, Toast.LENGTH_LONG).show();

             Request request = new Request.Builder()
                     .url(UPLOAD_URL)
                     .post(requestBody)
                     .build();

            OkHttpClient client = new OkHttpClient.Builder()
                    .connectTimeout(5, TimeUnit.MINUTES)
                    .readTimeout(5, TimeUnit.MINUTES)
                    .writeTimeout(5, TimeUnit.MINUTES)
                    .build();

//            OkHttpClient.Builder builder = new OkHttpClient.Builder();
//                         builder.readTimeout(5, TimeUnit.MINUTES);
//                         builder.writeTimeout(5, TimeUnit.MINUTES);
//                         builder.connectTimeout(5, TimeUnit.MINUTES);
//                  client = builder.build();
                         client.newCall(request).enqueue(new Callback() {
                             @Override
                             public void onFailure(Call call, final IOException e) {
                                 common_dialog.ClearThings();
                               handler.post(new Runnable() {
                                   @Override
                                   public void run() {

                                       builder.setContentText("upload Failed");
                                       builder.setProgress(0,0,false);
                                       notificationManager.notify(ID,builder.build());
                                       Toast.makeText(getApplicationContext(),"res "+e.toString(),Toast.LENGTH_LONG).show();
                                   }
                               });
                                 progressDialog.dismiss();
                             }

                             @Override
                             public void onResponse(Call call, final Response response) throws IOException {
                                     common_dialog.ClearThings();
                                 if (response.isSuccessful()){
                                     handler.post(new Runnable() {
                                         @Override
                                         public void run() {

                                             builder.setContentText("upload success");
                                             builder.setProgress(100,100,false);
                                             notificationManager.notify(ID,builder.build());
                                             try {
                                                 Toast.makeText(getApplicationContext(),"res "+response.body().string(),Toast.LENGTH_LONG).show();

                                             } catch (IOException e) {
                                                 e.printStackTrace();
                                             }

                                         }
                                     });
                                 }else{
                                     handler.post(new Runnable() {
                                         @Override
                                         public void run() {

                                             builder.setContentText("upload Failed");
                                             builder.setProgress(100,100,false);
                                             notificationManager.notify(ID,builder.build());
                                             try {
                                                 Toast.makeText(getApplicationContext(),"res "+response.body().string(),Toast.LENGTH_LONG).show();
                                             } catch (IOException e) {
                                                 e.printStackTrace();
                                             }

                                         }
                                     });
                                 }

                                 progressDialog.dismiss();
                             }
                         });

        }catch (Exception e){
            progressDialog.dismiss();
            Toast.makeText(getApplicationContext(),"Error "+e,Toast.LENGTH_LONG).show();

        }
    }

//    private void uploadFile() {
//
//         String name = namea.getText().toString().trim();
//         String path = FilePath.getPath(this, filepath);
//        Toast.makeText(getApplicationContext(),"Error "+name,Toast.LENGTH_LONG).show();
//        if (path == null){
//             Toast.makeText(getApplicationContext(),"null",Toast.LENGTH_LONG).show();
//         }else{
//             try{
//                 String uploadid = UUID.randomUUID().toString();
//                  new MultipartUploadRequest(this, uploadid, UPLOAD_URL)
//                          .addFileToUpload(path, "userFile")
//                          .addParameter("pdf", name)
//                          .setNotificationConfig(new UploadNotificationConfig())
//                          .setMaxRetries(0)
//                          .startUpload();
//
//             }catch (Exception e){
//
//                 Toast.makeText(getApplicationContext(),"Error "+e,Toast.LENGTH_LONG).show();
//             }
//         }
//    }

    private void filechooser() {
//        Intent intent = new Intent();
//        intent.setType("application/pdf");
//        intent.setAction(Intent.ACTION_GET_CONTENT);
//        startActivityForResult(Intent.createChooser(intent,"choose file"),PDF_CHOOSER);

//        Intent intent = new Intent(this, FilePickerActivity.class);
//        startActivityForResult(intent, PDF_CHOOSER);

        new MaterialFilePicker()
                .withActivity(this)
                .withRequestCode(PDF_CHOOSER)
                .withFilter(Pattern.compile(".*\\.pdf$")) // Filtering files and directories by file name using regexp
                .start();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode == PDF_CHOOSER && resultCode == RESULT_OK ){
//            upload.setVisibility(View.VISIBLE);
            fileMetadata();
//            flname.setVisibility(View.VISIBLE);
//            filepath = data.getData();
            path = data.getStringExtra(FilePickerActivity.RESULT_FILE_PATH);
//            String fpath = FilePath.getPath(this,filepath);
//            String filename = fpath.substring(fpath.lastIndexOf("/")+1);
//
//            flname.setText("Selected File: "+filename);
//            Toast.makeText(getApplicationContext(),"name "+filename+" fpath "+fpath,Toast.LENGTH_LONG).show();
        }
    }

    private void fileMetadata() {
        final EditText name,  uname;
        final Button chose, cancel, submit, course, branch, year, sub, chapter;
        CURRENT_SAME = "";
        Filedata.setContentView(R.layout.filenaming);

        name = Filedata.findViewById(R.id.editText2);
//        sub = Filedata.findViewById(R.id.editText3);
        uname = Filedata.findViewById(R.id.editText4);

        chose = Filedata.findViewById(R.id.button13);
        course = Filedata.findViewById(R.id.button20);
        branch = Filedata.findViewById(R.id.button21);
        sub = Filedata.findViewById(R.id.button23s);
        chapter = Filedata.findViewById(R.id.button23c);
        cancel = Filedata.findViewById(R.id.button15);
        submit = Filedata.findViewById(R.id.button16);
        year = Filedata.findViewById(R.id.button23yy);
        Filedata.show();

        chapter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (common_dialog.UNIQUES.isEmpty()){
                    Toast.makeText(getApplicationContext(),"Please choose Subject",Toast.LENGTH_LONG).show();

                }else{
                    common_dialog.ChooseChapter(dialog, fileuploads.this, requestQueue, chapter);
                }
            }
        });

        sub.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (common_dialog.UNIQUEC.isEmpty() || common_dialog.UNIQUEB.isEmpty() || common_dialog.UNIQUEY.isEmpty()){
                    Toast.makeText(getApplicationContext(),"Please choose All Fields",Toast.LENGTH_LONG).show();

                }else{

                    common_dialog.ChooseSubject(dialog,fileuploads.this,requestQueue,sub);
                }
            }
        });

        course.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                common_dialog.ChooseCourse(dialog, fileuploads.this, requestQueue, course);
//                ChooseCourse(course);
            }
        });

        branch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
              if (common_dialog.UNIQUEC.equals("")){
                  Toast.makeText(getApplicationContext(),"Please choose Course",Toast.LENGTH_LONG).show();
              }else{

                  common_dialog.ChooseBranch(dialog,fileuploads.this, requestQueue, branch);
//                  ChooseBranch(branch);
              }
            }
        });

        year.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            if (common_dialog.UNIQUEB.isEmpty()){
                Toast.makeText(getApplicationContext(),"Please choose Course",Toast.LENGTH_LONG).show();

            }else{
                common_dialog.LoadYear(dialog, fileuploads.this, requestQueue, year);
            }

            }

        });


        chose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ChosePopup(chose);
            }
        });

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (CURRENT_SAME.isEmpty() || CURRENT_SAME.equals("") || common_dialog.UNIQUEC.equals("") || common_dialog.UNIQUEB.equals("") || common_dialog.UNIQUECHA.equals("")){
                    Toast.makeText(getApplicationContext(),"Please Select All Fields.",Toast.LENGTH_LONG).show();
                }else{
                    notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
                    builder = new NotificationCompat.Builder(fileuploads.this);
                    builder.setContentTitle("Uploading")
                            .setContentText("your pdf file is uploading")
                            .setSmallIcon(R.drawable.default_artwork);
                    String na, su, un;
                    na = name.getText().toString();
                    un = uname.getText().toString();
                    if (na.isEmpty() || common_dialog.UNIQUES.isEmpty() || un.isEmpty() || common_dialog.UNIQUECHA.equals("")){
                        Toast.makeText(getApplicationContext()," Please Input All The Field", Toast.LENGTH_LONG).show();
                    }else{
                        uploadFile(na,  common_dialog.UNIQUES, un);
                       clearData();
                        Filedata.dismiss();
                    }

                }

            }
        });

       cancel.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View view) {
               common_dialog.ClearThings();
               Filedata.dismiss();
           }
       });

    }

    private void ChooseBranch(final Button branch) {

//        disable = true;

//        String url = "http://139.59.26.186/branch";
        final ListView listView;
        final    Dialog   cdialog = new Dialog(this,R.style.Theme_AppCompat_DayNight_Dialog_Alert);

        cdialog.setContentView(R.layout.courses);

        listView = (ListView) cdialog.findViewById(R.id.clist);
        final   ProgressBar Bar = cdialog.findViewById(R.id.progressBar10);
//        String[] courses = new String[]{
//                "Polytechnic",
//                "Engineering",
//                "D-Pharmacy",
//                "B-Pharmacy"
//        };
//        ArrayList<String> list = new ArrayList<>();
//
//        list.add("Polytechnic");
//        list.add("Engineering");
//        list.add("D-Pharmacy");
//        list.add("B-Pharmacy");

cdialog.show();
        HashMap<String, String> para = new HashMap<>();
        para.put("course",Acourse);


        final JsonObjectRequest request = new JsonObjectRequest(com.android.volley.Request.Method.POST, URLS.Branch, new JSONObject(para), new com.android.volley.Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {

                Bar.setVisibility(View.GONE);

//                disable = false;
                try {
                    boolean success = response.getBoolean("success");
                    JSONArray object = response.getJSONArray("cbranch");

                    if (success){

                        for (int i = 0; i<object.length(); i++){
                            JSONObject branches = object.getJSONObject(i);
                            JSONArray array = branches.getJSONArray("branches");

                            if (array.length() <= 0){

                                Toast.makeText(getApplicationContext(),"No branch added",Toast.LENGTH_LONG).show();

                            }else {
                                for (int b=0; b<array.length(); b++){
                                    JSONObject object1 = array.getJSONObject(b);

                                    String branch = object1.getString("name");

                                    cbpojo = new CBPOJO(branch);
                                    cbpojos.add(cbpojo);

                                    adapter = new CorseAdapter(cbpojos, fileuploads.this);

//                            ArrayAdapter<String> adapter = new ArrayAdapter<String>(signup.this,android.R.layout.simple_list_item_1,cbpojos);


                                    listView.setAdapter(adapter);


                                }
                            }
                        }

                    }else{
                        Bar.setVisibility(View.GONE);
                        String error = response.getString("Error");
                        Toast.makeText(getApplicationContext(),error,Toast.LENGTH_LONG).show();
                    }

                } catch (JSONException e) {
                    Bar.setVisibility(View.GONE);
                    e.printStackTrace();
                    Toast.makeText(getApplicationContext(),"Exeption "+e,Toast.LENGTH_LONG).show();

                }


            }
        }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                Bar.setVisibility(View.GONE);
//                disable = false;
//                progressBar.setVisibility(View.GONE);
                Toast.makeText(getApplicationContext(),"Volley "+error,Toast.LENGTH_LONG).show();

            }
        });

        requestQueue.add(request);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

//                disable = false;
                cbpojo = (CBPOJO) listView.getItemAtPosition(i);
                String course = cbpojo.getCbpojo();
                Toast.makeText(getApplicationContext(),course,Toast.LENGTH_LONG).show();
                branch.setText(course);
                 Abranch = course;
                cbpojos.clear();
                cdialog.dismiss();
            }
        });

        cdialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialogInterface) {
//                disable = false;
                cbpojos.clear();
            }
        });
    }

    private void ChosePopup(final Button b) {
        final RadioGroup group;
        final RadioButton button;
        Button submit;

        SemChoose.setContentView(R.layout.choosesem);

        group = SemChoose.findViewById(R.id.semgroup);
        submit = SemChoose.findViewById(R.id.button14);

        CURRENT_SAME = "";
        group.clearCheck();
        SemChoose.show();

        group.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                switch (i){
                    case R.id.radioButton:
                        CURRENT_SAME = "sem1";
                        b.setText("semester-1");
                        break;
                    case R.id.radioButton2:
                        CURRENT_SAME = "sem2";
                        b.setText("semester-2");

                        break;
                    case R.id.radioButton3:
                        CURRENT_SAME = "sem3";
                        b.setText("semester-3");

                        break;
                    case R.id.radioButton4:
                        CURRENT_SAME = "sem4";
                        b.setText("semester-4");

                        break;
                    case R.id.radioButton5:
                        CURRENT_SAME = "sem5";
                        b.setText("semester-5");

                        break;
                    case R.id.radioButton6:
                        CURRENT_SAME = "sem6";
                        b.setText("semester-6");

                        break;
                    case R.id.radioButton7:
                        CURRENT_SAME = "sem7";
                        b.setText("semester-7");

                        break;
                    case R.id.radioButton8:
                        CURRENT_SAME = "sem8";
                        b.setText("semester-8");

                        break;
                    default:
                        CURRENT_SAME = "";
                        break;
                }

            }
        });

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                SemChoose.dismiss();
            }
        });

    }

    private void requestStorage(){

        String[] permission = {Manifest.permission.WRITE_EXTERNAL_STORAGE};

        if(ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED){
            permissiongrant = true;
        }else{
            ActivityCompat.requestPermissions(this, permission, STORAGE_PER);
            permissiongrant = false;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if( requestCode == STORAGE_PER){
            if(grantResults.length >0 && grantResults[0] == PackageManager.PERMISSION_GRANTED){
                permissiongrant = true;
            }else {
                permissiongrant = false;
            }
        }
    }
}
