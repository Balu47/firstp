package sonawane2.balu.com.firstp;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.provider.Settings;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Patterns;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;
import java.util.Random;

/**
 * Created by shine on 04-Feb-18.
 */

public class login extends AppCompatActivity {
    private Button login, signup;
    private EditText number, numCopy;
    private RequestQueue requestQueue;
    private String androidid;
    private TextView signme, loginn, vari;
    private SharedPreferences preferences, Showpref;
    private ProgressDialog dialog;
    private SharedPreferences.Editor scrennEdito, ed;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login);

        login= findViewById(R.id.login);
        signup = findViewById(R.id.signup);
        number = findViewById(R.id.editText);
        numCopy = findViewById(R.id.editTextcopy);
        signme = findViewById(R.id.textView8);
        loginn = findViewById(R.id.textView21);
        vari = findViewById(R.id.textView26);

        requestQueue = Volley.newRequestQueue(this);
        androidid = Settings.Secure.getString(getApplicationContext().getContentResolver(), Settings.Secure.ANDROID_ID);

        preferences = this.getSharedPreferences("com.example.SHOW",this.MODE_PRIVATE);
        Showpref = this.getSharedPreferences("com.example.SHOW_SCREEN",this.MODE_PRIVATE);
        scrennEdito = Showpref.edit();
        ed = preferences.edit();
        dialog = new ProgressDialog(this);
        dialog.setMessage("please waite...");


        boolean show = preferences.getBoolean("show",false);

        if (show){
            Intent intent= new Intent(this,MainPage.class);
            startActivity(intent);
            finish();
        }

        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                LogIn();
            }
        });

        signup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                SignUp();
            }
        });

        signme.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                numCopy.setVisibility(View.VISIBLE);
                number.setVisibility(View.GONE);
                vari.setVisibility(View.GONE);
                login.setVisibility(View.GONE);
                signup.setVisibility(View.VISIBLE);
                signme.setVisibility(View.GONE);
                loginn.setVisibility(View.VISIBLE);
            }
        });

        loginn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                numCopy.setVisibility(View.GONE);
                number.setVisibility(View.VISIBLE);
                vari.setVisibility(View.VISIBLE);
                login.setVisibility(View.VISIBLE);
                signup.setVisibility(View.GONE);
                signme.setVisibility(View.VISIBLE);
                loginn.setVisibility(View.GONE);
            }
        });

        vari.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(login.this, Send_again.class);
                startActivity(intent);
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();

    }

//    private void SignUp() {
//        final String no = number.getText().toString();
//
//        if (!no.isEmpty()){
//            String uri = "https://my-exapp.herokuapp.com/signup";
//
//            StringRequest request = new StringRequest(Request.Method.POST, uri, new Response.Listener<String>() {
//                @Override
//                public void onResponse(String response) {
//                    String res = response;
//                    SharedPreferences.Editor editor = preferences.edit();
//                    if (response.equals("success")){
//                        editor.putBoolean("show",true);
//                        editor.commit();
//                        Intent intent = new Intent(login.this, signup.class);
//                        startActivity(intent);
//                    }
//                    Toast.makeText(getApplicationContext(),"res "+res,Toast.LENGTH_LONG).show();
//
//                }
//            }, new Response.ErrorListener() {
//                @Override
//                public void onErrorResponse(VolleyError error) {
//                    Toast.makeText(getApplicationContext(),"error"+error,Toast.LENGTH_LONG).show();
//
//                }
//            }){
//                @Override
//                public String getBodyContentType() {
//                    return "application/x-www-form-urlencoded; charset=UTF-8";
//                }
//
//                @Override
//                protected Map<String, String> getParams() throws AuthFailureError {
//                    Map<String, String> param = new HashMap<>();
//                    param.put("ep",no);
//                    param.put("aid",androidid);
//
//                    return param;
//                }
//            };
//
//            requestQueue.add(request);
//        }
//    }

//    private void LogIn() {
//        final String no = number.getText().toString();
//
//        if (!no.isEmpty()){
//            String url = "https://my-exapp.herokuapp.com/slogin";
//
//            StringRequest request = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
//                @Override
//                public void onResponse(String response) {
//                    String res = response;
//                    SharedPreferences.Editor editor = preferences.edit();
//                    if (response.equals("signup")){
//
//                    }else if(response.equals("success")){
//
//                        editor.putBoolean("show",true);
//                        editor.commit();
//                        Intent intent = new Intent(login.this, signup.class);
//                        startActivity(intent);
//                    }
//                    Toast.makeText(getApplicationContext(),"res "+res,Toast.LENGTH_LONG).show();
//
//                }
//            }, new Response.ErrorListener() {
//                @Override
//                public void onErrorResponse(VolleyError error) {
//                    Toast.makeText(getApplicationContext(),"error"+error,Toast.LENGTH_LONG).show();
//
//                }
//            }){
//                @Override
//                public String getBodyContentType() {
//                    return "application/x-www-form-urlencoded; charset=UTF-8";
//                }
//
//                @Override
//                protected Map<String, String> getParams() throws AuthFailureError {
//                    Map<String, String> param = new HashMap<>();
//                    param.put("ep",no);
//                    param.put("aid",androidid);
//
//                    return param;
//                }
//            };
//
//            requestQueue.add(request);
//        }
//    }

    private void LogIn(){
        final String no = number.getText().toString();
//        String url = "http://024ac872.ngrok.io/unique/slogin";
        String url = URLS.Unique;
        if (no.isEmpty() || no.equals("")){

            Toast.makeText(getApplicationContext(),"Email Cannot be Empty",Toast.LENGTH_LONG).show();

        }else{
            Dialogtog();
            HashMap<String, String> para = new HashMap<>();
            para.put("ep",no);
            para.put("aid",androidid);



            final JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, url, new JSONObject(para), new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {

                    Dialogtog();
                    SharedPreferences.Editor editor = preferences.edit();

                    try {
                        boolean success = response.getBoolean("success");
                        String error = response.getString("error");
                        if (success){
                            String uid = response.getString("uid");
                            String token = response.getString("token");
                scrennEdito.putString("goto","student");
                scrennEdito.putBoolean("show", false);
                scrennEdito.commit();
                            editor.putBoolean("show",true);
                            editor.putString("UID", "\""+uid+"\"");
                            editor.putString("STOKEN", token);
                            editor.putBoolean("login", true);
                            editor.commit();
                            Intent intent = new Intent(login.this, MainPage.class);
                            startActivity(intent);
                            finish();
//                            Toast.makeText(getApplicationContext(),uid,Toast.LENGTH_LONG).show();

                        }else {
                            Toast.makeText(getApplicationContext(),error,Toast.LENGTH_LONG).show();
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                        Toast.makeText(getApplicationContext(),"Exeption "+e,Toast.LENGTH_LONG).show();

                    }


                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {

                    Dialogtog();
                    Toast.makeText(getApplicationContext(),"Volley "+error,Toast.LENGTH_LONG).show();

                }
            });

            requestQueue.add(request);
        }
    }

    private void Dialogtog(){
        if (dialog.isShowing()){
            dialog.dismiss();
        }else{
            dialog.show();
        }
    }

    private void SignUp(){

        final String no = numCopy.getText().toString();
//        String url = "http://024ac872.ngrok.io/unique/signup";
        Random random = new Random();
        final int code = random.nextInt((10000 - 1000) + 1) + 1000;
       boolean valid = isValidEmail(no);

      if (no.isEmpty()){

          Toast.makeText(getApplicationContext(),"Email Cannot be Empty", Toast.LENGTH_LONG).show();

      }else if (valid){
          Dialogtog();
          HashMap<String, String> para = new HashMap<>();
          para.put("ep",no);
          para.put("aid",androidid);
          para.put("otpcode", String.valueOf(code));

          final JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, URLS.Signp, new JSONObject(para), new Response.Listener<JSONObject>() {
              @Override
              public void onResponse(JSONObject response) {
                  Dialogtog();
                  SharedPreferences.Editor editor = preferences.edit();

                  try {
                      boolean success = response.getBoolean("success");
                      String error = response.getString("error");
                      if (success){
                          String uid = response.getString("uid");
//
//
                          editor.putBoolean("show",true);
                          editor.putString("UID", uid);
                          editor.commit();
//                          Intent intent = new Intent(login.this, signup.class);
//                          startActivity(intent);
                          Toast.makeText(getApplicationContext(),"Your Successfully Registered Please Verify Email...",Toast.LENGTH_LONG).show();


                          Intent intent = new Intent(login.this, Varify_email.class);
                          intent.putExtra("update", false);
                          startActivity(intent);
                          finish();

                      }else {
                          if (error.equals("success")){

                              String uid = response.getString("uid");
//
//
                              editor.putBoolean("show",true);
                              editor.putString("UID", uid);
                              editor.commit();
//                          Intent intent = new Intent(login.this, signup.class);
//                          startActivity(intent);
                              Toast.makeText(getApplicationContext(),"Your Successfully Registered Please Verify Email...",Toast.LENGTH_LONG).show();


                              Intent intent = new Intent(login.this, Varify_email.class);
                              intent.putExtra("update", false);
                              startActivity(intent);
                              finish();
                          }else{

                              Toast.makeText(getApplicationContext(),error,Toast.LENGTH_LONG).show();

                          }
                      }

                  } catch (JSONException e) {
                      e.printStackTrace();
                      Toast.makeText(getApplicationContext(),"Exeption "+e,Toast.LENGTH_LONG).show();

                  }


              }
          }, new Response.ErrorListener() {
              @Override
              public void onErrorResponse(VolleyError error) {

                  Dialogtog();
                  Toast.makeText(getApplicationContext(),"Volley "+error,Toast.LENGTH_LONG).show();

              }
          });

          requestQueue.add(request);
      }else{
          Toast.makeText(getApplicationContext(),"Email is not Valid", Toast.LENGTH_LONG).show();
      }

    }

    public final static boolean isValidEmail(CharSequence target) {
        return (!TextUtils.isEmpty(target) && Patterns.EMAIL_ADDRESS.matcher(target).matches());
    }
}
