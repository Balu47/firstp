package sonawane2.balu.com.firstp;

/**
 * Created by shine on 27-Feb-18.
 */

public class URLS {

    public static  final String FirstR = "https://b8c8825b.ngrok.io/";


    public static  final String Unique = FirstR + "unique/slogin";

    public static  final String Signp = FirstR + "unique/signup";
    public static  final String AddStudents = FirstR + "students";

    public static  final String Course = FirstR + "course";
    public static  final String AddCourse = FirstR + "auth/course";
    public static  final String DelCourse = FirstR + "del/course";

    public static  final String Branch = FirstR + "branch";
    public static  final String AddBranch = FirstR + "auth/branches";
    public static  final String DelBranch = FirstR + "del/branch";

    public static  final String Admin = FirstR + "auth/admin";

    public static  final String Teacher = FirstR + "auth/teacherlog";

    public static  final String Apdfs = FirstR + "auth/allist";

    public static  final String Spdfs = FirstR + "list";

    public static  final String ListTeachers = FirstR + "auth/teachers";
    public static  final String AddTeacher = FirstR + "auth/addteacher";
    public static  final String DelTeacher = FirstR + "del/teacher";

    public static final String ListStudents = FirstR + "auth/students";
    public static final String CheckBlock = FirstR + "del/checkBlock";
    public static  final String Block = FirstR + "del/bstudent";
    public static  final String Unblock = FirstR + "del/ubstudent";

    public static final String DeletePdf = FirstR + "del/delete";
    public static final String About = FirstR + "address";

    public static final String VCode = FirstR + "unique/vemail";
    public static final String VCodeagain = FirstR + "unique/cagian";

    public static  final String AddSub = FirstR + "auth/addsub";
    public static  final String GetSub = FirstR + "auth/sub";
    public static  final String GetSSub = FirstR + "sub";
    public static final String DelSub = FirstR + "del/sub";
    public static  final String AddChap = FirstR + "auth/addch";
    public static  final String GetChap = FirstR + "auth/cha";
    public static final String DelChap = FirstR + "del/chap";

//    public static  final String FirstR = "";


}

