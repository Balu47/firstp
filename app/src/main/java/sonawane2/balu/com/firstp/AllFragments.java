package sonawane2.balu.com.firstp;

import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;

import java.util.ArrayList;


import sonawane2.balu.com.firstp.Fragment.Docs;
import sonawane2.balu.com.firstp.Fragment.TrackPagerAdapter;
import sonawane2.balu.com.firstp.Fragment.UProfile;
import sonawane2.balu.com.firstp.Fragment.Videos;


/**
 * Created by shine on 14-Jan-18.
 */

public class AllFragments extends AppCompatActivity {
    private ArrayList<Fragment> fragments;
    private TrackPagerAdapter allFragments;


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.allfrag);

        ViewPager viewPager =(ViewPager) findViewById(R.id.viewPager);
        TabLayout tabLayout =(TabLayout) findViewById(R.id.tabLayout);

        fragments = new ArrayList<>();
        fragments.add(UProfile.newInstance());
        fragments.add(Docs.newInstance());
        fragments.add(Videos.newInstance());

        allFragments = new TrackPagerAdapter(getSupportFragmentManager(),fragments);
        viewPager.setAdapter(allFragments);
        viewPager.setCurrentItem(0);
        viewPager.setOffscreenPageLimit(3);
        tabLayout.setupWithViewPager(viewPager);

    }
}
