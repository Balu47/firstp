package sonawane2.balu.com.firstp.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import sonawane2.balu.com.firstp.R;
import sonawane2.balu.com.firstp.pixabay;

/**
 * Created by shine on 14-Jan-18.
 */

public class Docsli extends RecyclerView.Adapter<Docsli.ViewHolder> {
    Context context;
    ArrayList<pixabay> pixabays = new ArrayList<>();

    public Docsli (Context context, ArrayList<pixabay> pixabays){
        this.context = context;
        this.pixabays = pixabays;
    }

    @Override
    public Docsli.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.new_list,parent,false);

        return new Docsli.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(Docsli.ViewHolder holder, int position) {
        pixabay pos = pixabays.get(position);
        final String imuri = pos.getImagev();

        holder.tag.setText(pos.getTags());
        holder.like.setText("Likes: " + pos.getLikes());
        holder.views.setText("Views: " + pos.getView());

        Picasso.with(context).load(imuri).fit().into(holder.imageView);
    }

    @Override
    public int getItemCount() {
        return pixabays == null ? 0: pixabays.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        TextView views, like, tag;
        ImageView imageView;

        public ViewHolder(View v){
            super(v);
            views =v.findViewById(R.id.textView4);
            like = v.findViewById(R.id.textView5);
            tag = v.findViewById(R.id.textView3);
            imageView = v.findViewById(R.id.imageView3);
        }
    }
}
