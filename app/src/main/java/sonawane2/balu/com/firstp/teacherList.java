package sonawane2.balu.com.firstp;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import sonawane2.balu.com.firstp.adapter.PdfAdapter;
import sonawane2.balu.com.firstp.adapter.Student_Adapter;
import sonawane2.balu.com.firstp.adapter.teacherAdapter;

/**
 * Created by shine on 15-Feb-18.
 */

public class teacherList extends AppCompatActivity implements Student_Adapter.Student_onclick, teacherAdapter.Teacher_onclick{

    private RecyclerView.LayoutManager manager;
    private RecyclerView recyclerView;
    private ArrayList<TeacherPOJO> teacher;
    private teacherAdapter adapter ;
    private TeacherPOJO teacherpojo;
    private ArrayList<StudentPOJO> studentPOJOS;
    private StudentPOJO studentPOJO;
    private Student_Adapter student_adapter;
    private RequestQueue requestQueue;
    private ProgressBar progressBar;
    private ProgressDialog progressDialog;
    private TextView er;
    private SharedPreferences preferences;
    private String cu ="";
    private  String token;
    private Bundle bundle;
    private Dialog dialog;
    private  String branch;
    private   String year;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_SECURE,WindowManager.LayoutParams.FLAG_SECURE);

        setContentView(R.layout.tfullist);

        manager = new LinearLayoutManager(this);
        recyclerView = findViewById(R.id.tfull);
        teacher = new ArrayList<>();
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(manager);
        er = findViewById(R.id.textView13);
        studentPOJOS = new ArrayList<>();
        dialog = new Dialog(this,R.style.Theme_AppCompat_DayNight_Dialog_Alert);

        preferences = this.getSharedPreferences("com.example.TOKEN", Context.MODE_PRIVATE);
        token = preferences.getString("TOKEN","");
        progressBar = findViewById(R.id.progressBar5);
        progressBar.getIndeterminateDrawable().setColorFilter(0xFF34DA32, PorterDuff.Mode.MULTIPLY);
        recyclerView.setVisibility(View.GONE);
        er.setVisibility(View.GONE);
        requestQueue = Volley.newRequestQueue(this);

        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Please waite...");
        int i = 0;
            bundle = getIntent().getExtras();
        if (bundle != null){
             branch = bundle.getString("branch");
             year = bundle.getString("year");
//            if (teacherOrstudent){
//                loadData(branch, year);
//
//            }else {
//                LoadStudet(branch, year);
//            }

            switch (bundle.getString("where")){

                case "student" :

                    LoadStudet(branch,year);
                    break;

                case "teacher":

                    loadData(branch, year);
                    break;

                case "pdf":

                    Intent intent = new Intent(teacherList.this,PdfList.class);
                    intent.putExtra("branch", branch);
                    intent.putExtra("year", year);
                    intent.putExtra("admin", true);
                    startActivity(intent);
                    finish();
                    break;

                default:

                    Toast.makeText(getApplicationContext(), " Some Error Occured!", Toast.LENGTH_LONG).show();
            }

        }else {
            Toast.makeText(getApplicationContext(), " Some Error Occured!", Toast.LENGTH_LONG).show();
        }

    }



    private void LoadStudet(String branch, String year) {

//        String uri = "http://024ac872.ngrok.io/auth/students";
//        String uri = "http://new-exapp.openode.io/auth/list";
//        String uri = "https://e23aed20.ngrok.io/auth/list";

        HashMap<String, String> para = new HashMap<>();
        para.put("token",token);
        para.put("branch", branch);
        para.put("year", year);

        final JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, URLS.ListStudents, new JSONObject(para), new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject response) {
                progressBar.setVisibility(View.GONE);
                recyclerView.setVisibility(View.VISIBLE);
                try {

                    int count = response.getInt("count");
                    JSONArray array = response.getJSONArray("Students");

                    for( int i = 0; i < array.length(); i++){

                        JSONObject object = array.getJSONObject(i);

                        String sname, sno, scourse, sbarnch, syear, cid;
                        sname = object.getString("name");
                        sno = object.getString("no");
                        scourse = object.getString("course");
                        sbarnch = object.getString("branch");
                        syear = object.getString("year");
                        cid = object.getString("cid");

                        studentPOJO = new StudentPOJO(sname, sno, scourse, sbarnch, syear, cid);

                        studentPOJOS.add(studentPOJO);
//                       Toast.makeText(getApplicationContext(),"RESPONSE "+response,Toast.LENGTH_LONG).show();
                    }
//                       String pdftitle = object.getString("pdftitle");
//                       String pdfurl = object.getString("pdfurl");






                    student_adapter = new Student_Adapter(studentPOJOS,teacherList.this,teacherList.this);
                    recyclerView.setAdapter(student_adapter);
                    Log.e("RESPONSE",""+response);
//                    Toast.makeText(getApplicationContext(),"RESPONSE "+response,Toast.LENGTH_LONG).show();
                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(getApplicationContext(),"e "+e,Toast.LENGTH_LONG).show();
                    Log.e("E ",""+e);
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressBar.setVisibility(View.GONE);
                er.setVisibility(View.VISIBLE);
                er.setText("Error Occured");
                Log.e("ERROR",""+error);
                Toast.makeText(getApplicationContext(),"error "+error,Toast.LENGTH_LONG).show();
            }
        });
        requestQueue.add(request);
    }

    private void Dialogtog(){
        if (progressDialog.isShowing()){
            progressDialog.dismiss();
        }else{
            progressDialog.show();
        }
    }

    private void loadData(String branch, String year) {

//        String uri = "http://024ac872.ngrok.io/auth/teachers";
//        String uri = "http://new-exapp.openode.io/auth/list";
//        String uri = "https://e23aed20.ngrok.io/auth/list";

        HashMap<String, String> para = new HashMap<>();
        para.put("token",token);
        para.put("branch", branch);
        para.put("year", year);

        final JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, URLS.ListTeachers, new JSONObject(para), new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject response) {
                progressBar.setVisibility(View.GONE);
                recyclerView.setVisibility(View.VISIBLE);
                try {

                    int count = response.getInt("count");
                    JSONArray array = response.getJSONArray("TeacherList");

                    for( int i = 0; i < array.length(); i++){

                        JSONObject object = array.getJSONObject(i);

                        String tname, tsub, tcourse, tbranch, tmo, temail, tid, twd;
                        tname = object.getString("name");
                        tcourse = object.getString("course");
                        tbranch = object.getString("branch");
                        tmo = object.getString("no");
                        temail = object.getString("email");
                        tid = object.getString("_id");
                        tsub = object.getString("sub");
                        twd = object.getString("pwd");

                        teacherpojo = new TeacherPOJO(tid,tname, tsub, tcourse, tbranch, tmo, temail, twd);

                       teacher.add(teacherpojo);
//                       Toast.makeText(getApplicationContext(),"RESPONSE "+response,Toast.LENGTH_LONG).show();
                    }
//                       String pdftitle = object.getString("pdftitle");
//                       String pdfurl = object.getString("pdfurl");






                    adapter = new teacherAdapter(teacher,teacherList.this, teacherList.this);
                    recyclerView.setAdapter(adapter);
                    Log.e("RESPONSE",""+response);
//                    Toast.makeText(getApplicationContext(),"RESPONSE "+response,Toast.LENGTH_LONG).show();
                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(getApplicationContext(),"e "+e,Toast.LENGTH_LONG).show();
                    Log.e("E ",""+e);
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressBar.setVisibility(View.GONE);
                er.setVisibility(View.VISIBLE);
                er.setText("Error Occured");
                Log.e("ERROR",""+error);
                Toast.makeText(getApplicationContext(),"error "+error,Toast.LENGTH_LONG).show();
            }
        });
        requestQueue.add(request);
    }

    @Override
    public void Smyonclick(int positions, String action) {

        Student_popup(positions);
    }


    private void Student_popup(int pos) {
        dialog.setContentView(R.layout.student_dialog);

        TextView na, no, co, br, yr;
        Button cancel, block, msil;

        na = dialog.findViewById(R.id.textView15);
        no = dialog.findViewById(R.id.textView17);
        co = dialog.findViewById(R.id.textView18);
        br = dialog.findViewById(R.id.textView19);
        yr = dialog.findViewById(R.id.textView20);
        block = dialog.findViewById(R.id.button18);
        cancel = dialog.findViewById(R.id.button19);
        msil = dialog.findViewById(R.id.button18c);

        msil.setVisibility(View.GONE);

        na.setText("Name : "+studentPOJOS.get(pos).getSname());
        no.setText("No. : "+studentPOJOS.get(pos).getNo());
        co.setText("Course : "+studentPOJOS.get(pos).getCourse());
        br.setText("Branch : "+studentPOJOS.get(pos).getBranch());
        yr.setText("Year : "+studentPOJOS.get(pos).getYear());
     final   String cid =  studentPOJOS.get(pos).getCid();
        dialog.show();

        block.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                CheckBlock(cid);
                dialog.dismiss();
            }
        });

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                dialog.dismiss();
            }
        });

    }

    private void CheckBlock(final String cid) {

//        String url = "http://024ac872.ngrok.io/del/checkBlock";

        StringRequest request = new StringRequest(Request.Method.POST, URLS.CheckBlock, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                if (response.equals("true")){

                   PopUp(cid, true);
                }else if (response.equals("false")){

                    PopUp(cid, false);
                }

                Toast.makeText(getApplicationContext(),response,Toast.LENGTH_LONG).show();

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                Toast.makeText(getApplicationContext(),"Error "+error,Toast.LENGTH_LONG).show();

            }
        }){
            @Override
            public String getBodyContentType() {
                return "application/x-www-form-urlencoded; charset=UTF-8";
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> header = new HashMap<>();
                header.put("token",token);
                return header;
            }

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> param = new HashMap<>();
                param.put("cid", cid);


                return param;
            }
        };
        requestQueue.add(request);
    }

    private void PopUp(final String id, final boolean block) {

        Button delete, cancel;
        TextView message;
        dialog.setContentView(R.layout.common_dialog);

        message = dialog.findViewById(R.id.textView22);
        delete = dialog.findViewById(R.id.delete);
        cancel = dialog.findViewById(R.id.cancel);

        if (block){
            message.setText("This Student is Currently Blocked by Admin.");
            delete.setText("UNBLOCK");
        }else{
            message.setText("This Student IS Not Blocked.");
            delete.setText("BLOCK");
        }
        dialog.show();

        delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (block){

                    UnBlock(id);
                }else {

                    BlockStudent(id);
                }

                dialog.dismiss();
            }
        });

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                dialog.dismiss();
            }
        });
    }

    private void UnBlock(final String id) {

//        progressDialoge.show();
//        String url = "http://024ac872.ngrok.io/del/ubstudent";

        StringRequest request = new StringRequest(Request.Method.POST, URLS.Unblock, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

//                progressDialoge.hide();
                Toast.makeText(getApplicationContext(),response,Toast.LENGTH_LONG).show();

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
//                progressDialoge.hide();
                Toast.makeText(getApplicationContext(),"Error "+error,Toast.LENGTH_LONG).show();

            }
        }){
            @Override
            public String getBodyContentType() {
                return "application/x-www-form-urlencoded; charset=UTF-8";
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> header = new HashMap<>();
                header.put("token",token);
                return header;
            }

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> param = new HashMap<>();
                param.put("cid",id);


                return param;
            }
        };
        requestQueue.add(request);
    }

    private void BlockStudent(final String id) {

//        progressDialoge.show();
//        String url = "http://024ac872.ngrok.io/del/bstudent";

        StringRequest request = new StringRequest(Request.Method.POST, URLS.Block, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

//                progressDialoge.hide();
                Toast.makeText(getApplicationContext(),response,Toast.LENGTH_LONG).show();

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
//                progressDialoge.hide();
                Toast.makeText(getApplicationContext(),"Error "+error,Toast.LENGTH_LONG).show();

            }
        }){
            @Override
            public String getBodyContentType() {
                return "application/x-www-form-urlencoded; charset=UTF-8";
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> header = new HashMap<>();
                header.put("token",token);
                return header;
            }

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> param = new HashMap<>();
                param.put("cid",id);


                return param;
            }
        };
        requestQueue.add(request);
    }

    @Override
    public void Tmyonclick(int positions, String action) {

        showTpopun(positions);
    }

    private void showTpopun(final int pos) {

        final Dialog dialog = new Dialog(this,R.style.Theme_AppCompat_DayNight_Dialog_Alert);
        dialog.setContentView(R.layout.student_dialog);

        TextView na, no, co, br, yr;
        Button cancel, block, mail;

        na = dialog.findViewById(R.id.textView15);
        no = dialog.findViewById(R.id.textView17);
        co = dialog.findViewById(R.id.textView18);
        br = dialog.findViewById(R.id.textView19);
        yr = dialog.findViewById(R.id.textView20);
        block = dialog.findViewById(R.id.button18);
        cancel = dialog.findViewById(R.id.button19);
        mail = dialog.findViewById(R.id.button18c);

        na.setText("Name : "+teacher.get(pos).getTname());
        no.setText("email : "+teacher.get(pos).getTemail());
        co.setText("Password : "+teacher.get(pos).getTwd());
        br.setText("Course : "+teacher.get(pos).getTcourse());
        yr.setText("Branch : "+teacher.get(pos).getTbranch());

        mail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Dialogtog();
                sendMail(teacher.get(pos).getTemail(), teacher.get(pos).getTwd());
                dialog.dismiss();
            }
        });

        block.setText("Delete");

        dialog.show();

        block.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Deletepop(teacher.get(pos).getTid());
                dialog.dismiss();
            }
        });

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                dialog.dismiss();
            }
        });
    }

    private void sendMail(final String email, final String pwd) {

        StringRequest request = new StringRequest(Request.Method.POST, URLS.FirstR+"unique/semail", new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Dialogtog();

                if (response.equals("success")){

                    Toast.makeText(getApplicationContext(),"Email Send Succefully",Toast.LENGTH_LONG).show();

                }else{

                    Toast.makeText(getApplicationContext(),response,Toast.LENGTH_LONG).show();

                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Dialogtog();
                Toast.makeText(getApplicationContext(),"Error "+error,Toast.LENGTH_LONG).show();

            }
        }){
            @Override
            public String getBodyContentType() {
                return "application/x-www-form-urlencoded; charset=UTF-8";
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> header = new HashMap<>();
                header.put("token",token);
                return header;
            }

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> param = new HashMap<>();
                param.put("email",email);
                param.put("pwd", pwd);


                return param;
            }
        };
        requestQueue.add(request);

    }

    private void DeleteTeacher(final String id) {

//        String url = "http://024ac872.ngrok.io/del/teacher";

        StringRequest request = new StringRequest(Request.Method.POST, URLS.DelTeacher, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                adapter.notifyDataSetChanged();
                teacher.clear();
                loadData(branch, year);
                Toast.makeText(getApplicationContext(),response,Toast.LENGTH_LONG).show();

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                Toast.makeText(getApplicationContext(),"Error "+error,Toast.LENGTH_LONG).show();

            }
        }){
            @Override
            public String getBodyContentType() {
                return "application/x-www-form-urlencoded; charset=UTF-8";
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> header = new HashMap<>();
                header.put("token",token);
                return header;
            }

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> param = new HashMap<>();
                param.put("cid",id);


                return param;
            }
        };
        requestQueue.add(request);
    }

    private void Deletepop(final String id){
        Button delete, cancel;
        TextView message;
        dialog.setContentView(R.layout.common_dialog);

        message = dialog.findViewById(R.id.textView22);
        delete = dialog.findViewById(R.id.delete);
        cancel = dialog.findViewById(R.id.cancel);

        delete.setText("DELETE");

        dialog.show();

        delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                DeleteTeacher(id);

                dialog.dismiss();
            }
        });

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                dialog.dismiss();
            }
        });
    }
}


