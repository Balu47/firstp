package sonawane2.balu.com.firstp;

/**
 * Created by shine on 15-Feb-18.
 */

public class TeacherPOJO {

    String tname, tsub, tcourse, tbranch, tmo, temail, tid, twd;

    public TeacherPOJO(String tid, String tname, String tsub, String tcourse, String tbranch, String tmo, String temail, String twd) {
        this.tid = tid;
        this.tname = tname;
        this.tsub = tsub;
        this.tcourse = tcourse;
        this.tbranch = tbranch;
        this.tmo = tmo;
        this.temail = temail;
        this.twd = twd;
    }

    public String getTwd() {
        return twd;
    }

    public void setTwd(String twd) {
        this.twd = twd;
    }

    public String getTcourse() {
        return tcourse;
    }

    public String getTid() {
        return tid;
    }

    public void setTid(String tid) {
        this.tid = tid;
    }

    public void setTcourse(String tcourse) {
        this.tcourse = tcourse;
    }

    public String getTbranch() {
        return tbranch;
    }

    public void setTbranch(String tbranch) {
        this.tbranch = tbranch;
    }

    public String getTmo() {
        return tmo;
    }

    public void setTmo(String tmo) {
        this.tmo = tmo;
    }

    public String getTemail() {
        return temail;
    }

    public void setTemail(String temail) {
        this.temail = temail;
    }

    public String getTname() {
        return tname;
    }

    public void setTname(String tname) {
        this.tname = tname;
    }

    public String getTsub() {
        return tsub;
    }

    public void setTsub(String tsub) {
        this.tsub = tsub;
    }
}
