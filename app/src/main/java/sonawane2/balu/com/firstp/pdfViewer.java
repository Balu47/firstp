package sonawane2.balu.com.firstp;

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.WindowManager;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

/**
 * Created by shine on 12-Jan-18.
 */

public class pdfViewer extends AppCompatActivity {
    WebView webView;
    String myPdfUrl;
    private TextView er;
    ProgressDialog progressDialog;
    String url;
    private Button refresh;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_SECURE,WindowManager.LayoutParams.FLAG_SECURE);
        setContentView(R.layout.pdfview);

        er = findViewById(R.id.weberror);
        webView = findViewById(R.id.webview);
        refresh = findViewById(R.id.refresh);

//        webView = new WebView(this);
        er.setVisibility(View.GONE);
        refresh.setVisibility(View.GONE);
          Bundle bundle = getIntent().getExtras();

          if (bundle != null){
              myPdfUrl = bundle.getString("pdfurl");
          }else {
              myPdfUrl = "http://www.tutorialspoint.com/javascript/javascript_tutorial.pdf";

          }
        progressDialog = new ProgressDialog(this);

        progressDialog.setTitle("PDF Viewer");
        progressDialog.setMessage("Opening...");
        progressDialog.setCancelable(false);




          refresh.setOnClickListener(new View.OnClickListener() {
              @Override
              public void onClick(View view) {
                 LoadPdf();
              }
          });

        url = "http://docs.google.com/gview?embedded=true&url=" + myPdfUrl;
//        Toast.makeText(getApplicationContext(),myPdfUrl,Toast.LENGTH_LONG).show();
//        webView.getSettings().setPluginState(WebSettings.PluginState.ON);


       LoadPdf();
    }

//    private void CheckConn(){
//
//        if (isNetworkConnected()){
//            refresh.setVisibility(View.GONE);
//            er.setVisibility(View.GONE);
//            LoadPdf();
//        }else{
//            refresh.setVisibility(View.VISIBLE);
//            er.setVisibility(View.VISIBLE);
//            er.setText("Your Not Connected To Internet!");
//        }
//
//    }

   private void LoadPdf(){


       if (isNetworkConnected()){
            refresh.setVisibility(View.GONE);
            er.setVisibility(View.GONE);


           webView.getSettings().setJavaScriptEnabled(true);
           webView.loadUrl(url);

           ConstraintLayout.LayoutParams params= (ConstraintLayout.LayoutParams) webView.getLayoutParams();
           params.width = params.MATCH_PARENT;
           params.height = params.MATCH_PARENT;
           params.setMargins(0, -164, 0, 0);
           webView.setLayoutParams(params);
           webView.setWebViewClient(new WebViewClient(){
               @Override
               public void onPageStarted(WebView view, String url, Bitmap favicon) {
                   super.onPageStarted(view, url, favicon);
                   progressDialog.show();

               }

               @Override
               public void onPageFinished(WebView view, String url) {
                   super.onPageFinished(view, url);
                   progressDialog.dismiss();
               }

               @Override
               public void onReceivedError(WebView view, WebResourceRequest request, WebResourceError error) {
                   super.onReceivedError(view, request, error);
                   progressDialog.dismiss();
                   webView.setVisibility(View.GONE);
                   er.setVisibility(View.VISIBLE);
               }
           });

        }else{
            refresh.setVisibility(View.VISIBLE);
            er.setVisibility(View.VISIBLE);
            er.setText("Your Not Connected To Internet!");
        }
    }

    private boolean isNetworkConnected() {
        ConnectivityManager connec = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        if ( connec.getNetworkInfo(0).getState() == android.net.NetworkInfo.State.CONNECTED ||
                connec.getNetworkInfo(0).getState() == android.net.NetworkInfo.State.CONNECTING ||
                connec.getNetworkInfo(1).getState() == android.net.NetworkInfo.State.CONNECTING ||
                connec.getNetworkInfo(1).getState() == android.net.NetworkInfo.State.CONNECTED ) {

            // if connected with internet

            Toast.makeText(this, " Connected ", Toast.LENGTH_LONG).show();
            return true;

        } else if (
                connec.getNetworkInfo(0).getState() == android.net.NetworkInfo.State.DISCONNECTED ||
                        connec.getNetworkInfo(1).getState() == android.net.NetworkInfo.State.DISCONNECTED  ) {

            Toast.makeText(this, " Not Connected ", Toast.LENGTH_LONG).show();
            return false;
        }


        return false;
//        return connec.getActiveNetworkInfo() != null;
    }
}
