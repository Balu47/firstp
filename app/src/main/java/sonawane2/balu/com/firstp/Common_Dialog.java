package sonawane2.balu.com.firstp;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import sonawane2.balu.com.firstp.adapter.Comon_Adapter;

/**
 * Created by shine on 27-Feb-18.
 */

public class Common_Dialog {

    private CBPOJO cbpojo;
    private ArrayList<CBPOJO> cbpojos = new ArrayList<>();
    private BaseAdapter adapter;
    public String UNIQUEC = "";
    public String UNIQUEB = "";
    public String UNIQUEY  = "";
    public String UNIQUES = "";
//    public String UNIQUESEM = "";
    public String UNIQUECHA = "";
    public String UNIQUEID = "";
    private ListView listView;

    public void ClearThings(){
        UNIQUEC = "";
        UNIQUEB = "";
        UNIQUEY = "";
        UNIQUES = "";
//        UNIQUESEM = "";
        UNIQUECHA = "";
        UNIQUEID = "";
    }

    public void ChooseCourse(final Dialog dialog, final Context context, final RequestQueue requestQueue, final Button b) {



         dialog.setContentView(R.layout.courses);

        listView = (ListView) dialog.findViewById(R.id.clist);
        final ProgressBar Bar = dialog.findViewById(R.id.progressBar10);

        dialog.show();

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, URLS.Course, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {

                Bar.setVisibility(View.GONE);
                try {
                    boolean success = response.getBoolean("success");

                    if (success){

                        JSONArray array = response.getJSONArray("Name");
                        for (int i = 0; i < array.length(); i++){
                            JSONObject object = array.getJSONObject(i);

                            String courses = object.getString("name");

                            cbpojo = new CBPOJO(courses);
                            cbpojos.add(cbpojo);

                            adapter = new CorseAdapter(cbpojos, context);


                            listView.setAdapter(adapter);



                        }
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                Bar.setVisibility(View.GONE);
                Toast.makeText(context,"Volley "+error,Toast.LENGTH_LONG).show();

            }
        });

        requestQueue.add(request);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {


                cbpojo = (CBPOJO) listView.getItemAtPosition(i);
                String course = cbpojo.getCbpojo();
                Toast.makeText(context,course,Toast.LENGTH_LONG).show();
                b.setText(course);
                UNIQUEC = course;
                cbpojos.clear();
                dialog.dismiss();

            }
        });

        dialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialogInterface) {

                cbpojos.clear();
            }
        });


    }

    public void ChooseBranch(final Dialog dialog, final Context context, final RequestQueue requestQueue,  final Button b) {




        dialog.setContentView(R.layout.courses);

        listView = (ListView) dialog.findViewById(R.id.clist);
        final   ProgressBar Bar = dialog.findViewById(R.id.progressBar10);

        dialog.show();

        HashMap<String, String> para = new HashMap<>();
        para.put("course",UNIQUEC);


        final JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, URLS.Branch, new JSONObject(para), new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {

                Bar.setVisibility(View.GONE);
                try {
                    boolean success = response.getBoolean("success");
                    JSONArray object = response.getJSONArray("cbranch");

                    if (success){

                        for (int i = 0; i<object.length(); i++){
                            JSONObject branches = object.getJSONObject(i);
                            JSONArray array = branches.getJSONArray("branches");

                            if (array.length() <= 0){

                                Toast.makeText(context,"No branch added",Toast.LENGTH_LONG).show();

                            }else {
                                for (int b=0; b<array.length(); b++){
                                    JSONObject object1 = array.getJSONObject(b);

                                    String branch = object1.getString("name");

                                    cbpojo = new CBPOJO(branch);
                                    cbpojos.add(cbpojo);

                                    adapter = new CorseAdapter(cbpojos, context);



                                    listView.setAdapter(adapter);

                                }
                            }
                        }

                    }else{
                        String error = response.getString("Error");
                        Toast.makeText(context,error,Toast.LENGTH_LONG).show();
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(context,"Exeption "+e,Toast.LENGTH_LONG).show();

                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                Bar.setVisibility(View.GONE);
                Toast.makeText(context,"Volley "+error,Toast.LENGTH_LONG).show();

            }
        });

        requestQueue.add(request);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

                cbpojo = (CBPOJO) listView.getItemAtPosition(i);
                String course = cbpojo.getCbpojo();
                Toast.makeText(context,course,Toast.LENGTH_LONG).show();
                b.setText(course);
                UNIQUEB = course;
                cbpojos.clear();
                dialog.dismiss();
            }
        });

        dialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialogInterface) {
                cbpojos.clear();
            }
        });

    }

    public void LoadYear(final Dialog dialog, final Context context, final RequestQueue requestQueue,  final Button b){



        HashMap<String, String> para = new HashMap<>();
        para.put("course", UNIQUEC);


        dialog.setContentView(R.layout.courses);


        final   ListView  dlistView = (ListView) dialog.findViewById(R.id.clist);
        final   ProgressBar Bar = dialog.findViewById(R.id.progressBar10);

        dialog.show();


        final JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, URLS.Branch, new JSONObject(para), new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {

                Bar.setVisibility(View.GONE);
                try {

                    JSONArray array = response.getJSONArray("cbranch");
                    for (int i = 0; i<array.length(); i++){
                        JSONObject object = array.getJSONObject(i);

                        String year = object.getString("toyear");
                        int yr = Integer.parseInt(year);

                        String[] toyear =new String[]{
                                "First Year",
                                "Second Year",
                                "Third Year",
                                "Fourth Year",
                                "Fifth Year",
                                "Sixth Year",
                                "Seventh Year"
                        };

                        ArrayList<String> years = new ArrayList<>();

                        for(int c = 0; c < yr; c++){
                            years.add(toyear[c]);
                        }
                        ArrayAdapter<String> Aadapter = new ArrayAdapter<String>(context, R.layout.simple_list3, R.id.textsl3,years);
                        dlistView.setAdapter(Aadapter);


                    }





                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(context,"Exeption "+e,Toast.LENGTH_LONG).show();

                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                Bar.setVisibility(View.GONE);
                Toast.makeText(context,"Volley "+error,Toast.LENGTH_LONG).show();

            }
        });

        requestQueue.add(request);



        dialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialogInterface) {

            }
        });


        dlistView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

                String yea = (String) dlistView.getItemAtPosition(i);
                UNIQUEY = yea;
                b.setText(yea);
                Toast.makeText(context,yea,Toast.LENGTH_LONG).show();

                dialog.dismiss();
            }
        });

        dialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialogInterface) {
                adapter.notifyDataSetChanged();

            }
        });


    }

    public void ChooseSubject(final Dialog dialog, final Context context, final RequestQueue requestQueue,  final Button b) {




        dialog.setContentView(R.layout.courses);

        listView = (ListView) dialog.findViewById(R.id.clist);
        final   ProgressBar Bar = dialog.findViewById(R.id.progressBar10);

        dialog.show();

        HashMap<String, String> para = new HashMap<>();
        para.put("course",UNIQUEC);
        para.put("branch",UNIQUEB);
        para.put("year",UNIQUEY);



        final JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, URLS.GetSub, new JSONObject(para), new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {

                Bar.setVisibility(View.GONE);
                try {
                    boolean success = response.getBoolean("success");
                    JSONArray object = response.getJSONArray("subjects");

                    if (success){

                        if (object.length() <= 0){

                                Toast.makeText(context,"No Subject added",Toast.LENGTH_LONG).show();

                            }else {
                                for (int b=0; b<object.length(); b++){
                                    JSONObject object1 = object.getJSONObject(b);

                                    String subject = object1.getString("name");
                                    String branch = object1.getString("branch");
                                    String id = object1.getString("id");


                                    cbpojo = new CBPOJO(subject, branch, id);
                                    cbpojos.add(cbpojo);

                                    adapter = new Comon_Adapter(cbpojos, context);

                                    listView.setAdapter(adapter);

                                }
                            }


                    }else{
                        String error = response.getString("Error");
                        Toast.makeText(context,error,Toast.LENGTH_LONG).show();
                    }
//                    Toast.makeText(context,"Reaponse "+response,Toast.LENGTH_LONG).show();


                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(context,"Exeption "+e,Toast.LENGTH_LONG).show();

                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                Bar.setVisibility(View.GONE);
                Toast.makeText(context,"Volley "+error,Toast.LENGTH_LONG).show();

            }
        });

        requestQueue.add(request);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

                cbpojo = (CBPOJO) listView.getItemAtPosition(i);
                String subjecct = cbpojo.getCbpojo();
                String id = cbpojo.getId();
                Toast.makeText(context,subjecct,Toast.LENGTH_LONG).show();
                b.setText(subjecct);
                UNIQUES = subjecct;
                UNIQUEID = id;
                cbpojos.clear();
                dialog.dismiss();
            }
        });

        dialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialogInterface) {
                cbpojos.clear();
            }
        });

    }

    public void ChooseChapter(final Dialog dialog, final Context context, final RequestQueue requestQueue,  final Button b) {




        dialog.setContentView(R.layout.courses);

        listView = (ListView) dialog.findViewById(R.id.clist);
        final   ProgressBar Bar = dialog.findViewById(R.id.progressBar10);

        dialog.show();

        HashMap<String, String> para = new HashMap<>();
        para.put("sid",UNIQUEID);




        final JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, URLS.GetChap, new JSONObject(para), new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {

                Bar.setVisibility(View.GONE);
                try {
                    boolean success = response.getBoolean("success");
                    JSONArray object = response.getJSONArray("chapters");

                    if (success){

                        if (object.length() <= 0){

                                Toast.makeText(context,"No Chapter added",Toast.LENGTH_LONG).show();

                            }else {
                                for (int b=0; b<object.length(); b++){
                                    JSONObject object1 = object.getJSONObject(b);

                                    String branch = object1.getString("name");

                                    cbpojo = new CBPOJO(branch);
                                    cbpojos.add(cbpojo);

                                    adapter = new CorseAdapter(cbpojos, context);



                                    listView.setAdapter(adapter);

                                }
                            }


                    }else{
                        String error = response.getString("Error");
                        Toast.makeText(context,error,Toast.LENGTH_LONG).show();
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(context,"Exeption "+e,Toast.LENGTH_LONG).show();

                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                Bar.setVisibility(View.GONE);
//                Toast.makeText(context,"Volley "+error,Toast.LENGTH_LONG).show();

            }
        });

        requestQueue.add(request);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

                cbpojo = (CBPOJO) listView.getItemAtPosition(i);
                String course = cbpojo.getCbpojo();
//                Toast.makeText(context,course,Toast.LENGTH_LONG).show();
                b.setText(course);
                UNIQUECHA = course;
                cbpojos.clear();
                dialog.dismiss();
            }
        });

        dialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialogInterface) {
                cbpojos.clear();
            }
        });

    }

}
