package sonawane2.balu.com.firstp;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by shine on 12-Jan-18.
 */

public class mAdapter extends RecyclerView.Adapter<mAdapter.ViewHolder> {
  Context context;
   ArrayList<pixabay> pixabays = new ArrayList<>();
   private onclick oncli;

   public mAdapter(Context context, ArrayList<pixabay> pixabays, onclick oncli){
       this.context = context;
       this.pixabays = pixabays;
       this.oncli = oncli;
   }

    @Override
    public mAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
       View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_view,parent,false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
       final pixabay pos = pixabays.get(position);
       final String imuri = pos.getImagev();
       final String tg = pos.getTags();

       holder.tag.setText(pos.getTags());
       holder.like.setText("Likes: " + pos.getLikes());
       holder.views.setText("Views: " + pos.getView());

        Picasso.with(context).load(imuri).placeholder(R.drawable.default_artwork).fit().into(holder.imageView);

        holder.tag.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (oncli != null){
                    oncli.myonclick(tg,imuri);
                }
            }
        });

//        holder.imageView.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Intent intent = new Intent(context,VidPlayer.class);
//                intent.putExtra("uri",imuri);
//                context.startActivity(intent);
//            }
//        });

    }


    public static class ViewHolder extends RecyclerView.ViewHolder {
       TextView views, like, tag;
       ImageView imageView;

       public ViewHolder(View v){
           super(v);
          views =v.findViewById(R.id.view);
          like = v.findViewById(R.id.like);
          tag = v.findViewById(R.id.tag);
          imageView = v.findViewById(R.id.imageView);
       }
    }

    @Override
    public int getItemCount() {
        return pixabays.isEmpty() ? 0 : pixabays.size();
    }

    public interface onclick {
        public void myonclick(String tag, String action);
    }

    }
