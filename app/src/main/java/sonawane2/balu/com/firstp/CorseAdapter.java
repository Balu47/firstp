package sonawane2.balu.com.firstp;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by shine on 23-Feb-18.
 */

public class CorseAdapter extends BaseAdapter {

   Context context;
    ArrayList<CBPOJO>  list;

    public CorseAdapter(ArrayList<CBPOJO> list, Context context) {
        this.list = list;
        this.context = context;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int i) {
        return list.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }



    @Override
    public View getView(int position, View view, ViewGroup parent) {

            LayoutInflater inflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(R.layout.simple_list3, parent, false);

            TextView name = view.findViewById(R.id.textsl3);
            name.setText(list.get(position).getCbpojo());

            return view;

    }
}
