package sonawane2.balu.com.firstp;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import java.util.HashMap;
import java.util.Map;
import java.util.Random;

/**
 * Created by shine on 15-Mar-18.
 */

public class Send_again extends AppCompatActivity {

    private RequestQueue requestQueue;
    private ProgressDialog dialog;
    private int i = 0;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.varagain);

        Button button = findViewById(R.id.button33);
        final EditText email = findViewById(R.id.editText12);
        TextView codes = findViewById(R.id.textView27);
        requestQueue = Volley.newRequestQueue(this);
        dialog = new ProgressDialog(this);
        dialog.setMessage("Varifying...");

        codes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(Send_again.this, Varify_email.class);
                startActivity(intent);
                finish();
            }
        });

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String  androidid = Settings.Secure.getString(getApplicationContext().getContentResolver(), Settings.Secure.ANDROID_ID);

                String co = email.getText().toString();

                if (co.isEmpty() || co.equals("") || androidid.isEmpty()){

                    Toast.makeText(getApplicationContext(),"Please Enter Email",Toast.LENGTH_LONG).show();


                }else if (i == 0){

                    Dialogtog();
                    VerifyCode(co, androidid);
                    i = i + 1 ;

                }else{

                    Toast.makeText(getApplicationContext(),"Please try again",Toast.LENGTH_LONG).show();

                }
            }
        });

    }

    private void VerifyCode(final String co, final String uid) {
        Random random = new Random();
        final int code = random.nextInt((10000 - 1000) + 1) + 1000;

        StringRequest request = new StringRequest(Request.Method.POST, URLS.VCodeagain, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                Dialogtog();
                if (response.equals("success")){

                    Intent intent = new Intent(Send_again.this, Varify_email.class);
                    startActivity(intent);
                    finish();
                    Toast.makeText(getApplicationContext(),"Please check your email...",Toast.LENGTH_LONG).show();

                }else{

                    i = 0;
                    Toast.makeText(getApplicationContext(),response,Toast.LENGTH_LONG).show();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                i = 0;
                Dialogtog();
                Toast.makeText(getApplicationContext(),""+error,Toast.LENGTH_LONG).show();

            }
        }){

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> param = new HashMap<>();
                param.put("email",co);
                param.put("aid", uid);
                param.put("otpcode", String.valueOf(code));

                return param;
            }
        };

        requestQueue.add(request);
    }


    private void Dialogtog(){
        if (dialog.isShowing()){
            dialog.dismiss();
        }else{
            dialog.show();
        }
    }
}
