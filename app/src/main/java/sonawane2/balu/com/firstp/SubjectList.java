package sonawane2.balu.com.firstp;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import sonawane2.balu.com.firstp.adapter.Comon_Adapter;

/**
 * Created by shine on 20-Mar-18.
 */

public class SubjectList extends AppCompatActivity {

    private ListView listView;
    private BaseAdapter adapter, adapterss;
    private CBPOJO cbpojo;
    private ArrayList<CBPOJO> cbpojos;
    private RequestQueue requestQueue;
    private ProgressBar progressBar;
    private Bundle bundle;
    private Dialog dialog , dialog2;
    private ProgressDialog progressDialoge;
    private SharedPreferences preferences;
    private String token;

    private Common_Dialog common_dialog;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.subject_list);

        listView = findViewById(R.id.subjectlist);
        cbpojos = new ArrayList<>();
        dialog = new Dialog(this,R.style.Theme_AppCompat_DayNight_Dialog_Alert);
        dialog2 = new Dialog(this,R.style.Theme_AppCompat_DayNight_Dialog_Alert);
        progressDialoge = new ProgressDialog(this);
        progressDialoge.setMessage("Please Wait..");
        progressDialoge.setCancelable(false);
        requestQueue = Volley.newRequestQueue(this);
        bundle = getIntent().getExtras();
        if (bundle != null){


            LOadDatas();

        }else {
            Toast.makeText(getApplicationContext(), " Some Error Occured!", Toast.LENGTH_LONG).show();
        }
    }

    private void ProgresDToggle(){
        if (progressDialoge.isShowing()){

            progressDialoge.dismiss();
        }else{

            progressDialoge.show();
        }
    }
    private void LOadDatas(){
        ProgresDToggle();

        String url = "";
        HashMap<String, String> para = new HashMap<>();
        if (bundle.getBoolean("admin")){

            para.put("course", bundle.getString("course"));
            para.put("branch", bundle.getString("branch"));
            para.put("year", bundle.getString("year"));

            url = URLS.GetSub;

        }else {

            para.put("course", bundle.getString("course"));
            para.put("branch", bundle.getString("branch"));
            para.put("sem", bundle.getString("sem"));

            url = URLS.GetSSub;
        }

        final JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, url, new JSONObject(para), new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {

                ProgresDToggle();

                try {
                    boolean success = response.getBoolean("success");

                    if (success){
                        JSONArray array = response.getJSONArray("subjects");
                        for (int i = 0; i<array.length(); i++){
                            JSONObject object = array.getJSONObject(i);
                            String subject = object.getString("name");
                            String branch = object.getString("branch");
                            String id = object.getString("id");


                            cbpojo = new CBPOJO(subject, branch, id);
                            cbpojos.add(cbpojo);

                            adapter = new Comon_Adapter(cbpojos, SubjectList.this);

                        }
                        listView.setAdapter(adapter);

                    }else{
                        String error = response.getString("Error");
                        Toast.makeText(getApplicationContext(),error,Toast.LENGTH_LONG).show();
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(getApplicationContext(),"Exeption "+e,Toast.LENGTH_LONG).show();

                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                ProgresDToggle();
                Toast.makeText(getApplicationContext(),"Volley "+error,Toast.LENGTH_LONG).show();

            }
        });
        requestQueue.add(request);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

                cbpojo = (CBPOJO) listView.getItemAtPosition(i);
                String subject = cbpojo.getCbpojo();
                String branch = cbpojo.getYear();
                String id = cbpojo.getId();

             if (bundle.getBoolean("admin")){

                 if (bundle.getString("where").equals("subject")){

                     Deletecha(id);
                     Toast.makeText(getApplicationContext(),subject,Toast.LENGTH_LONG).show();
                 }else{

                   Intent intent = new Intent(SubjectList.this, LoadChapter.class);
                     intent.putExtra("year",bundle.getString("year"));
                     intent.putExtra("course", bundle.getString("course"));
                     intent.putExtra("branch", bundle.getString("branch"));
                     intent.putExtra("sid", id);
                     intent.putExtra("isAdmin", true);
                     startActivity(intent);
                 }

             }else{

                 Intent intent = new Intent(SubjectList.this, LoadChapter.class);
                 intent.putExtra("sem",bundle.getString("sem"));
                 intent.putExtra("branch", bundle.getString("branch"));
                 intent.putExtra("sid", id);
                 intent.putExtra("isAdmin", false);
                 startActivity(intent);             }

            }
        });
    }

//    private void ShowChapters(final boolean isAdmin, final String id){
//
//
//        dialog.setContentView(R.layout.courses);
//
//      final ListView  nlistView = (ListView) dialog.findViewById(R.id.clist);
//        final   ProgressBar Bar = dialog.findViewById(R.id.progressBar10);
//
//        dialog.show();
//
//        HashMap<String, String> para = new HashMap<>();
//        para.put("sid",id);
//
//
//
//
//        final JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, URLS.GetChap, new JSONObject(para), new Response.Listener<JSONObject>() {
//            @Override
//            public void onResponse(JSONObject response) {
//
//                Bar.setVisibility(View.GONE);
//                try {
//                    boolean success = response.getBoolean("success");
//                    JSONArray object = response.getJSONArray("chapters");
//
//                    if (success){
//
//                        if (object.length() <= 0){
//
//                            Toast.makeText(getApplicationContext(),"No Chapter added",Toast.LENGTH_LONG).show();
//
//                        }else {
//                            for (int b=0; b<object.length(); b++){
//                                JSONObject chapters = object.getJSONObject(b);
//
//                                String branch = chapters.getString("cname");
//
//                              CBPOJO  cbpoj = new CBPOJO(branch);
//                            ArrayList<CBPOJO> cbpojoss = new ArrayList<>();
//                                cbpojoss.add(cbpoj);
//
//                                adapterss = new CorseAdapter(cbpojos, getApplicationContext());
//
//
//
//                                nlistView.setAdapter(adapterss);
//
//                            }
//                        }
//                        Toast.makeText(getApplicationContext(),"Res "+response,Toast.LENGTH_LONG).show();
//
//
//                    }else{
//                        String error = response.getString("Error");
//                        Toast.makeText(getApplicationContext(),error,Toast.LENGTH_LONG).show();
//                    }
//
//                } catch (JSONException e) {
//                    e.printStackTrace();
//                    Toast.makeText(getApplicationContext(),"Exeption "+e,Toast.LENGTH_LONG).show();
//
//                }
//
//
//            }
//        }, new Response.ErrorListener() {
//            @Override
//            public void onErrorResponse(VolleyError error) {
//
//                Bar.setVisibility(View.GONE);
//                Toast.makeText(getApplicationContext(),"Volley "+error,Toast.LENGTH_LONG).show();
//
//            }
//        });
//
//        requestQueue.add(request);
//
//        nlistView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
//            @Override
//            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
//
//                cbpojo = (CBPOJO) nlistView.getItemAtPosition(i);
//                String chapter = cbpojo.getCbpojo();
//                Toast.makeText(getApplicationContext(),chapter,Toast.LENGTH_LONG).show();
//                if (isAdmin){
//
//                    Deletecha(id,chapter );
//                    dialog.dismiss();
//                }else {
//                    Intent intent = new Intent(SubjectList.this, PdfList.class);
//                    intent.putExtra("sem",bundle.getString("sem"));
//                    intent.putExtra("course", bundle.getString("course"));
//                    intent.putExtra("branch", bundle.getString("branch"));
//                    intent.putExtra("admin", false);
//                    startActivity(intent);
//                }
//
//            }
//        });
//
//        dialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
//            @Override
//            public void onCancel(DialogInterface dialogInterface) {
//                LOadDatas();
//            }
//        });
//    }

    private void Deletecha(final String id) {

        Button delete, cancel;

        dialog2.setContentView(R.layout.common_dialog);

        delete = dialog2.findViewById(R.id.delete);
        cancel = dialog2.findViewById(R.id.cancel);

        dialog2.show();

        delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Deletechapter(id);
                dialog2.dismiss();
            }
        });

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                dialog2.dismiss();
            }
        });
    }

    private void Deletechapter(final String id) {

        ProgresDToggle();
//        Toast.makeText(getApplicationContext(),id,Toast.LENGTH_LONG).show();
//        String url = "http://024ac872.ngrok.io/del/course";

        StringRequest request = new StringRequest(Request.Method.POST, URLS.DelSub, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                adapter.notifyDataSetChanged();
                cbpojos.clear();
                LOadDatas();
                ProgresDToggle();
//                Toast.makeText( getApplicationContext(),response,Toast.LENGTH_LONG).show();

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                adapter.notifyDataSetChanged();
                cbpojos.clear();
                LOadDatas();
                ProgresDToggle();
//                Toast.makeText(getApplicationContext(),"Error "+error,Toast.LENGTH_LONG).show();

            }
        }){
            @Override
            public String getBodyContentType() {
                return "application/x-www-form-urlencoded; charset=UTF-8";
            }

//            @Override
//            public Map<String, String> getHeaders() throws AuthFailureError {
//                Map<String, String> header = new HashMap<>();
//                header.put("token", token);
//                return header;
//            }

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> param = new HashMap<>();
                param.put("sid",id);

                return param;
            }
        };
        requestQueue.add(request);
    }



//    private void showpopun(final String sub, final String bra, final String id ) {
//
//
//        dialog.setContentView(R.layout.student_dialog);
//
//        final TextView na, no, co, br, yr;
//        Button cancel, block;
//
//        na = dialog.findViewById(R.id.textView15);
//        no = dialog.findViewById(R.id.textView17);
//        co = dialog.findViewById(R.id.textView18);
//        br = dialog.findViewById(R.id.textView19);
//        yr = dialog.findViewById(R.id.textView20);
//        block = dialog.findViewById(R.id.button18);
//        cancel = dialog.findViewById(R.id.button19);
//
//        na.setText("Course Name : "+sub);
//        no.setVisibility(View.GONE);
//        co.setVisibility(View.GONE);
//        br.setVisibility(View.GONE);
//        yr.setVisibility(View.GONE);
//
//        block.setText("DELETE");
//
//        dialog.show();
//
//        block.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//
//                DeleteCourse(sub, bra,id);
//
//            }
//        });
//
//        cancel.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//
//                dialog.dismiss();
//            }
//        });
//    }

    private void DeleteCourse(final String sub, final String br, final String id) {

        Button delete, cancel;

        dialog.setContentView(R.layout.common_dialog);

        delete = dialog.findViewById(R.id.delete);
        cancel = dialog.findViewById(R.id.cancel);

        dialog.show();

        delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Delete(sub, br, id);
                dialog.dismiss();
            }
        });

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                dialog.dismiss();
            }
        });
    }

    private void Delete(final String sub, final String br,final String id)






















    {

        ProgresDToggle();
        Toast.makeText(getApplicationContext(),id,Toast.LENGTH_LONG).show();
//        String url = "http://024ac872.ngrok.io/del/course";

        StringRequest request = new StringRequest(Request.Method.POST, URLS.DelSub, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                ProgresDToggle();
                adapter.notifyDataSetChanged();
                cbpojos.clear();
                LOadDatas();
                Toast.makeText(getApplicationContext(),response,Toast.LENGTH_LONG).show();

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                ProgresDToggle();
//                Toast.makeText(getApplicationContext(),"Error "+error,Toast.LENGTH_LONG).show();

            }
        }){
            @Override
            public String getBodyContentType() {
                return "application/x-www-form-urlencoded; charset=UTF-8";
            }

//            @Override
//            public Map<String, String> getHeaders() throws AuthFailureError {
//                Map<String, String> header = new HashMap<>();
//                header.put("token", token);
//                return header;
//            }

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> param = new HashMap<>();
                param.put("sid",id);
                param.put("sname",sub);
                param.put("branch",br);


                return param;
            }
        };
        requestQueue.add(request);
    }

}
