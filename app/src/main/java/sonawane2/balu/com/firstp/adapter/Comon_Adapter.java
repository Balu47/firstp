package sonawane2.balu.com.firstp.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

import sonawane2.balu.com.firstp.CBPOJO;
import sonawane2.balu.com.firstp.R;

/**
 * Created by shine on 12-Mar-18.
 */

public class Comon_Adapter extends BaseAdapter{

    Context context;
    ArrayList<CBPOJO> list;

    public Comon_Adapter(ArrayList<CBPOJO> list, Context context) {
        this.list = list;
        this.context = context;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int i) {
        return list.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }



    @Override
    public View getView(int position, View view, ViewGroup parent) {

        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        view = inflater.inflate(R.layout.simple_list, parent, false);

        TextView name = view.findViewById(R.id.textView14);
        name.setText(list.get(position).getCbpojo());

        return view;

    }
}
