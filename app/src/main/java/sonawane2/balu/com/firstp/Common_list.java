package sonawane2.balu.com.firstp;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import sonawane2.balu.com.firstp.adapter.Common_Adapter;
import sonawane2.balu.com.firstp.adapter.Comon_Adapter;
import sonawane2.balu.com.firstp.adapter.Student_Adapter;

/**
 * Created by shine on 24-Feb-18.
 */

public class Common_list extends AppCompatActivity implements Common_Adapter.onclick, Student_Adapter.Student_onclick{
    private RecyclerView recyclerView;
    private RecyclerView.LayoutManager manager;
    private ArrayList<CBPOJO> cbpojos;
    private ArrayList<StudentPOJO> studentPOJOS;
    private Common_Adapter common_adapter;
    private RequestQueue requestQueue;
    private CBPOJO cbpojo;
    private StudentPOJO studentPOJO;
    private String where;
    private ListView listView;
    private BaseAdapter adapter;
    private int year;
    private String branchOryear = "";
    private Bundle bundle;
    private String course ="";
    private SharedPreferences preferences, tokenpref;
    private SharedPreferences.Editor editor;
    private ProgressBar progressBar;
    private Dialog dialog ;
    private String token;
    private ProgressDialog progressDialoge;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.common_list);

        recyclerView = findViewById(R.id.commonr);
        manager = new LinearLayoutManager(this);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(manager);
        DividerItemDecoration itemdecor = new DividerItemDecoration(recyclerView.getContext(),DividerItemDecoration.VERTICAL);
        recyclerView.addItemDecoration(itemdecor);
        listView = findViewById(R.id.commol);
        preferences = this.getSharedPreferences("com.example.COURSE", Context.MODE_PRIVATE);
        editor = preferences.edit();
        cbpojos = new ArrayList<>();
        studentPOJOS = new ArrayList<>();
        progressBar = findViewById(R.id.progressBar7);
        requestQueue = Volley.newRequestQueue(this);
        dialog = new Dialog(this,R.style.Theme_AppCompat_DayNight_Dialog_Alert);

        progressDialoge = new ProgressDialog(this);
        progressDialoge.setMessage("Please Wait..");
        progressDialoge.setCancelable(false);

        tokenpref = this.getSharedPreferences("com.example.TOKEN", Context.MODE_PRIVATE);
        token = tokenpref.getString("TOKEN","");

        hide();
        bundle = getIntent().getExtras();
        if (bundle != null){
            branchOryear = bundle.getString("where");
            course = bundle.getString("course");
            loadData();

        }else {
            Toast.makeText(getApplicationContext(), " Some Error Occured!", Toast.LENGTH_LONG).show();
        }

    }



    private void show(){
       progressBar.setVisibility(View.VISIBLE);
  }
    private void hide(){
        progressBar.setVisibility(View.GONE);
    }

    private void loadData() {
         show();
//       String url = "http://024ac872.ngrok.io/branch";

        if (course.isEmpty()){

            Toast.makeText(getApplicationContext(),"Some Error Occured!",Toast.LENGTH_LONG).show();
        }else{

            HashMap<String, String> para = new HashMap<>();
            para.put("course", course);


            final JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, URLS.Branch, new JSONObject(para), new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {

                    hide();
                    try {
                        boolean success = response.getBoolean("success");
                        JSONArray object = response.getJSONArray("cbranch");

                        if (success){

                            for (int i = 0; i<object.length(); i++){
                                JSONObject branches = object.getJSONObject(i);
                                String courses = branches.getString("cousre");
                                String id = branches.getString("_id");

                                JSONArray array = branches.getJSONArray("branches");

                                if (array.length() <= 0){

                                    Toast.makeText(getApplicationContext(),"No branch added",Toast.LENGTH_LONG).show();

                                }else {
                                    for (int b=0; b<array.length(); b++){
                                        JSONObject object1 = array.getJSONObject(b);

                                        String branch = object1.getString("name");

                                        cbpojo = new CBPOJO(branch, courses, id);
                                        cbpojos.add(cbpojo);

                                        adapter = new Comon_Adapter(cbpojos, Common_list.this);
                                    }


                                }


                                listView.setAdapter(adapter);

                            }

                        }else{
                            String error = response.getString("Error");
                            Toast.makeText(getApplicationContext(),error,Toast.LENGTH_LONG).show();
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                        Toast.makeText(getApplicationContext(),"Exeption "+e,Toast.LENGTH_LONG).show();

                    }


                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {

                    hide();
                    Toast.makeText(getApplicationContext(),"Volley "+error,Toast.LENGTH_LONG).show();

                }
            });

            requestQueue.add(request);

            listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                    cbpojo = (CBPOJO) listView.getItemAtPosition(i);
                    String branch = cbpojo.getCbpojo();
                    String course = cbpojo.getCbpojo();
                    String id = cbpojo.getId();
                    Toast.makeText(getApplicationContext(),branch,Toast.LENGTH_LONG).show();


                    if (branchOryear.equals("branch")){

                        showpopup(course, branch, id);
                        Toast.makeText(getApplicationContext(),branch,Toast.LENGTH_LONG).show();
                    }else{
                        LoadYear(branch);
                    }

                }
            });
        }

    }

    private void showpopup(String course, final String branch, final String id ) {


        dialog.setContentView(R.layout.student_dialog);

        TextView na, no, co, br, yr, email;
        Button cancel, block;

        na = dialog.findViewById(R.id.textView15);
        no = dialog.findViewById(R.id.textView17);
        co = dialog.findViewById(R.id.textView18);
        br = dialog.findViewById(R.id.textView19);
        yr = dialog.findViewById(R.id.textView20);
        block = dialog.findViewById(R.id.button18);
        cancel = dialog.findViewById(R.id.button19);
        email = dialog.findViewById(R.id.button18c);

        na.setText("Branch Name : "+branch);
        no.setVisibility(View.GONE);
        co.setVisibility(View.GONE);
        br.setVisibility(View.GONE);
        yr.setVisibility(View.GONE);
        email.setVisibility(View.GONE);

        block.setText("DELETE");

        dialog.show();

        block.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                DeleteBranch(id, branch);


//                dialog.dismiss();
            }
        });

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                dialog.dismiss();
            }
        });
    }

    private void DeleteBranch( final String id, final String branch) {

        Button delete, cancel;

        dialog.setContentView(R.layout.common_dialog);

        delete = dialog.findViewById(R.id.delete);
        cancel = dialog.findViewById(R.id.cancel);

        dialog.show();

        delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Delete(id, branch);

                dialog.dismiss();
            }
        });

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                dialog.dismiss();
            }
        });

        dialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialogInterface) {

            }
        });
    }

    private void Delete(final String id, final String branch) {

        progressDialoge.show();
//        String url = "http://024ac872.ngrok.io/del/branch";

        StringRequest request = new StringRequest(Request.Method.POST, URLS.DelBranch, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                progressDialoge.hide();
                adapter.notifyDataSetChanged();
                cbpojos.clear();
                loadData();

                Toast.makeText(getApplicationContext(),response,Toast.LENGTH_LONG).show();

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                progressDialoge.hide();


//                Toast.makeText(getApplicationContext(),"Error "+error,Toast.LENGTH_LONG).show();

            }
        }){
            @Override
            public String getBodyContentType() {
                return "application/x-www-form-urlencoded; charset=UTF-8";
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> header = new HashMap<>();
                header.put("token",tokenpref.getString("TOKEN",""));
                return header;
            }

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> param = new HashMap<>();
                param.put("bid",id);
                param.put("bname",branch);



                return param;
            }
        };
        requestQueue.add(request);
    }


    private void LoadYear(final String branch){

//        String url = "http://024ac872.ngrok.io/branch";
show();

       final Dialog cdialog = new Dialog(this, R.style.Theme_AppCompat_DayNight_Dialog_Alert);
        HashMap<String, String> para = new HashMap<>();
        para.put("course", course);

        final ListView listView;

        cdialog.setContentView(R.layout.courses);


    final   ListView  dlistView = (ListView) cdialog.findViewById(R.id.clist);

        final   ProgressBar Bar = cdialog.findViewById(R.id.progressBar10);

      cdialog.show();

        final JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, URLS.Branch, new JSONObject(para), new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
hide();
//                    disable = false;
                Bar.setVisibility(View.INVISIBLE);
                try {

                    JSONArray array = response.getJSONArray("cbranch");
                    for (int i = 0; i<array.length(); i++){
                        JSONObject object = array.getJSONObject(i);

                        String year = object.getString("toyear");
                        int yr = Integer.parseInt(year);
                        String se = object.getString("nofsem");
//                        semester = Integer.parseInt(se);

                        String[] toyear =new String[]{
                                "First Year",
                                "Second Year",
                                "Third Year",
                                "Fourth Year",
                                "Fifth Year",
                                "Sixth Year",
                                "Seventh Year"
                        };

                        ArrayList<String> years = new ArrayList<>();

                        for(int c = 0; c < yr; c++){
                            years.add(toyear[c]);
                        }
                        ArrayAdapter<String> Aadapter = new ArrayAdapter<String>(Common_list.this, R.layout.simple_list3, R.id.textsl3,years);
                        dlistView.setAdapter(Aadapter);

//                        progressBar.setVisibility(View.GONE);

                    }





                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(getApplicationContext(),"Exeption "+e,Toast.LENGTH_LONG).show();

                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Bar.setVisibility(View.INVISIBLE);

                hide();
//                    disable = false;
//                progressBar.setVisibility(View.GONE);
                Toast.makeText(getApplicationContext(),"Volley "+error,Toast.LENGTH_LONG).show();

            }
        });

        requestQueue.add(request);



        cdialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialogInterface) {

            }
        });


        dlistView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

                String yea = (String) dlistView.getItemAtPosition(i);
                Intent intent = new Intent(Common_list.this, teacherList.class);
                intent.putExtra("branch", branch);
                intent.putExtra("year", yea);
                intent.putExtra("where",branchOryear);
                startActivity(intent);
                Toast.makeText(getApplicationContext(),yea,Toast.LENGTH_LONG).show();

                cdialog.dismiss();
            }
        });

        cdialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialogInterface) {
                adapter.notifyDataSetChanged();
                loadData();
            }
        });
    }



    @Override
    public void myonclick(int pos, String action) {
        Toast.makeText(getApplicationContext(),"LongClick...",Toast.LENGTH_LONG).show();

    }

    @Override
    public void Smyonclick(int positions, String action) {

    }

    }

