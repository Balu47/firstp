package sonawane2.balu.com.firstp;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by shine on 07-Feb-18.
 */

public class signup extends AppCompatActivity {

    private EditText uname, uno;
    private Button chyear, sub, ccourse, cbranch;
    private Dialog dialog, cdialog;
    private String year = "";
    private String  Acourse ="", Abranch ="", Ayear = "";
    private SharedPreferences preferences, uidpref, Showpref;
    private SharedPreferences.Editor sedior, uideditor;
    private CBPOJO cbpojo;
    private ArrayList<CBPOJO> cbpojos;
    private RequestQueue requestQueue;
    private BaseAdapter adapter;
    private boolean disable = false;
    private ProgressBar progressBar;
    private String semester = "";
    private Common_Dialog common_dialog;
    private Bundle bundle;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.signup);

        chyear = findViewById(R.id.button8);
        sub = findViewById(R.id.button3);
        uname = findViewById(R.id.uname);
        uno = findViewById(R.id.uno);
        ccourse = findViewById(R.id.ccourse);
        cbranch = findViewById(R.id.cbranch);
        progressBar  = findViewById(R.id.progressBar6);
        progressBar.setVisibility(View.GONE);

        requestQueue = Volley.newRequestQueue(this);

        cbpojos = new ArrayList<>();
        common_dialog = new Common_Dialog();
        preferences = this.getSharedPreferences("com.example.USER_INFO", Context.MODE_PRIVATE);
        uidpref = this.getSharedPreferences("com.example.SHOW", Context.MODE_PRIVATE);
        final SharedPreferences.Editor editor = preferences.edit();
        Showpref = this.getSharedPreferences("com.example.SHOW_SCREEN",this.MODE_PRIVATE);
        sedior = Showpref.edit();
        uideditor = uidpref.edit();
       boolean show = preferences.getBoolean("showsign",false);

//       if (show){
//           Intent intent = new Intent(signup.this, MainPage.class);
//           startActivity(intent);
//       }
         bundle = getIntent().getExtras();

        dialog = new Dialog(this, R.style.Theme_AppCompat_DayNight_Dialog_Alert);
        cdialog = new Dialog(this, R.style.Theme_AppCompat_DayNight_Dialog_Alert);

        chyear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showPop(chyear);
//                alertD();

//                if (Acourse.equals("") || Acourse.isEmpty()){
//                    Toast.makeText(getApplicationContext(),"Please Select Course",Toast.LENGTH_LONG).show();
//                }else{
//
//                    Ayear = common_dialog.LoadYear(dialog, signup.this, requestQueue, Acourse);
//
//                }
            }
        });

        ccourse.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (disable){

                }else{


                    disable = true;
                    ChooseCourse(ccourse);
                }

//       Acourse =  common_dialog.ChooseCourse(dialog,signup.this,requestQueue);

            }
        });

        cbranch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (disable){

                }else{

                    if (Acourse.equals("") || Acourse.isEmpty()){
                        Toast.makeText(getApplicationContext(),"Please Select Course",Toast.LENGTH_LONG).show();
                    }else{
                        disable = true;
                        ChooseBranch(cbranch);
                    }


                }

//                if (Acourse.equals("") || Acourse.isEmpty()){
//                    Toast.makeText(getApplicationContext(),"Please Select Course",Toast.LENGTH_LONG).show();
//                }else{
//
//                    Abranch = common_dialog.ChooseBranch(dialog,signup.this,requestQueue,Acourse);
//
//                }
            }
        });

        sub.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String unam = uname.getText().toString().trim();
                String un = uno.getText().toString().trim();
                if ( !unam.isEmpty() && !un.isEmpty()){
                    if (!year.equals("") && !Abranch.equals("") && !Acourse.equals("")) {
                        editor.putBoolean("showsign",false);
                        editor.putString("username", uname.getText().toString());
                        editor.putString("userno", uno.getText().toString());
                        editor.putString("curyear", year);
                        editor.putString("course", Acourse);
                        editor.putString("branch", Abranch);
                        editor.putString("sem", semester);
                        editor.commit();
                       AddStudent();
                    }else{
                        Toast.makeText(getApplicationContext(),"Please Choose All Fields",Toast.LENGTH_LONG).show();
                    }
                }else{
                    Toast.makeText(getApplicationContext(),"Please Fill All The Field",Toast.LENGTH_LONG).show();

                }
            }
        });



    }

    private void ChooseCourse(final  Button b) {
        disable = true;

        final ListView listView;

        cdialog.setContentView(R.layout.courses);

        listView = (ListView) cdialog.findViewById(R.id.clist);
        final   ProgressBar Bar = cdialog.findViewById(R.id.progressBar10);

        cdialog.show();

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, URLS.Course, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {

                Bar.setVisibility(View.INVISIBLE);
                try {
                    boolean success = response.getBoolean("success");

                    if (success){

                        JSONArray array = response.getJSONArray("Name");
                        for (int i = 0; i < array.length(); i++){
                            JSONObject object = array.getJSONObject(i);

                            String courses = object.getString("name");

                            cbpojo = new CBPOJO(courses);
                            cbpojos.add(cbpojo);

                            adapter = new CorseAdapter(cbpojos, signup.this);


                            listView.setAdapter(adapter);



                        }
                    }

                } catch (JSONException e) {
                    Bar.setVisibility(View.INVISIBLE);
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                disable = false;
                Bar.setVisibility(View.INVISIBLE);
//                Toast.makeText(getApplicationContext(),"Volley "+error,Toast.LENGTH_LONG).show();

            }
        });

        requestQueue.add(request);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

                disable = false;
                cbpojo = (CBPOJO) listView.getItemAtPosition(i);
                String course = cbpojo.getCbpojo();
                Toast.makeText(getApplicationContext(),course,Toast.LENGTH_LONG).show();
                b.setText(course);
                Acourse = course;
                cbpojos.clear();
                cdialog.dismiss();
            }
        });

        cdialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialogInterface) {
                disable = false;
                cbpojos.clear();
            }
        });
    }

    private void AddStudent() {
        String url = "";
        final String cid = uidpref.getString("UID","");

        if (bundle.getBoolean("update", false)){

            url = URLS.FirstR+"updatestudents";

        }else{
            url  = URLS.AddStudents;
        }

     final String uid = uidpref.getString("UID","");
// Toast.makeText(getApplicationContext(),uid,Toast.LENGTH_LONG).show();
      if (uid.isEmpty()){

          Toast.makeText(getApplicationContext(), "Error Please Clear App Data.",Toast.LENGTH_LONG).show();
      }else{

          progressBar.setVisibility(View.VISIBLE);

//          String url = "http://024ac872.ngrok.io/students";

          StringRequest request = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
              @Override
              public void onResponse(String response) {
                  progressBar.setVisibility(View.GONE);

                  if (response.equals("success")){
                      sedior.putBoolean("show", false);
                      sedior.putString("goto", "student");
                      sedior.commit();

                      uideditor.putBoolean("login", false);
                      uideditor.commit();

                      Intent intent = new Intent(signup.this, MainPage.class);
                      startActivity(intent);
                      finish();
                  }else{
                      Toast.makeText(getApplicationContext(),"Something Goes Wrong"+response,Toast.LENGTH_LONG).show();
                  }

              }
          }, new Response.ErrorListener() {
              @Override
              public void onErrorResponse(VolleyError error) {
                  progressBar.setVisibility(View.GONE);
//                  Toast.makeText(getApplicationContext(),"VolleyError "+error,Toast.LENGTH_LONG).show();

              }
          }){

              @Override
              public String getBodyContentType() {
                  return "application/x-www-form-urlencoded; charset=UTF-8";
              }

//            @Override
//            public Map<String, String> getHeaders() throws AuthFailureError {
//                Map<String, String> header = new HashMap<>();
//                header.put("token",token);
//                return header;
//            }

              @Override
              protected Map<String, String> getParams() throws AuthFailureError {
                  Map<String, String> param = new HashMap<>();
                  param.put("sname",uname.getText().toString());
                  param.put("no", uno.getText().toString());
                  param.put("course", Acourse);
                  param.put("branch", Abranch);
                  param.put("year", year);
                  param.put("cid", cid);
                  param.put("sem", semester);


                  return param;
              }
          };

          requestQueue.add(request);
      }
    }

    private void ChooseBranch(final Button b) {

        disable = true;

        final ListView listView;

        cdialog.setContentView(R.layout.courses);

        listView = (ListView) cdialog.findViewById(R.id.clist);
      final   ProgressBar Bar = cdialog.findViewById(R.id.progressBar10);

//        String[] courses = new String[]{
//                "Polytechnic",
//                "Engineering",
//                "D-Pharmacy",
//                "B-Pharmacy"
//        };
//        ArrayList<String> list = new ArrayList<>();
//
//        list.add("Polytechnic");
//        list.add("Engineering");
//        list.add("D-Pharmacy");
//        list.add("B-Pharmacy");

        cdialog.show();

        HashMap<String, String> para = new HashMap<>();
        para.put("course",Acourse);


        final JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, URLS.Branch, new JSONObject(para), new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Bar.setVisibility(View.INVISIBLE);

                disable = false;
                try {
                    boolean success = response.getBoolean("success");
                    JSONArray object = response.getJSONArray("cbranch");

                    if (success){

                        for (int i = 0; i<object.length(); i++){
                            JSONObject branches = object.getJSONObject(i);
                            JSONArray array = branches.getJSONArray("branches");

                            if (array.length() <= 0){

                                Toast.makeText(getApplicationContext(),"No branch added",Toast.LENGTH_LONG).show();

                            }else {
                                for (int b=0; b<array.length(); b++){
                                    JSONObject object1 = array.getJSONObject(b);

                                    String branch = object1.getString("name");

                                    cbpojo = new CBPOJO(branch);
                                    cbpojos.add(cbpojo);

                                    adapter = new CorseAdapter(cbpojos, signup.this);

//                            ArrayAdapter<String> adapter = new ArrayAdapter<String>(signup.this,android.R.layout.simple_list_item_1,cbpojos);


                                    listView.setAdapter(adapter);


                                }
                            }
                        }

                    }else{
                        String error = response.getString("Error");
                        Toast.makeText(getApplicationContext(),error,Toast.LENGTH_LONG).show();
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(getApplicationContext(),"Exeption "+e,Toast.LENGTH_LONG).show();

                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                disable = false;
                Bar.setVisibility(View.INVISIBLE);
//                Toast.makeText(getApplicationContext(),"Volley "+error,Toast.LENGTH_LONG).show();

            }
        });

        requestQueue.add(request);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

                disable = false;
                cbpojo = (CBPOJO) listView.getItemAtPosition(i);
                String course = cbpojo.getCbpojo();
                Toast.makeText(getApplicationContext(),course,Toast.LENGTH_LONG).show();
                b.setText(course);
                Abranch = course;
                cbpojos.clear();
                cdialog.dismiss();
            }
        });

        cdialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialogInterface) {
                disable = false;
                cbpojos.clear();
            }
        });
    }

//    private void chcourse() {
//
//        disable = true;
//
//        final ListView listView;
//
//        cdialog.setContentView(R.layout.courses);
//
//        listView = (ListView) cdialog.findViewById(R.id.clist);
//
////        String[] courses = new String[]{
////                "Polytechnic",
////                "Engineering",
////                "D-Pharmacy",
////                "B-Pharmacy"
////        };
////        ArrayList<String> list = new ArrayList<>();
////
////        list.add("Polytechnic");
////        list.add("Engineering");
////        list.add("D-Pharmacy");
////        list.add("B-Pharmacy");
//
//        String url = "http://139.59.26.186/course";
//
//        final JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
//            @Override
//            public void onResponse(JSONObject response) {
//
//                try {
//                    boolean success = response.getBoolean("success");
//
//                    if (success){
//                        JSONArray array = response.getJSONArray("Courses");
//                        for (int i = 0; i<array.length(); i++){
//                            JSONObject object = array.getJSONObject(i);
//                            String courses = object.getString("course");
//
//                            cbpojo = new CBPOJO(courses);
//                            cbpojos.add(cbpojo);
//
//                            adapter = new CorseAdapter(cbpojos, signup.this);
//
////                            ArrayAdapter<String> adapter = new ArrayAdapter<String>(signup.this,android.R.layout.simple_list_item_1,cbpojos);
//
//
//                            listView.setAdapter(adapter);
//
//                            progressBar.setVisibility(View.GONE);
//                            cdialog.show();
//                        }
//
//                    }else{
//                        String error = response.getString("Error");
//                        Toast.makeText(getApplicationContext(),error,Toast.LENGTH_LONG).show();
//                    }
//
//                } catch (JSONException e) {
//                    e.printStackTrace();
//                    Toast.makeText(getApplicationContext(),"Exeption "+e,Toast.LENGTH_LONG).show();
//
//                }
//
//
//            }
//        }, new Response.ErrorListener() {
//            @Override
//            public void onErrorResponse(VolleyError error) {
//
//                progressBar.setVisibility(View.GONE);
//                Toast.makeText(getApplicationContext(),"Volley "+error,Toast.LENGTH_LONG).show();
//
//            }
//        });
//
//     requestQueue.add(request);
//
//        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
//            @Override
//            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
//
//                cbpojo = (CBPOJO) listView.getItemAtPosition(i);
//                String course = cbpojo.getCbpojo();
//                Toast.makeText(getApplicationContext(),course,Toast.LENGTH_LONG).show();
//                Acourse = course;
//                disable = false;
//                cbpojos.clear();
//                cdialog.dismiss();
//            }
//        });
//
//        cdialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
//            @Override
//            public void onCancel(DialogInterface dialogInterface) {
//                disable = false;
//                cbpojos.clear();
//            }
//        });
//    }


//    private void alertD(){
//        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
//        builder.setItems(R.array.select_year, new DialogInterface.OnClickListener() {
//            @Override
//            public void onClick(DialogInterface dialogInterface, int i) {
//                Toast.makeText(getApplicationContext(),i,Toast.LENGTH_LONG).show();
//            }
//        });
//        builder.create();
//        builder.show();
//    }

    private void showPop(final Button b) {
//        final RadioGroup group;
//        final RadioButton selectedy;
//        Button submit;
//
//        dialog.setContentView(R.layout.yearpopup);
//        group = dialog.findViewById(R.id.yeargroup);
//        submit = dialog.findViewById(R.id.button9);
//
//        year = "Select Year";
//        group.clearCheck();
//        dialog.show();
//
//        group.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
//            @Override
//            public void onCheckedChanged(RadioGroup radioGroup, int i) {
//                switch (i){
//                    case R.id.fryear:
//                        year = "First Year";
//
//                        break;
//                    case R.id.scyear:
//                        year = "Second Year";
//                        break;
//                    case R.id.thyear:
//                        year = "Third Year";
//                        break;
//                    case R.id.foyear:
//                        year = "Fourth Year";
//                        break;
//                    default:
//                        year = "Please Select Year";
//                }
//            }
//        });
//
//        submit.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Toast.makeText(getApplicationContext(),year,Toast.LENGTH_LONG).show();
//                dialog.dismiss();
//            }
//        });
//
//        String url = "http://139.59.26.186/branch";



        if(Acourse.isEmpty() || Acourse.equals("")){
            Toast.makeText(getApplicationContext(),"Please Select Course",Toast.LENGTH_LONG).show();
        }else{

//            disable = true;
            HashMap<String, String> para = new HashMap<>();
            para.put("course",Acourse);

            final ListView listView;

            cdialog.setContentView(R.layout.courses);

            listView = (ListView) cdialog.findViewById(R.id.clist);
            final   ProgressBar Bar = cdialog.findViewById(R.id.progressBar10);
//        String[] courses = new String[]{
//                "Polytechnic",
//                "Engineering",
//                "D-Pharmacy",
//                "B-Pharmacy"
//        };
//        ArrayList<String> list = new ArrayList<>();
//
//        list.add("Polytechnic");
//        list.add("Engineering");
//        list.add("D-Pharmacy");
//        list.add("B-Pharmacy");

      cdialog.show();
            final JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, URLS.Branch, new JSONObject(para), new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                      Bar.setVisibility(View.INVISIBLE);
//                    disable = false;
                    try {

                       JSONArray array = response.getJSONArray("cbranch");
                       for (int i = 0; i<array.length(); i++){
                           JSONObject object = array.getJSONObject(i);

                           String year = object.getString("toyear");
                           int yr = Integer.parseInt(year);
                           String se = object.getString("nofsem");
                           semester = se;

                           String[] toyear =new String[]{
                                   "First Year",
                                   "Second Year",
                                   "Third Year",
                                   "Fourth Year",
                                   "Fifth Year",
                                   "Sixth Year",
                                   "Seventh Year"
                           };

                           ArrayList<String> years = new ArrayList<>();

                           for(int c = 0; c < yr; c++){
                               years.add(toyear[c]);
                           }
                           ArrayAdapter<String> adapter = new ArrayAdapter<String>(signup.this,android.R.layout.simple_list_item_1,years);
                           listView.setAdapter(adapter);



                       }





                    } catch (JSONException e) {
                        Bar.setVisibility(View.GONE);
                        e.printStackTrace();
                        Toast.makeText(getApplicationContext(),"Exeption "+e,Toast.LENGTH_LONG).show();

                    }


                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {

//                    disable = false;
                    Bar.setVisibility(View.GONE);
                    Toast.makeText(getApplicationContext(),"Volley "+error,Toast.LENGTH_LONG).show();

                }
            });

            requestQueue.add(request);

            listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

//                    disable = false;
                    String yea = (String) listView.getItemAtPosition(i);
//                    Toast.makeText(getApplicationContext(),yea,Toast.LENGTH_LONG).show();
                    b.setText(yea);
                    year = yea;
                    cdialog.dismiss();
                }
            });

            cdialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
                @Override
                public void onCancel(DialogInterface dialogInterface) {

                }
            });
        }
    }
   }
