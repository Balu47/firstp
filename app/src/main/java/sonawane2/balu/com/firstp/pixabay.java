package sonawane2.balu.com.firstp;

/**
 * Created by shine on 12-Jan-18.
 */

public class pixabay {
    private int view, likes;
    private String imagev, tags;

    public pixabay(int view, int likes, String imagev, String tags){
        this.view = view;
        this.likes = likes;
        this.imagev = imagev;
        this.tags = tags;
    }

    public String getTags() {
        return tags;
    }

    public void setTags(String tags) {
        this.tags = tags;
    }

    public int getView() {
        return view;
    }

    public void setView(int view) {
        this.view = view;
    }

    public int getLikes() {
        return likes;
    }

    public void setLikes(int likes) {
        this.likes = likes;
    }

    public String getImagev() {
        return imagev;
    }

    public void setImagev(String imagev) {
        this.imagev = imagev;
    }
}
