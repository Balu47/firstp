package sonawane2.balu.com.firstp;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import sonawane2.balu.com.firstp.adapter.Comon_Adapter;

/**
 * Created by shine on 27-Feb-18.
 */

public class CourseList extends AppCompatActivity {
    private ListView listView;
    private BaseAdapter adapter;
    private CBPOJO cbpojo;
    private ArrayList<CBPOJO> cbpojos;
    private RequestQueue requestQueue;
    private ProgressBar progressBar;
    private Bundle bundle;
    private String branchOryear;
    private Dialog dialog ;
    private ProgressDialog progressDialoge;
    private SharedPreferences preferences;
    private String token;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.course_list);


        listView = findViewById(R.id.course_list);
        cbpojos = new ArrayList<>();
        progressBar = findViewById(R.id.progressBar8);
        requestQueue = Volley.newRequestQueue(this);
        dialog = new Dialog(this,R.style.Theme_AppCompat_DayNight_Dialog_Alert);
        progressDialoge = new ProgressDialog(this);
        progressDialoge.setMessage("Please Wait..");
        progressDialoge.setCancelable(false);

        preferences = this.getSharedPreferences("com.example.TOKEN", Context.MODE_PRIVATE);
        token = preferences.getString("TOKEN","");

        bundle = getIntent().getExtras();
        if (bundle != null){
            branchOryear = bundle.getString("where");
            LOadDatas();

        }else {
            Toast.makeText(getApplicationContext(), " Some Error Occured!", Toast.LENGTH_LONG).show();
        }
    }

    private void show(){
        progressBar.setVisibility(View.VISIBLE);
    }
    private void hide(){
        progressBar.setVisibility(View.GONE);
    }
    private void LOadDatas(){
        show();

//        String url = "http://024ac872.ngrok.io/course";

        final JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, URLS.Course, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {

                hide();

                try {
                    boolean success = response.getBoolean("success");

                    if (success){
                        JSONArray array = response.getJSONArray("Name");
                        for (int i = 0; i<array.length(); i++){
                            JSONObject object = array.getJSONObject(i);
                            String courses = object.getString("name");
                            String yr = object.getString("year");
                            String id = object.getString("id");


                            cbpojo = new CBPOJO(courses, yr, id);
                            cbpojos.add(cbpojo);

                            adapter = new Comon_Adapter(cbpojos, CourseList.this);

                        }
                        listView.setAdapter(adapter);

                    }else{
                        String error = response.getString("Error");
                        Toast.makeText(getApplicationContext(),error,Toast.LENGTH_LONG).show();
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(getApplicationContext(),"Exeption "+e,Toast.LENGTH_LONG).show();

                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                hide();
                Toast.makeText(getApplicationContext(),"Volley "+error,Toast.LENGTH_LONG).show();

            }
        });
        requestQueue.add(request);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

                cbpojo = (CBPOJO) listView.getItemAtPosition(i);
                String course = cbpojo.getCbpojo();
                String year = cbpojo.getYear();
                String id = cbpojo.getId();

                if (branchOryear.equals("course")){

                    showpopun(course, year, id);
                    Toast.makeText(getApplicationContext(),course,Toast.LENGTH_LONG).show();
                }else{

                    Intent intent = new Intent(CourseList.this,Common_list.class);
                    intent.putExtra("where", branchOryear);
                    intent.putExtra("course",course);
                    startActivity(intent);
                }

            }
        });
    }

    private void showpopun(final String course, String year, final String id ) {


        dialog.setContentView(R.layout.student_dialog);

        TextView na, no, co, br, yr;
        Button cancel, block, email;

        na = dialog.findViewById(R.id.textView15);
        no = dialog.findViewById(R.id.textView17);
        co = dialog.findViewById(R.id.textView18);
        br = dialog.findViewById(R.id.textView19);
        yr = dialog.findViewById(R.id.textView20);
        block = dialog.findViewById(R.id.button18);
        email = dialog.findViewById(R.id.button18c);
        cancel = dialog.findViewById(R.id.button19);

        na.setText("Course Name : "+course);
        no.setText("No. year : "+year);
        co.setVisibility(View.GONE);
        br.setVisibility(View.GONE);
        yr.setVisibility(View.GONE);
        email.setVisibility(View.GONE);

        block.setText("DELETE");

        dialog.show();

        block.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                DeleteCourse(course ,id);

            }
        });

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                dialog.dismiss();
            }
        });
    }

    private void DeleteCourse(final String course, final String id) {

        Button delete, cancel;

        dialog.setContentView(R.layout.common_dialog);

        delete = dialog.findViewById(R.id.delete);
        cancel = dialog.findViewById(R.id.cancel);

        dialog.show();

        delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Delete(course, id);
                dialog.dismiss();
            }
        });

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                dialog.dismiss();
            }
        });
    }

    private void Delete(final String course, final String id) {

        progressDialoge.show();
        Toast.makeText(getApplicationContext(),id,Toast.LENGTH_LONG).show();
//        String url = "http://024ac872.ngrok.io/del/course";

        StringRequest request = new StringRequest(Request.Method.POST, URLS.DelCourse, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                progressDialoge.hide();
                adapter.notifyDataSetChanged();
                cbpojos.clear();
                LOadDatas();
                Toast.makeText(getApplicationContext(),response,Toast.LENGTH_LONG).show();

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialoge.hide();
//                Toast.makeText(getApplicationContext(),"Error "+error,Toast.LENGTH_LONG).show();

            }
        }){
            @Override
            public String getBodyContentType() {
                return "application/x-www-form-urlencoded; charset=UTF-8";
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> header = new HashMap<>();
                header.put("token", token);
                return header;
            }

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> param = new HashMap<>();
                param.put("cid",id);
                param.put("course",course);


                return param;
            }
        };
        requestQueue.add(request);
    }


}
