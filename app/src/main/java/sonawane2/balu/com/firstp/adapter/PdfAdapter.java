package sonawane2.balu.com.firstp.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

import sonawane2.balu.com.firstp.R;
import sonawane2.balu.com.firstp.pdfViewer;
import sonawane2.balu.com.firstp.pdfmdm;

/**
 * Created by shine on 03-Feb-18.
 */

public class PdfAdapter extends RecyclerView.Adapter<PdfAdapter.ViewHolder> {

    ArrayList<pdfmdm> pdf = new ArrayList<pdfmdm>();
    Context context;
    private onclick oncli ;

    public PdfAdapter(ArrayList<pdfmdm> pdf, Context context, onclick oncli){
        this.pdf = pdf;
        this.context = context;
        this.oncli = oncli;
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.listofpdf,parent,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {

        holder.pdfname.setText("Name : "+pdf.get(position).getName());
        holder.pdfsub.setText("Subject : "+pdf.get(position).getSub());
        holder.pdfsem.setText(pdf.get(position).getSem());
        holder.uploader.setText("uploded by : "+pdf.get(position).getUploader());

//        holder.pdfname.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Intent intent = new Intent(context,pdfViewer.class);
//                intent.putExtra("pdfurl",pdf.get(position).getPdfurl());
//                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//                context.startActivity(intent);
//            }
//        });

        holder.pdfname.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (oncli != null) {
                    oncli.myonclick(position,"view");
                }
            }
        });

    }

    @Override
    public int getItemCount() {
        return pdf == null ? 0 : pdf.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView pdfname, pdfsub, pdfsem, uploader;
        public ViewHolder(View itemView) {
            super(itemView);

            pdfname = itemView.findViewById(R.id.pdfname);
            pdfsub = itemView.findViewById(R.id.pdfsub);
            pdfsem = itemView.findViewById(R.id.pdfsem);
            uploader = itemView.findViewById(R.id.uploader);
        }
    }

    public interface onclick{
        public void myonclick(int pos,String action);
    }
}
