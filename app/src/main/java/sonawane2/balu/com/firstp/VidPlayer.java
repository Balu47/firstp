package sonawane2.balu.com.firstp;

import android.app.ProgressDialog;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.widget.MediaController;
import android.widget.VideoView;


/**
 * Created by shine on 11-Jan-18.
 */

public class VidPlayer extends AppCompatActivity {
    ProgressDialog dialog;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.act_vid);

        final VideoView video = findViewById(R.id.videoView2);
        Bundle bundle = getIntent().getExtras();

        if (bundle != null){
            String vidUrl = bundle.getString("uri");
            Uri vidUri = Uri.parse(vidUrl);
            video.setVideoURI(vidUri);
        }else {
            String vidUrl = "https://archive.org/download/ksnn_compilation_master_the_internet/ksnn_compilation_master_the_internet_512kb.mp4";
            Uri vidUri = Uri.parse(vidUrl);
            video.setVideoURI(vidUri);
        }



        MediaController controller = new MediaController(this);
        controller.setAnchorView(video);
        video.setMediaController(controller);

        dialog = new ProgressDialog(this);
        dialog.setTitle("Video Player");
        dialog.setMessage("your video is loading...");
        dialog.setIndeterminate(false);
        dialog.setCancelable(true);
        dialog.show();

        video.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            @Override
            public void onPrepared(MediaPlayer mediaPlayer) {
                dialog.dismiss();
                video.start();
            }
        });
    }
}
