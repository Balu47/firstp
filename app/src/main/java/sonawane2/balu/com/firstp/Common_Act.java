package sonawane2.balu.com.firstp;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;

import sonawane2.balu.com.firstp.R;

/**
 * Created by shine on 12-Mar-18.
 */

public class Common_Act extends AppCompatActivity {

    private Button cou, bra, year, common, subjectr, chapter;
    private Common_Dialog common_dialog;
    private String where = "";
    private Bundle bundle;
    private Dialog dialog;
    private RequestQueue requestQueue;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.common_act);

        cou = findViewById(R.id.button26);
        bra = findViewById(R.id.button27);
        year = findViewById(R.id.button28);
        subjectr = findViewById(R.id.button29);
        chapter = findViewById(R.id.button30);

        common = findViewById(R.id.button31);

        common_dialog = new Common_Dialog();

        common_dialog.ClearThings();

        dialog = new Dialog(this, R.style.Theme_AppCompat_DayNight_Dialog_Alert);
        requestQueue = Volley.newRequestQueue(this);
        hide();
        bundle = getIntent().getExtras();
        if (bundle.getString("where","").equals("pdf") || bundle.getString("where").equals("deletepdf")){
            show();
        }

        init();

        cou.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                common_dialog.ChooseCourse(dialog,Common_Act.this, requestQueue,cou);
            }
        });

        bra.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (common_dialog.UNIQUEC.isEmpty() || common_dialog.UNIQUEC.equals("")){
                    Toast.makeText(getApplicationContext(),"please select Course", Toast.LENGTH_LONG).show();
                }else{
                    common_dialog.ChooseBranch(dialog,Common_Act.this,requestQueue,bra);

                }
            }
        });

        year.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (common_dialog.UNIQUEB.isEmpty() || common_dialog.UNIQUEB.equals("")){
                    Toast.makeText(getApplicationContext(),"please select Branch", Toast.LENGTH_LONG).show();
                }else{
                    common_dialog.LoadYear(dialog,Common_Act.this,requestQueue, year);

                }
            }
        });

        subjectr.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (common_dialog.UNIQUEC.isEmpty() || common_dialog.UNIQUEB.isEmpty() || common_dialog.UNIQUEY.isEmpty()){
                    Toast.makeText(getApplicationContext(),"please select Course", Toast.LENGTH_LONG).show();
                }else{
                    common_dialog.ChooseSubject(dialog,Common_Act.this,requestQueue,subjectr);

                }
            }
        });

        chapter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (common_dialog.UNIQUEC.isEmpty() || common_dialog.UNIQUEB.isEmpty() || common_dialog.UNIQUEY.isEmpty()){
                    Toast.makeText(getApplicationContext(),"please select Course", Toast.LENGTH_LONG).show();
                }else{
                    common_dialog.ChooseChapter(dialog,Common_Act.this,requestQueue,chapter);

                }
            }
        });

        common.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (common_dialog.UNIQUEB.isEmpty() ){
                    Toast.makeText(getApplicationContext(),"please select All Fields", Toast.LENGTH_LONG).show();
                }else if(common_dialog.UNIQUEY.isEmpty() || common_dialog.UNIQUEY.equals("")){
                    Toast.makeText(getApplicationContext(),"please select year", Toast.LENGTH_LONG).show();

                }else{
                    Intent intent = new Intent(Common_Act.this, teacherList.class);
                    intent.putExtra("branch",common_dialog.UNIQUEB);
                    intent.putExtra("year", common_dialog.UNIQUEY);

                    switch (bundle.getString("where","")){
                        case "teacher":

                            intent.putExtra("where", "teacher");
                            startActivity(intent);
                            break;
                        case "student":


                            intent.putExtra("where","student");
                            startActivity(intent);
                            break;
                        case "pdf":

                         if (common_dialog.UNIQUES.isEmpty() || common_dialog.UNIQUECHA.isEmpty()){

                             Toast.makeText(getApplicationContext(),"Please select subject or chapter", Toast.LENGTH_LONG).show();
                         }else{
                             Intent intentt = new Intent(Common_Act.this,PdfList.class);
//                             intentt.putExtra("branch", common_dialog.UNIQUEB);
                             intentt.putExtra("year", common_dialog.UNIQUEY);
                             intentt.putExtra("subid", common_dialog.UNIQUEID);
                             intentt.putExtra("chapter", common_dialog.UNIQUECHA);
                             intentt.putExtra("admin", true);
                             startActivity(intentt);
                         }

                            break;
                        case "subject":

                            Intent intents = new Intent(Common_Act.this, SubjectList.class);
                            intents.putExtra("branch", common_dialog.UNIQUEB);
                            intents.putExtra("year", common_dialog.UNIQUEY);
                            intents.putExtra("course", common_dialog.UNIQUEC);
                            intents.putExtra("where", "subject");
                            intents.putExtra("admin", true);
                            startActivity(intents);

                            break;

                        case "chapter":

                            Intent intentc = new Intent(Common_Act.this,SubjectList.class);
                            intentc.putExtra("branch", common_dialog.UNIQUEB);
                            intentc.putExtra("year", common_dialog.UNIQUEY);
                            intentc.putExtra("course", common_dialog.UNIQUEC);
                            intentc.putExtra("where", "chapters");
                            intentc.putExtra("admin", true);
                            startActivity(intentc);
                            break;
                        case "deletepdf":

                            Intent intentt = new Intent(Common_Act.this,PdfList.class);
//                             intentt.putExtra("branch", common_dialog.UNIQUEB);
                            intentt.putExtra("year", common_dialog.UNIQUEY);
                            intentt.putExtra("subid", common_dialog.UNIQUEID);
                            intentt.putExtra("chapter", common_dialog.UNIQUECHA);
                            intentt.putExtra("admin", true);
                            startActivity(intentt);
                            break;
                        default:

                    }
                }




            }
        });
    }

    private void show(){
        subjectr.setVisibility(View.VISIBLE);
        chapter.setVisibility(View.VISIBLE);
    }
    private void hide(){
        subjectr.setVisibility(View.GONE);
        chapter.setVisibility(View.GONE);
    }

    private void init() {

        switch (bundle.getString("where","")){
            case "teacher":
                common.setText("View Teacher");
                break;
            case "student":
                common.setText("view students");
                break;
            case "pdf":
                common.setText("view pdf");
                break;
            case "subject":
                common.setText("View Subjects");
                break;
            case "chapter":
                common.setText("View Chapters");
                break;
            case "deletepdf":
                common.setText("View Pdf For Delete");
                break;
            default:
                common.setText("please go back");

        }
    }
}
