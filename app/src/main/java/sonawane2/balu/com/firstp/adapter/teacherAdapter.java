package sonawane2.balu.com.firstp.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

import sonawane2.balu.com.firstp.R;
import sonawane2.balu.com.firstp.TeacherPOJO;

/**
 * Created by shine on 15-Feb-18.
 */

public class teacherAdapter extends RecyclerView.Adapter<teacherAdapter.ViewHolder> {
    ArrayList<TeacherPOJO> teacher = new ArrayList<>();
    Context context;
    private Teacher_onclick oncli ;

    public teacherAdapter( ArrayList<TeacherPOJO> teacher, Context c, Teacher_onclick oncli){
        this.context = c;
        this.teacher  = teacher;
        this.oncli = oncli;
    }

    @Override
    public teacherAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.tlist,parent,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(teacherAdapter.ViewHolder holder, final int position) {

        holder.tname.setText("Name : "+teacher.get(position).getTname());
        holder.tsub.setText("Subject : "+teacher.get(position).getTsub());

        holder.tname.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (oncli != null) {
                    oncli.Tmyonclick(position,"view");
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return teacher == null ? 0 : teacher.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        TextView tname, tsub;
        public ViewHolder(View itemView) {
            super(itemView);
            tname = itemView.findViewById(R.id.tname);
            tsub = itemView.findViewById(R.id.tsub);
        }
    }
    public interface Teacher_onclick{
        public void Tmyonclick(int positions,String action);
    }
}
