package sonawane2.balu.com.firstp;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import sonawane2.balu.com.firstp.R;

/**
 * Created by shine on 10-Mar-18.
 */

public class MapView extends AppCompatActivity {


    private TextView line1, line2, line3, land, mo, pincode;
    private RequestQueue requestQueue;
    private SharedPreferences preferences;
    private WebView webView;
    private ProgressDialog progressDialog;
    private Dialog dialog;
    private Button button;
    private LinearLayout linearLayout;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.mapview);

       line1 = findViewById(R.id.line1);
        line2 = findViewById(R.id.line2);
        line3 = findViewById(R.id.line3);
        pincode = findViewById(R.id.line4);
        land = findViewById(R.id.line5);
        mo = findViewById(R.id.line6);

        dialog = new Dialog(this, R.style.Theme_AppCompat_DayNight_Dialog_Alert);
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Please Waite...");
        progressDialog.setCancelable(false);
        webView = findViewById(R.id.mapwebView);
        button = findViewById(R.id.button34);
        linearLayout = findViewById(R.id.add);

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                linearLayout.setVisibility(View.GONE);
                webView.setVisibility(View.VISIBLE);
                button.setVisibility(View.GONE);
                ChechNEt();
            }
        });


        requestQueue = Volley.newRequestQueue(this);

        preferences = this.getSharedPreferences("com.example.TOKEN", Context.MODE_PRIVATE);

        Bundle bundle = getIntent().getExtras();

        if (bundle !=null){
            boolean load = bundle.getBoolean("load");

            if (load){
                init();
            }else{
                Toast.makeText(getApplicationContext(),"Something Error",Toast.LENGTH_LONG).show();
            }
        }else{

            setText();
        }



    }

    private void ChechNEt() {

        if (isNetworkConnected()){

            LoadMap();


        }else{

            DialogCon();
        }
    }

    private void DialogCon() {

        TextView title;
        Button reload, back;

        dialog.setContentView(R.layout.common_dialog);

        title = dialog.findViewById(R.id.textView22);
        reload = dialog.findViewById(R.id.cancel);
        back = dialog.findViewById(R.id.delete);

        title.setText("Network Error!");
        reload.setText("reload");
        back.setText("back");
        back.setVisibility(View.GONE);

        dialog.show();

        reload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                dialog.dismiss();
                ChechNEt();
            }
        });

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


            }
        });
    }

    private void LoadMap() {
        webView.setWebViewClient(new WebViewClient(){

            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                super.onPageStarted(view, url, favicon);


                progressDialog.show();
            }

            @Override
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);

                progressDialog.dismiss();
            }

            @Override
            public void onReceivedError(WebView view, WebResourceRequest request, WebResourceError error) {
                super.onReceivedError(view, request, error);

                progressDialog.dismiss();
                DialogCon();
            }
        });
        webView.getSettings().setJavaScriptEnabled(true);

        String url = "https://www.google.co.in/maps/place/MAYUR+ACADEMY+for+Polytechnic+%26+Engg/@19.1721098,77.3141028,17z/data=!3m1!4b1!4m5!3m4!1s0x3bd1d658bc9e0f7f:0x73ae4c9404f657ea!8m2!3d19.1721098!4d77.3162915?dcr=0";
        webView.loadUrl(url);
    }

    private boolean isNetworkConnected() {
        ConnectivityManager connec = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        if ( connec.getNetworkInfo(0).getState() == android.net.NetworkInfo.State.CONNECTED ||
                connec.getNetworkInfo(0).getState() == android.net.NetworkInfo.State.CONNECTING ||
                connec.getNetworkInfo(1).getState() == android.net.NetworkInfo.State.CONNECTING ||
                connec.getNetworkInfo(1).getState() == android.net.NetworkInfo.State.CONNECTED ) {


            Toast.makeText(this, " Connected ", Toast.LENGTH_LONG).show();
            return true;

        } else if (
                connec.getNetworkInfo(0).getState() == android.net.NetworkInfo.State.DISCONNECTED ||
                        connec.getNetworkInfo(1).getState() == android.net.NetworkInfo.State.DISCONNECTED  ) {

            Toast.makeText(this, " Not Connected ", Toast.LENGTH_LONG).show();
            return false;
        }


        return false;

    }

    private void setText() {
        line1.setText(preferences.getString("line1",""));
        line2.setText(preferences.getString("line2",""));
        line3.setText(preferences.getString("line3",""));
        pincode.setText("Pincode : "+preferences.getString("pin",""));
        land.setText("Mobile : "+preferences.getString("land",""));
        mo.setText("Landline"+preferences.getString("mobile",""));
    }


    public  void  init(){

        progressDialog.show();
        JsonArrayRequest request = new JsonArrayRequest(Request.Method.GET, URLS.About, null, new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {

                progressDialog.dismiss();
                try {
                    JSONObject object = response.getJSONObject(0);

                    String line10, line20, line30, landline0, pincode0, mobile0, title0, about0;
                    line10 = object.getString("line1");
                    line20= object.getString("line2");
                    line30 = object.getString("line3");
                    landline0 = object.getString("landline");
                    pincode0 = object.getString("pincode");
                    mobile0 = object.getString("mobile");
                    title0 = object.getString("title");
                    about0 = object.getString("about");

                    line1.setText(line10);
                    line2.setText(line20);
                    line3.setText(line30);
                    pincode.setText("pincode : "+pincode0);
                    mo.setText("Landline : "+mobile0);
                    land.setText("Mobile : "+landline0);

//                    line1.setText(line10);
//                    line1.setText(line10);




                } catch (JSONException e) {
                    e.printStackTrace();
                }

//                Toast.makeText(getApplicationContext(),"Response "+response,Toast.LENGTH_LONG).show();

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                progressDialog.dismiss();
                Toast.makeText(getApplicationContext(),"Error"+error,Toast.LENGTH_LONG).show();
            }
        });

        requestQueue.add(request);
    }
}
