package sonawane2.balu.com.firstp.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

import sonawane2.balu.com.firstp.CBPOJO;
import sonawane2.balu.com.firstp.R;
import sonawane2.balu.com.firstp.StudentPOJO;

/**
 * Created by shine on 24-Feb-18.
 */

public class Common_Adapter extends RecyclerView.Adapter<Common_Adapter.ViewHolder> {
    private ArrayList<CBPOJO> names = new ArrayList<>();
    Context context;
    private  onclick oncli ;

    public Common_Adapter(ArrayList<CBPOJO> names, Context context, onclick oncli) {
        this.names = names;
        this.context = context;
        this.oncli = oncli;
    }

    @Override
    public Common_Adapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.simple_list,parent,false);
        return new Common_Adapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(Common_Adapter.ViewHolder holder,final int position) {

        holder.name.setText(names.get(position).getCbpojo());
//        holder.name.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                if (oncli != null) {
//                    oncli.myonclick(position,"view");
//                }
//            }
//        });
        holder.name.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                if (oncli != null) {
                    oncli.myonclick(position,"view");
                }                return true;
            }
        });
    }

    @Override
    public int getItemCount() {
        return names.isEmpty() ? 0 : names.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        private TextView name;

        public ViewHolder(View itemView) {
            super(itemView);

            name = itemView.findViewById(R.id.textView14);
        }
    }

    public interface onclick{
        public void myonclick(int pos,String action);
    }
}
