package sonawane2.balu.com.firstp;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import sonawane2.balu.com.firstp.adapter.PdfAdapter;

/**
 * Created by shine on 02-Feb-18.
 */

public class PdfList extends AppCompatActivity implements PdfAdapter.onclick {
    private RecyclerView.LayoutManager manager;
    private RecyclerView recyclerView;
    private ArrayList<pdfmdm> pdfmdms;
    private PdfAdapter pdfAdapter;
    private pdfmdm pdfmdm;
    private RequestQueue requestQueue;
    private ProgressBar progressBarg;
    private TextView er;
    private String SEM = "";
    private SharedPreferences preferences, stoken;
    private Dialog dialog;
    private Bundle bundle;
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_SECURE,WindowManager.LayoutParams.FLAG_SECURE);

        setContentView(R.layout.pdflist);

        er = findViewById(R.id.textView7);
        manager = new LinearLayoutManager(this);
        recyclerView = findViewById(R.id.pdflistr);
        pdfmdms = new ArrayList<>();
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(manager);
        requestQueue =  Volley.newRequestQueue(this);
        progressBarg = findViewById(R.id.progressBar4);
        progressBarg.getIndeterminateDrawable().setColorFilter(0xFF34DA32, PorterDuff.Mode.MULTIPLY);
        recyclerView.setVisibility(View.GONE);
        progressBarg.setVisibility(View.VISIBLE);
        preferences = this.getSharedPreferences("com.example.TOKEN", Context.MODE_PRIVATE);
        stoken = this.getSharedPreferences("com.example.SHOW", Context.MODE_PRIVATE);
        dialog = new Dialog(this, R.style.Theme_AppCompat_DayNight_Dialog_Alert);

       init();

    }

    private void init(){
           bundle = getIntent().getExtras();
        if (bundle != null){
//            String branch = bundle.getString("branch");
            String year =  bundle.getString("year");
//            String course =  bundle.getString("course");
            String subject = bundle.getString("subid");
            String chapter = bundle.getString("chapter");

            if (bundle.getBoolean("admin")){

                LOadDatas(  year, subject,chapter);
            }else{
                SEM = bundle.getString("sem");

                loadData( subject, chapter);
            }
        }else {

        }
    }

    private void LOadDatas( String year, String sub, String cha) {
//        String uri = "http://024ac872.ngrok.io/auth/allist";
        final String token = preferences.getString("TOKEN","");
        HashMap<String, String> para = new HashMap<>();
        para.put("token",token);

        para.put("year", year);
        para.put("subid", sub);
        para.put("chapter", cha);



        final JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, URLS.Apdfs, new JSONObject(para), new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject response) {
                progressBarg.setVisibility(View.GONE);
                recyclerView.setVisibility(View.VISIBLE);
                try {

                    int count = response.getInt("count");
                    JSONArray array = response.getJSONArray("PdfList");

                    for( int i = 0; i < array.length(); i++){

                        JSONObject object = array.getJSONObject(i);

                        String pdfname, pdfurl, pdfsub, pdfsem, uploader, course, branch, year, pdname, id;
                        pdfname = object.getString("name");
                        pdfurl = object.getString("url");
                        pdfsub = object.getString("sub");
                        pdfsem = object.getString("sem");
                        uploader = object.getString("uploader");
                        course = object.getString("course");
                        branch = object.getString("branch");
                        year = object.getString("year");
                        pdname = object.getString("pdfname");
                        id = object.getString("id");

                        pdfmdms.add(new pdfmdm(pdfname, pdfurl, pdfsub, pdfsem, uploader, course, branch,year, pdname, id));
//                       Toast.makeText(getApplicationContext(),"RESPONSE "+response,Toast.LENGTH_LONG).show();
                    }
//                       String pdftitle = object.getString("pdftitle");
//                       String pdfurl = object.getString("pdfurl");






                    pdfAdapter = new PdfAdapter(pdfmdms,getApplicationContext(), PdfList.this);
                    recyclerView.setAdapter(pdfAdapter);
                    Log.e("RESPONSE",""+response);
//                    Toast.makeText(getApplicationContext(),"RESPONSE "+response,Toast.LENGTH_LONG).show();
                } catch (JSONException e) {
                    e.printStackTrace();
//                    Toast.makeText(getApplicationContext(),"e "+e,Toast.LENGTH_LONG).show();
                    Log.e("E ",""+e);
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressBarg.setVisibility(View.GONE);
                er.setVisibility(View.VISIBLE);
                Log.e("ERROR",""+error);
//                Toast.makeText(getApplicationContext(),"error "+error,Toast.LENGTH_LONG).show();
            }
        });
        requestQueue.add(request);
    }


    private void loadData( String sub, String cha) {

//        String uri = "http://024ac872.ngrok.io/list";
//          String uri = "http://new-exapp.openode.io/list";

        HashMap<String, String> para = new HashMap<>();
        para.put("sem",SEM);
//        para.put("course", course);
//        para.put("branch", branch);
        para.put("subid", sub);
        para.put("chapter", cha);
        para.put("token", stoken.getString("STOKEN",""));


       final JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, URLS.Spdfs, new JSONObject(para), new Response.Listener<JSONObject>() {

           @Override
            public void onResponse(JSONObject response) {
                     progressBarg.setVisibility(View.GONE);
                   recyclerView.setVisibility(View.VISIBLE);
                try {

                       int count = response.getInt("count");
                       JSONArray array = response.getJSONArray("PdfList");

                       for( int i = 0; i < array.length(); i++){

                           JSONObject object = array.getJSONObject(i);

                           String pdfname, pdfurl, pdfsub, pdfsem, uploader, course, branch, year,pdname, id;
                           pdfname = object.getString("name");
                           pdfurl = object.getString("url");
                           pdfsub = object.getString("sub");
                           pdfsem = object.getString("sem");
                           uploader = object.getString("uploader");
                           course = object.getString("course");
                           branch = object.getString("branch");
                           year = object.getString("year");
                           pdname = object.getString("pdfname");
                           id = object.getString("id");

                           pdfmdms.add(new pdfmdm(pdfname, pdfurl, pdfsub, pdfsem, uploader, course , branch, year, pdname, id));
//                       Toast.makeText(getApplicationContext(),"RESPONSE "+response,Toast.LENGTH_LONG).show();
                       }
//                       String pdftitle = object.getString("pdftitle");
//                       String pdfurl = object.getString("pdfurl");






                    pdfAdapter = new PdfAdapter(pdfmdms,getApplicationContext(),PdfList.this);
                    recyclerView.setAdapter(pdfAdapter);
                    Log.e("RESPONSE",""+response);
//                    Toast.makeText(getApplicationContext(),"RESPONSE "+response,Toast.LENGTH_LONG).show();
                } catch (JSONException e) {
                    e.printStackTrace();
//                    Toast.makeText(getApplicationContext(),"e "+e,Toast.LENGTH_LONG).show();
                    Log.e("E ",""+e);
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressBarg.setVisibility(View.GONE);
                er.setVisibility(View.VISIBLE);
                Log.e("ERROR",""+error);
//                Toast.makeText(getApplicationContext(),"error "+error,Toast.LENGTH_LONG).show();
            }
        });
     requestQueue.add(request);
    }

    @Override
    public void myonclick(int pos, String action) {
        ArrayList<pdfmdm> pdf = new ArrayList<>();
       if (bundle.getBoolean("admin")){
           boolean delete = preferences.getBoolean("delete",false);
           if (delete){
               showTpopun(pos);

           }else{
               Intent intent = new Intent(PdfList.this,pdfViewer.class);
               intent.putExtra("pdfurl",pdfmdms.get(pos).getPdfurl());
               intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
               startActivity(intent);
           }
       }else{
           Intent intent = new Intent(PdfList.this,pdfViewer.class);
           intent.putExtra("pdfurl",pdfmdms.get(pos).getPdfurl());
           intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
           startActivity(intent);
       }
    }

    private void showTpopun(final int pos) {

        final Dialog dialog = new Dialog(this,R.style.Theme_AppCompat_DayNight_Dialog_Alert);
        dialog.setContentView(R.layout.student_dialog);

        TextView na, no, co, br, yr;
        Button cancel, block, email;

        na = dialog.findViewById(R.id.textView15);
        no = dialog.findViewById(R.id.textView17);
        co = dialog.findViewById(R.id.textView18);
        br = dialog.findViewById(R.id.textView19);
        yr = dialog.findViewById(R.id.textView20);
        block = dialog.findViewById(R.id.button18);
        email = dialog.findViewById(R.id.button18c);
        cancel = dialog.findViewById(R.id.button19);

        na.setText("Name : "+pdfmdms.get(pos).getName());
        no.setText("email : "+pdfmdms.get(pos).getCourse());
        co.setText("Course : "+pdfmdms.get(pos).getBranch());
        br.setText("Branch : "+pdfmdms.get(pos).getYear());
        yr.setText("Subject : "+pdfmdms.get(pos).getSub());
        email.setVisibility(View.GONE);

        block.setText("Delete");

        dialog.show();

        block.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Deletepop(pdfmdms.get(pos).getPdfname());
                dialog.dismiss();
            }
        });

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                dialog.dismiss();
            }
        });
    }

    private void DeleteTeacher(final String id) {

//        String url = "http://024ac872.ngrok.io/del/teacher";

        StringRequest request = new StringRequest(Request.Method.POST, URLS.DeletePdf, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                pdfAdapter.notifyDataSetChanged();
                pdfmdms.clear();
                init();
                Toast.makeText(getApplicationContext(),response,Toast.LENGTH_LONG).show();

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                Toast.makeText(getApplicationContext(),"Error "+error,Toast.LENGTH_LONG).show();

            }
        }){
            @Override
            public String getBodyContentType() {
                return "application/x-www-form-urlencoded; charset=UTF-8";
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> header = new HashMap<>();
                header.put("token",preferences.getString("TOKEN",""));
                return header;
            }

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> param = new HashMap<>();
                param.put("pdfname",id);
                param.put("token",preferences.getString("TOKEN",""));


                return param;
            }
        };
        requestQueue.add(request);
    }

    private void Deletepop(final String id){

        Button delete, cancel;
        TextView message;
        dialog.setContentView(R.layout.common_dialog);

        message = dialog.findViewById(R.id.textView22);
        delete = dialog.findViewById(R.id.delete);
        cancel = dialog.findViewById(R.id.cancel);

        delete.setText("DELETE");

        dialog.show();

        delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                DeleteTeacher(id);

                dialog.dismiss();
            }
        });

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                dialog.dismiss();
            }
        });
    }

}
