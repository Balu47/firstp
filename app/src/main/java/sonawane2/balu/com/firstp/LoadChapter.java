package sonawane2.balu.com.firstp;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import sonawane2.balu.com.firstp.adapter.Comon_Adapter;

/**
 * Created by shine on 21-Mar-18.
 */

public class LoadChapter extends AppCompatActivity {

    private ListView listView;
    private BaseAdapter adapter, adapterss;
    private CBPOJO cbpojo;
    private ArrayList<CBPOJO> cbpojos;
    private RequestQueue requestQueue;
    private ProgressBar progressBar;
    private Bundle bundle;
    private Dialog dialog , dialog2;
    private ProgressDialog progressDialoge;
    private SharedPreferences preferences;
    private String token;
    private String id ="";
    private Common_Dialog common_dialog;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.subject_list);

        listView = findViewById(R.id.subjectlist);
        cbpojos = new ArrayList<>();
        dialog = new Dialog(this,R.style.Theme_AppCompat_DayNight_Dialog_Alert);
        dialog2 = new Dialog(this,R.style.Theme_AppCompat_DayNight_Dialog_Alert);
        progressDialoge = new ProgressDialog(this);
        progressDialoge.setMessage("Please Wait..");
        progressDialoge.setCancelable(false);
        requestQueue = Volley.newRequestQueue(this);
        cbpojos = new ArrayList<>();

        bundle = getIntent().getExtras();
        if (bundle != null){

            id = bundle.getString("sid");

            ShowChapters();

        }else {
            Toast.makeText(getApplicationContext(), " Some Error Occured!", Toast.LENGTH_LONG).show();
        }
    }

    private void ProgresDToggle(){
        if (progressDialoge.isShowing()){

            progressDialoge.dismiss();
        }else{

            progressDialoge.show();
        }
    }


    private void ShowChapters(){

        ProgresDToggle();

        HashMap<String, String> para = new HashMap<>();
        para.put("sid",id);




        final JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, URLS.GetChap, new JSONObject(para), new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                ProgresDToggle();

                try {
                    boolean success = response.getBoolean("success");
                    JSONArray object = response.getJSONArray("chapters");

                    if (success){

                        if (object.length() <= 0){

                            Toast.makeText(getApplicationContext(),"No Chapter added",Toast.LENGTH_LONG).show();

                        }else {
                            for (int b=0; b<object.length(); b++){
                                JSONObject chapters = object.getJSONObject(b);

                                String branch = chapters.getString("name");

                                  cbpojo = new CBPOJO(branch);
                                cbpojos.add(cbpojo);

                                adapter = new CorseAdapter(cbpojos, getApplicationContext());



                                listView.setAdapter(adapter);

                            }
                        }
//                        Toast.makeText(getApplicationContext(),"Res "+response,Toast.LENGTH_LONG).show();


                    }else{
                        String error = response.getString("Error");
                        Toast.makeText(getApplicationContext(),error,Toast.LENGTH_LONG).show();
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(getApplicationContext(),"Exeption "+e,Toast.LENGTH_LONG).show();

                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                ProgresDToggle();

                Toast.makeText(getApplicationContext(),"Volley "+error,Toast.LENGTH_LONG).show();

            }
        });

        requestQueue.add(request);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

                cbpojo = (CBPOJO) listView.getItemAtPosition(i);
                String chapter = cbpojo.getCbpojo();
                Toast.makeText(getApplicationContext(),chapter,Toast.LENGTH_LONG).show();
                if (bundle.getBoolean("isAdmin",false)){

                    Deletecha(id,chapter );
                    dialog.dismiss();
                }else {
                    Intent intent = new Intent(LoadChapter.this, PdfList.class);
                    intent.putExtra("sem",bundle.getString("sem"));
                    intent.putExtra("chapter", chapter);
                    intent.putExtra("branch", bundle.getString("branch"));
                    intent.putExtra("subid",bundle.getString("sid"));
                    intent.putExtra("admin", false);
                    startActivity(intent);
                }

            }
        });

        dialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialogInterface) {

            }
        });
    }

    private void Deletecha(final String id, final String chapter) {

        Button delete, cancel;

        dialog2.setContentView(R.layout.common_dialog);

        delete = dialog2.findViewById(R.id.delete);
        cancel = dialog2.findViewById(R.id.cancel);

        dialog2.show();

        delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Deletechapter(id, chapter);
                dialog2.dismiss();
            }
        });

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                dialog2.dismiss();
            }
        });
    }

    private void Deletechapter(final String id, final String chapter) {

        ProgresDToggle();
//        Toast.makeText(getApplicationContext(),id,Toast.LENGTH_LONG).show();
//        String url = "http://024ac872.ngrok.io/del/course";

        StringRequest request = new StringRequest(Request.Method.POST, URLS.DelChap, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                adapter.notifyDataSetChanged();
                cbpojos.clear();
              ShowChapters();
                ProgresDToggle();
//                Toast.makeText( getApplicationContext(),response,Toast.LENGTH_LONG).show();

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                ProgresDToggle();
                adapter.notifyDataSetChanged();
                cbpojos.clear();
                ShowChapters();
//                Toast.makeText(getApplicationContext(),"Error "+error,Toast.LENGTH_LONG).show();

            }
        }){
            @Override
            public String getBodyContentType() {
                return "application/x-www-form-urlencoded; charset=UTF-8";
            }

//            @Override
//            public Map<String, String> getHeaders() throws AuthFailureError {
//                Map<String, String> header = new HashMap<>();
//                header.put("token", token);
//                return header;
//            }

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> param = new HashMap<>();
                param.put("sid",id);
                param.put("cname",chapter);



                return param;
            }
        };
        requestQueue.add(request);
    }
}
