package sonawane2.balu.com.firstp;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import sonawane2.balu.com.firstp.R;

/**
 * Created by shine on 13-Feb-18.
 */

public class MainPage extends AppCompatActivity {

    TextView title, about;
    private RequestQueue requestQueue;
    private   SharedPreferences preferences;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.mainpage);

        Button button = findViewById(R.id.sm);
        title = findViewById(R.id.textView23);
         about = findViewById(R.id.textView9);

         requestQueue = Volley.newRequestQueue(this);

         preferences = this.getSharedPreferences("com.example.TOKEN", Context.MODE_PRIVATE);

         title.setText(preferences.getString("title",""));
         about.setText(preferences.getString("about", ""));

      init();

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainPage.this,StudyM.class);
                startActivity(intent);
                finish();
            }
        });
    }

    public  void  init(){

        final SharedPreferences.Editor editor = preferences.edit();

        JsonArrayRequest request = new JsonArrayRequest(Request.Method.GET, URLS.About, null, new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {

                try {
                    JSONObject object = response.getJSONObject(0);

                    String line10, line20, line30, landline0, pincode0, mobile0, title0, about0;
                    line10 = object.getString("line1");
                    line20= object.getString("line2");
                    line30 = object.getString("line3");
                    landline0 = object.getString("landline");
                    pincode0 = object.getString("pincode");
                    mobile0 = object.getString("mobile");
                    title0 = object.getString("title");
                    about0 = object.getString("about");

                    editor.putString("line1",line10);
                    editor.putString("line2",line20);
                    editor.putString("line3",line30);
                    editor.putString("pin", pincode0);
                    editor.putString("land", landline0);
                    editor.putString("mobile",mobile0);
                    editor.putString("title", title0);
                    editor.putString("about", about0);
                    editor.commit();


                    title.setText(title0);
                    about.setText(about0);




                } catch (JSONException e) {
                    e.printStackTrace();
                }

//                Toast.makeText(getApplicationContext(),"Response "+response,Toast.LENGTH_LONG).show();

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                Toast.makeText(getApplicationContext(),"Error"+error,Toast.LENGTH_LONG).show();
            }
        });

        requestQueue.add(request);
    }

}
