package sonawane2.balu.com.firstp;

/**
 * Created by shine on 23-Feb-18.
 */

public class CBPOJO {

    String cbpojo, year, id ;


    public CBPOJO(String cbpojo, String year, String id) {
        this.cbpojo = cbpojo;
        this.year = year;
        this.id = id;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public CBPOJO(String cbpojo) {
        this.cbpojo = cbpojo;
    }

    public String getCbpojo() {
        return cbpojo;
    }

    public void setCbpojo(String cbpojo) {
        this.cbpojo = cbpojo;
    }
}
