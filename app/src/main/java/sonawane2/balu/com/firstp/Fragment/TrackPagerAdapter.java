package sonawane2.balu.com.firstp.Fragment;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;


import java.util.ArrayList;

/**
 * Created by shine on 14-Jan-18.
 */

public class TrackPagerAdapter extends FragmentPagerAdapter {
   private ArrayList<Fragment> fragments;
   private String[] name= new String[]{"Profile", "Docs", "Videos"};

    public TrackPagerAdapter(FragmentManager fm, ArrayList<Fragment> fragments) {
        super(fm);
        this.fragments = fragments;
    }

    @Override
    public Fragment getItem(int position) {
        return this.fragments.get(position);
    }

    @Override
    public int getCount() {
        return this.fragments.size();
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return name[position];
    }
}
