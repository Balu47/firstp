package sonawane2.balu.com.firstp.Fragment;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import sonawane2.balu.com.firstp.R;
import sonawane2.balu.com.firstp.adapter.Videoli;
import sonawane2.balu.com.firstp.mAdapter;
import sonawane2.balu.com.firstp.pixabay;


/**
 * Created by shine on 14-Jan-18.
 */

public class Videos extends Fragment {
    private RecyclerView recyclerView;
    private RecyclerView.LayoutManager manager;
    private Videoli adapter;
    private ArrayList<pixabay> pix;
    private pixabay pixabay;
    private RequestQueue requestQueue;
    private ProgressDialog dialog;

    public Videos(){}
    public  static Videos newInstance(){
        Videos fragment = new Videos();
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.vids,container,false);
        return view;

    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        recyclerView = getActivity().findViewById(R.id.vidsre);
        manager = new LinearLayoutManager(getActivity());
        pix = new ArrayList<>();
        recyclerView.setLayoutManager(manager);
        recyclerView.setHasFixedSize(true);
        dialog = new ProgressDialog(getActivity());
        dialog.setTitle("Loading Videos...");

        requestQueue = Volley.newRequestQueue(getActivity());
        getData();

//        Button button = getActivity().findViewById(R.id.button3a);
//        button.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//
//                dialog.show();
//            }
//        });

    }

    @Override
    public void onStart() {
        super.onStart();

    }

    private void getData() {
        String uri = "https://pixabay.com/api/videos/?key=7677086-fc3100c74f1a2cae8a7a7f145";

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, uri, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                       dialog.dismiss();
                        try {
                            JSONArray jsonArray = response.getJSONArray("hits");
                            for (int i =0; i < jsonArray.length(); i++){
                                JSONObject hit = jsonArray.getJSONObject(i);
                                JSONObject vid = hit.getJSONObject("videos");
                                JSONObject vidm = vid.getJSONObject("tiny");


                                int views = hit.getInt("views");
                                int likes = hit.getInt("likes");
                                String tags = hit.getString("tags");
//                                String imguri = hit.getString("webformatURL");
                                String imguri = vidm.getString("url");

                                pix.add(new pixabay(views,likes,imguri,tags));
                            }


                            adapter = new Videoli(getActivity(), pix);
                            recyclerView.setAdapter(adapter);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                dialog.dismiss();
                error.printStackTrace();
            }
        });
        requestQueue.add(request);
    }

}
