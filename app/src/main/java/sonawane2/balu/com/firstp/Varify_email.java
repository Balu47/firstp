package sonawane2.balu.com.firstp;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by shine on 14-Mar-18.
 */

public class Varify_email extends AppCompatActivity {

    private Button sub;
    private EditText code;
    private TextView logi;
    private RequestQueue requestQueue;
    private int i = 0;
    private ProgressDialog dialog;
    private SharedPreferences preferences;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.varemail);

        sub = findViewById(R.id.button32);
        code = findViewById(R.id.editText11);
        logi = findViewById(R.id.textView25);

        requestQueue = Volley.newRequestQueue(this);
        dialog = new ProgressDialog(this);
        dialog.setMessage("Varifying...");
        preferences = this.getSharedPreferences("com.example.SHOW",this.MODE_PRIVATE);


        sub.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            String  androidid = Settings.Secure.getString(getApplicationContext().getContentResolver(), Settings.Secure.ANDROID_ID);

                String co = code.getText().toString();

                if (co.isEmpty() || co.equals("") || androidid.isEmpty()){

                    Toast.makeText(getApplicationContext(),"Please Enter Code",Toast.LENGTH_LONG).show();


                }else {

                    Dialogtog();
                    VerifyCode(co, androidid);

                }

            }
        });

        logi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Varify_email.this, login.class);
                startActivity(intent);
                finish();
            }
        });
    }

    private void Dialogtog(){
        if (dialog.isShowing()){
            dialog.dismiss();
        }else{
            dialog.show();
        }
    }

    private void VerifyCode(final String co, final String uid) {

        StringRequest request = new StringRequest(Request.Method.POST, URLS.VCode, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                SharedPreferences.Editor editor = preferences.edit();

                Dialogtog();
                if (response.equals("err")){

                    Toast.makeText(getApplicationContext(),"Error",Toast.LENGTH_LONG).show();

                }
                else if (response.equals("wrong")){

                    Toast.makeText(getApplicationContext(),"Wrong Otp",Toast.LENGTH_LONG).show();

                }
                else{


                    editor.putString("UID", response);
                    editor.commit();

                    Intent intent = new Intent(Varify_email.this, signup.class);
                    intent.putExtra("update", false);
                    startActivity(intent);
                    finish();


                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                Dialogtog();
                Toast.makeText(getApplicationContext(),""+error,Toast.LENGTH_LONG).show();

            }
        }){

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> param = new HashMap<>();
                param.put("otpcode",co);
                param.put("aid", uid);


                return param;
            }
        };

        requestQueue.add(request);
    }
}
